<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

setlocale(LC_ALL, 'nl_NL');
date_default_timezone_set('Europe/Amsterdam');

$data_tables =json_decode(file_get_contents('setup.json'), true);

// Set defaults
foreach ($data_tables as $key => $table) {
	$select = 'naam';
	if(array_key_exists('select', $table)) {
		$select = $table['select'];
	}
	else if(array_key_exists('check', $table)) {
		$select = $table['check'];
	}
	$data_tables[$key]['select'] = $select;

	$fields = [];
	foreach ($table['fields'] as $field) {
		if(array_key_exists('default', $field)) {
			foreach ($_ENV as $e => $ev) {
				$field['default'] = str_replace("{%ENV:".$e."}", $ev, $field['default']);
			}
			$files = [
				'template.html',
			];
			foreach ($files as $f) {
				$field['default'] = str_replace("{%FILE:".$f."}", file_get_contents($f), $field['default']);
			}
		}

		$fields[$field['name']] = $field;
	}
	$data_tables[$key]['fields'] = $fields;
}

function has_auth(array $needle, array $haystack) {
	foreach ($needle as $i) {
		if(in_array($i, $haystack)) {
			return true;
		}
	}
	return false;
}

function is_group_list($field) {
	return array_key_exists('options', $field['options']) && !is_array($field['options']['options']);
}

function csvstr(array $fields) : string
{
	$f = fopen('php://memory', 'r+');
	if (fputcsv($f, $fields) === false) {
		return false;
	}
	rewind($f);
	$csv_line = stream_get_contents($f);
	return rtrim($csv_line);
}

function get_stmt_params(PDOStatement $stmt) {
	$ret = [];

	$regex = '~:(\w+)~';
	if (preg_match_all($regex, $stmt->queryString, $matches, PREG_PATTERN_ORDER)) {
	   foreach ($matches[1] as $param) {
			$ret[] = $param;
	   }
	}

	return $ret;
}

function get_stmt_fields(string $table, int $id, PDOStatement $stmt, array|null &$all_fields = []) {
	global $data_tables;

	if(!array_key_exists($table, $data_tables)) return false;
	$fields = $data_tables[$table]['fields'];

	$key = sprintf('%s%d_', $table, $id);

	if(!array_key_exists($key.reset($fields)['name'], $_POST))
		return false;

	$ret = [];
	$all_fields = [];

	$params = get_stmt_params($stmt);
	if(in_array('id', $params)) {
		$ret['id'] = $id;
		$all_fields ['id'] = $id;
	}

	foreach ($fields as $id => $field) {
		$column = $field['name'];
		if(array_key_exists('column', $field)) {
			$column = $field['column'];
		}

		$value = array_key_exists($key.$field['name'], $_POST) ? $_POST[$key.$field['name']] : null;
		if($field['type'] == 'number' || $field['type'] == 'select') {
			if($value == "") {
				$value = null;
			}
		}

		if($field['type'] == 'checkbox') {
			if($value !== null) {
				$value = in_array($value, [true, 'true', 1, '1', 'on', 'yes']) ? 1 : 0;
			}
			else  {
				$value = 0;
			}
		}

		if(in_array($column, $params)) {
			$ret[$column] = $value;
		}
		$all_fields[$column] = $value;
	}

	return $ret;
}

if(array_key_exists('csv_example', $_REQUEST)) {
	header('Content-Encoding: UTF-8');
	header('Content-type: text/csv; charset=UTF-8');
	header("Pragma: no-cache");
	header("Expires: 0");

	if(array_key_exists($_REQUEST['csv_example'], $data_tables)) {
		$table = $data_tables[$_REQUEST['csv_example']];
		header("Content-Disposition: attachment; filename=".$table['index'].'.'.$_REQUEST['csv_example'].".csv");

		$fields = [];
		$line = [];
		foreach ($table['fields'] as $field) {
			$fields[] = $field['name'];

			if(is_array($field['default'])) {
				$field['default'] = json_encode($field['default']);
			}

			$line[] = $field['default'];
		}

		echo "\xEF\xBB\xBF"; // UTF-8 BOM
		echo csvstr($fields)."\n";
		echo csvstr($line)."\n";
	}

	die();
}

if(array_key_exists('text_example', $_REQUEST)) {
	header('Content-Encoding: UTF-8');
	header('Content-type: text/plain; charset=UTF-8');
	header("Pragma: no-cache");
	header("Expires: 0");

	if(array_key_exists($_REQUEST['text_example'], $data_tables)) {
		$table = $data_tables[$_REQUEST['text_example']];
		header("Content-Disposition: attachment; filename=".$table['index'].'.'.$_REQUEST['text_example'].'.txt');

		foreach ($table['fields'] as $field) {
			echo $field['default'];
		}
	}

	die();
}

require_once("lib/database.php");
require_once("lib/global.php");

if(file_exists("config.php")) ///TODO: Make live editable (via 'system.admin')
	if(filesize("config.php") > 0)
		die("<h1>Setup uitgeschakeld</h1>'config.php' bestaat, hierdoor is de setup uitgeschakeld");

if(array_key_exists("save", $_REQUEST)) {
	// Create config
	$config = [
		'db'=>[
			'dsn'=>'',
			'type'=>'mysql',
			'host'=>$_POST['db_host'],
			'user'=>$_POST['db_user'],
			'pass'=>$_POST['db_pass'],
			'data'=>$_POST['db_data']
		],
		'token'=>[
			'cookie'=>$_POST['token_cookie'],
			'chars'=>$_POST['token_chars'],
			'user'=>$_POST['token_user'],
			'team'=>$_POST['token_team']
		],
		'statics'=>[
			'hike_score_group'=>0
		],
		'score'=>[
			'hike'=>true,
			'rsw'=>true
		]
	];

	// Connect to database
	$t = explode(':', $_POST['db_host']);
	$dsn = 'mysql';
	$dsn .= ':host='.$t[0];
	$dsn .= ((count($t) > 1) ? (';port=' . $t[1]) : '');
	$dsn .= ';dbname=' . $_POST['db_data'];
	$dsn .= ';charset=utf8mb4';
	$config['db']['dsn'] = $dsn;

	try {
		$db = new DataBase($config['db']);
	}
	catch (\PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
		die();
   }

	// === User modules ====

	// Create user table and add admin user
	$users = [];
	$db->query("CREATE TABLE `user` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`username` VARCHAR(100) NOT NULL,
		`password` VARCHAR(50) NULL DEFAULT NULL,
		`token` VARCHAR(50) NULL DEFAULT NULL,
		`is_active` BOOLEAN NOT NULL DEFAULT false,
		PRIMARY KEY (`id`),
		UNIQUE (`username`),
		UNIQUE (`token`)
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_user_pass = $db->prepare("INSERT INTO `user` (`username`, `password`, `token`, `is_active`) VALUES (:username, SHA1(:password), :token, true);");

	for($id = 1; $user = get_stmt_fields('user', $id, $stmt_user_pass, $post); $id++) {
		$user['token'] = generateRandomString($config['token']['user'], $config['token']['chars']);
		$stmt_user_pass->execute($user);
		$users[$db->lastInsertId()] = ($post['role'] ? $post['role'] : []);
	}

	echo "<pre>System users:\n".json_encode($users, JSON_PRETTY_PRINT)."</pre>";

	// Create auth table
	$db->query("CREATE TABLE `auth` (
			`user_id` INT UNSIGNED NOT NULL,
			`module` ENUM(
				'system.admin',
				'system.backup',
				'rsw.toevoegen',
				'rsw.overzicht',
				'rsw.onderdeel_update',
				'rsw.submit',
				'rsw.correctie',
				'rsw.view',
				'rsw.export',
				'rsw.comments',
				'rsw.remove_comment',
				'rsw.formulier',
				'rsw.tentbriefjes',
				'team.update',
				'team.inschrijving',
				'hike.antwoord',
				'hike.checkin',
				'hike.checkout',
				'hike.correctie',
				'hike.gps',
				'hike.punten',
				'hike.route',
				'hike.view',
				'hike.formulier',
				'qrcodes.teams',
				'qrcodes.rsw',
				'qrcodes.users',
				'klassement.overzicht'
			) NOT NULL,
			PRIMARY KEY (`user_id`, `module`),
			CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
		) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_auth_module = $db->prepare("INSERT INTO `auth` (`user_id`, `module`) VALUES (:user_id, :role);");

	foreach ($users as $user_id=>$roles) {
		foreach ($roles as $role) {
			$stmt_auth_module->execute([
				'user_id'=>$user_id,
				'role'=>$role,
			]);
		}
	}

	// === Kamp modules ====

	// Create group table
	$db->query("CREATE TABLE `group` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`name` VARCHAR(255) NOT NULL,
		`contact` VARCHAR(255) NULL DEFAULT NULL,
		PRIMARY KEY (`id`)
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_group = $db->prepare("INSERT INTO `group` (`id`, `name`, `contact`) VALUES (:id, :name, :contact);");

	for($id = 1; $group = get_stmt_fields('group', $id, $stmt_group); $id++) {
		$stmt_group->execute($group);
	}

	// Koppeling tussen groups en users
	$db->query("CREATE TABLE `group_user` (
		`user_id` INT UNSIGNED NOT NULL,
		`group_id` INT UNSIGNED NOT NULL,
		PRIMARY KEY (`user_id`, `group_id`),
		CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_update_group_user = $db->prepare("INSERT `group_user` (`user_id`, `group_id`) VALUES (:id, :group_id);");

	for($id=1; $user = get_stmt_fields('user', $id, $stmt_update_group_user, $post); $id++) {
		if(is_array($post['group'])) {
			foreach ($post['group'] as $group_id) {
				$stmt_update_group_user->execute([
					'id'=>$id,
					'group_id'=>$group_id
				]);
			}
		}
	}

	// Create team table
	$db->query("CREATE TABLE `team` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`naam` VARCHAR(50) NOT NULL,
		`nummer` INT NOT NULL,
		`speltak` VARCHAR(100) NOT NULL,
		`group_id` INT UNSIGNED DEFAULT NULL,
		`contact` VARCHAR(100) NOT NULL DEFAULT '',
		`canceled` DATETIME NULL DEFAULT NULL,
		`token` VARCHAR(50) NULL DEFAULT NULL,
		PRIMARY KEY (`id`),
		UNIQUE (`token`),
		CONSTRAINT FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_bin;");

	$stmt_team = $db->prepare("INSERT INTO `team`
		(`naam`, `nummer`, `speltak`, `group_id`, `contact`, `token`)
		VALUES (:naam, :nummer, :speltak, :group_id, :contact, :token);");

	for($id = 1; $team = get_stmt_fields('team', $id, $stmt_team); $id++) {
		$team['token'] = generateRandomString($config['token']['team'], $config['token']['chars']);
		$stmt_team->execute($team);
	}

	// === RSW modules ===

	// Create subcampen table
	$subcampen = [];
	$db->query("CREATE TABLE `subcamp` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`name` VARCHAR(255) NOT NULL,
		`contact` VARCHAR(255) NULL DEFAULT NULL,
		PRIMARY KEY (`id`)
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_subcamp = $db->prepare("INSERT INTO `subcamp` (`id`, `name`, `contact`) VALUES (:id, :name, :contact);");

	for($id = 1; $subcamp = get_stmt_fields('subcamp', $id, $stmt_subcamp); $id++) {
		$stmt_subcamp->execute($subcamp);
		$subcampen[$db->lastInsertId()] = $subcamp['name'];
	}

	echo "<pre>Subkampen:\n".json_encode($subcampen, JSON_PRETTY_PRINT)."</pre>";

	$db->query("ALTER TABLE `team`
		ADD `subcamp_id` INT UNSIGNED NULL DEFAULT NULL,
		ADD `team` VARCHAR(255) NOT NULL,
		ADD CONSTRAINT FOREIGN KEY (`subcamp_id`) REFERENCES `subcamp` (`id`) ON DELETE SET NULL ON UPDATE CASCADE");

	$stmt_update_team_subcamp = $db->prepare("UPDATE `team` SET `team` = :team, `subcamp_id` = :subcamp_id WHERE `nummer` = :nummer;");

	for($id=1; $team = get_stmt_fields('team', $id, $stmt_update_team_subcamp); $id++) {
		$stmt_update_team_subcamp->execute($team);
	}

	// Create joker table
	$db->query("CREATE TABLE `rsw_joker` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`naam` VARCHAR(50) NOT NULL,
		`max_punten` INT NULL DEFAULT NULL,
		PRIMARY KEY (`id`)
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$db->query("ALTER TABLE `team`
		ADD `rsw_joker` INT UNSIGNED NULL DEFAULT NULL,
		ADD CONSTRAINT FOREIGN KEY (`rsw_joker`) REFERENCES `rsw_joker` (`id`) ON DELETE SET NULL ON UPDATE CASCADE");

	$stmt_rsw_joker = $db->prepare("INSERT INTO `rsw_joker` (`id`, `naam`, `max_punten`) VALUES (:id, :naam, :max_punten);");

	for($id=1; $rsw_joker = get_stmt_fields('rsw_joker', $id, $stmt_rsw_joker); $id++) {
		$stmt_rsw_joker->execute($rsw_joker);
	}

	// Create score_group table
	$group_with_post_users = [];
	$group_users = [];
	$db->query("CREATE TABLE `rsw_group` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`naam` VARCHAR(50) NOT NULL,
		`max_punten` INT NOT NULL DEFAULT 0,
		`dynamic` BOOLEAN NOT NULL DEFAULT FALSE,
		`dynamic_min` INT NULL DEFAULT NULL,
		`dynamic_max` INT NULL DEFAULT NULL,
		`joker_id`  INT UNSIGNED DEFAULT NULL,
		`user` ENUM(
			'None',
			'Event',
			'Subkamp',
			'Post&Subkamp',
			'Post',
			'Post/Subkamp',
			'Hike'
		) NOT NULL,
		PRIMARY KEY (`id`),
		CONSTRAINT FOREIGN KEY (`joker_id`) REFERENCES `rsw_joker` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_rsw_group = $db->prepare("INSERT INTO `rsw_group`
		(`id`, `naam`, `max_punten`, `dynamic`, `joker_id`, `user`)
		VALUES (:id, :naam, :max_punten, :dynamic, :joker_id, :user);");

	//Create rsw_rights table
	$db->query("CREATE TABLE `rsw_auth` (
		`user_id` INT UNSIGNED NOT NULL,
		`group_id` INT UNSIGNED NOT NULL,
		`subcamp_id` INT UNSIGNED NOT NULL,
		PRIMARY KEY (`user_id`, `group_id`, `subcamp_id`),
		CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`group_id`) REFERENCES `rsw_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`subcamp_id`) REFERENCES `subcamp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_rsw_auth_subcampen = $db->prepare("REPLACE INTO `rsw_auth`
		(`user_id`, `group_id`, `subcamp_id`)
		SELECT :user_id, :group_id,`id`
		FROM `subcamp`;");

	$stmt_rsw_auth_subcamp = $db->prepare("INSERT INTO `rsw_auth`
		(`user_id`, `group_id`, `subcamp_id`)
		VALUES (:user_id, :group_id, :subcamp_id);");

	$db->query("ALTER TABLE `user`
		ADD `rsw_group_id` INT UNSIGNED NULL DEFAULT NULL,
		ADD CONSTRAINT FOREIGN KEY (`rsw_group_id`) REFERENCES `rsw_group` (`id`) ON DELETE SET NULL ON UPDATE CASCADE");

	$stmt_user_token = $db->prepare("INSERT INTO `user` (`username`, `password`, `token`, `rsw_group_id`, `is_active`) VALUES (:username, null, :token, :group_id, true);");

	for($id = 1; $rsw_group = get_stmt_fields('rsw_group', $id, $stmt_rsw_group, $post); $id++) {
		$stmt_rsw_group->execute($rsw_group);
		$group_id = $db->lastInsertId();
		$group_users[$group_id] = [];

		// Group users
		switch ($post['user']) {
			case 'Hike':
				$config['statics']['hike_score_group'] = $group_id;
				break;

			case 'Event':
				$stmt_user_token->execute([
					'username'=>$post['naam'].':event',
					'token'=>generateRandomString($config['token']['user'], $config['token']['chars']),
					'group_id'=>$group_id,
				]);
				break;

			case 'Post&Subkamp':
				$group_with_post_users[$group_id] = [$post['naam'], $post['user']];
				// no break;
			case 'Subkamp':
				foreach ($subcampen as $subcamp_id => $subcamp) {
					$stmt_user_token->execute([
						'username'=>$post['naam'].':'.$subcamp,
						'token'=>generateRandomString($config['token']['user'], $config['token']['chars']),
						'group_id'=>$group_id,
					]);

					$user_id = $db->lastInsertId();
					$stmt_auth_module->execute([
						'user_id'=>$user_id,
						'role'=>'rsw.submit',
					]);
					$group_users[$group_id][] = $user_id;
					$stmt_rsw_auth_subcamp->execute([
						'user_id'=>$user_id,
						'group_id'=>$group_id,
						'subcamp_id'=>$subcamp_id,
					]);
				}
				break;

			case 'Post':
			case 'Post/Subkamp':
				$group_with_post_users[$group_id] = [$post['naam'], $post['user']];
				break;

			default:
				# Do nothing
				break;
		}

		// RSW group auth
		$auth_roles = [
			'system.admin',
			'rsw.correctie',
			'rsw.export'
		];
		foreach ($users as $user_id=>$roles) {
			if(has_auth($auth_roles, $roles)) {
				$stmt_rsw_auth_subcampen->execute([
					'user_id'=>$user_id,
					'group_id'=>$group_id,
				]);
				$group_users[$group_id][] = $user_id;
			}
		}
	}

	echo "<pre>Group with post users:\n".json_encode($group_with_post_users, JSON_PRETTY_PRINT)."</pre>";
	echo "<pre>Group users:\n".json_encode($group_users, JSON_PRETTY_PRINT)."</pre>";

	// Tabel met alle plek waar punten op te behalen zijn (id is gebruikt voor joker)
	$db->query("CREATE TABLE `rsw_post` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`naam` VARCHAR(50) NOT NULL,
		`custom` varchar(500) DEFAULT NULL,
		`groep_id` INT UNSIGNED NOT NULL,
		`uitleg` VARCHAR(500) NOT NULL,
		`max_punten` INT NOT NULL DEFAULT 0,
		PRIMARY KEY (`id`),
		CONSTRAINT FOREIGN KEY (`groep_id`) REFERENCES `rsw_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_rsw_post = $db->prepare("INSERT INTO `rsw_post`
		(`naam`, `groep_id`, `uitleg`, `max_punten`, `custom`)
		VALUES (:naam, :group_id, :uitleg, :max_punten, :custom);");

	$db->query("CREATE TABLE `rsw_auth_post` (
		`user_id` INT UNSIGNED NOT NULL,
		`rsw_post_id` INT UNSIGNED NOT NULL,
		CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`rsw_post_id`) REFERENCES `rsw_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		PRIMARY KEY (`user_id`, `rsw_post_id`)
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_auth_post = $db->prepare("INSERT INTO `rsw_auth_post`
		(`user_id`, `rsw_post_id`)
		VALUES (:user_id, :rsw_post_id);");

	for($id = 1; $rsw_post = get_stmt_fields('rsw_post', $id, $stmt_rsw_post); $id++) {
		$stmt_rsw_post->execute($rsw_post);
		$rsw_post_id = $db->lastInsertId();
		$group_id = $rsw_post['group_id'];

		if(array_key_exists($group_id, $group_with_post_users)) {
			if($group_with_post_users[$group_id][1] == 'Post' || $group_with_post_users[$group_id][1] == 'Post&Subkamp' ) {
				$stmt_user_token->execute([
					'username'=>$group_with_post_users[$group_id][0].':'.$rsw_post['naam'],
					'token'=>generateRandomString($config['token']['user'], $config['token']['chars']),
					'group_id'=>$group_id,
				]);

				$user_id = $db->lastInsertId();
				$stmt_auth_module->execute([
					'user_id'=>$user_id,
					'role'=>'rsw.submit',
				]);
				$stmt_auth_post->execute([
					'user_id'=>$user_id,
					'rsw_post_id'=>$rsw_post_id,
				]);
				$stmt_rsw_auth_subcampen->execute([
					'user_id'=>$user_id,
					'group_id'=>$group_id,
				]);
			}

			if($group_with_post_users[$group_id][1] == 'Post/Subkamp') {
				foreach ($subcampen as $subcamp_id => $subcamp) {
					$stmt_user_token->execute([
						'username'=>$group_with_post_users[$group_id][0].':'.$rsw_post['naam'].':'.$subcamp,
						'token'=>generateRandomString($config['token']['user'], $config['token']['chars']),
						'group_id'=>$group_id,
					]);

					$user_id = $db->lastInsertId();
					$stmt_auth_module->execute([
						'user_id'=>$user_id,
						'role'=>'rsw.submit',
					]);
					$stmt_auth_post->execute([
						'user_id'=>$user_id,
						'rsw_post_id'=>$rsw_post_id,
					]);
					$stmt_rsw_auth_subcamp->execute([
						'user_id'=>$user_id,
						'group_id'=>$group_id,
						'subcamp_id'=>$subcamp_id,
					]);
				}
			}

		}

		// RSW post auth
		foreach ($group_users[$group_id] as $user_id) {
			$stmt_auth_post->execute([
				'user_id'=>$user_id,
				'rsw_post_id'=>$rsw_post_id,
			]);
		}
	}

	// Tabel die punten bevad die behaald zijn
	$db->query("CREATE TABLE `rsw_score` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`user_id` INT UNSIGNED NULL DEFAULT NULL,
		`post_id` INT UNSIGNED NOT NULL,
		`team_id` INT UNSIGNED NOT NULL,
		`time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
		`score` INT NULL DEFAULT NULL,
		PRIMARY KEY (`id`),
		CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`post_id`) REFERENCES `rsw_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	// Tabel voor opmerkingen tijdens het jureren
	$db->query("CREATE TABLE `rsw_opmerking` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`user_id` INT UNSIGNED NULL DEFAULT NULL,
		`groep_id` INT UNSIGNED NOT NULL,
		`team_id` INT UNSIGNED NOT NULL,
		`time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
		`visible` TINYINT NOT NULL DEFAULT 1,
		`opmerking` TEXT NOT NULL,
		PRIMARY KEY (`id`),
		CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`groep_id`) REFERENCES `rsw_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	// === Hike modules ====

	// Create route table
	$db->query("CREATE TABLE `hike_route` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`naam` VARCHAR(50) NOT NULL,
		PRIMARY KEY (`id`)
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$db->query("ALTER TABLE `team`
		ADD `route_id` INT UNSIGNED NULL DEFAULT NULL,
		ADD CONSTRAINT FOREIGN KEY (`route_id`) REFERENCES `hike_route` (`id`) ON DELETE SET NULL ON UPDATE CASCADE");

	$stmt_hike_route = $db->prepare("INSERT INTO `hike_route` (`id`, `naam`) VALUES (:id, :naam);");

	for($id=1; $hike_route = get_stmt_fields('hike_route', $id, $stmt_hike_route); $id++) {
		$stmt_hike_route->execute($hike_route);
	}

	$stmt_update_team_route = $db->prepare("UPDATE `team` SET `route_id` = :route_id WHERE `nummer` = :nummer;");

	for($id=1; $team = get_stmt_fields('team', $id, $stmt_update_team_route); $id++) {
		$stmt_update_team_route->execute($team);
	}

	// Create post table and make for evry post a user
	$db->query("CREATE TABLE `hike_post` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`naam` VARCHAR(50) NOT NULL,
		`type` ENUM('Post','Tag','Start','Finish') NOT NULL DEFAULT 'Post',
		`allow_skip` TINYINT NOT NULL DEFAULT 0,
		`rsw_post_id` INT UNSIGNED NULL DEFAULT NULL,
		`lat` DECIMAL(10, 8) NULL,
		`lng` DECIMAL(11, 8) NULL,
		PRIMARY KEY (`id`),
		CONSTRAINT FOREIGN KEY (`rsw_post_id`) REFERENCES `rsw_post` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_hike_post = $db->prepare("INSERT INTO `hike_post` (`id`, `naam`, `type`, `allow_skip`, `rsw_post_id`, `lat`, `lng`) VALUES (:id, :naam, :type, :allow_skip, :rsw_post_id, :lat, :lng);");

	//Create hike_rights table
	$db->query("CREATE TABLE `hike_auth` (
		`user_id` INT UNSIGNED NOT NULL,
		`post_id` INT UNSIGNED NOT NULL,
		PRIMARY KEY (`user_id`, `post_id`),
		CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`post_id`) REFERENCES `hike_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_hike_auth = $db->prepare("INSERT INTO `hike_auth` (`user_id`, `post_id`) VALUES (:user_id, :id);");

	for($id = 1; $hike_post = get_stmt_fields('hike_post', $id, $stmt_hike_post, $post); $id++) {
		$stmt_rsw_post->execute([
			'naam' => $hike_post['naam'],
			'group_id' => $config['statics']['hike_score_group'],
			'uitleg' => 'Score voor hike post: '.$hike_post['naam'].' ('.$id.')',
			'max_punten' => intval($post['max_punten']),
			'custom' => null,
		]);
		$rsw_post_id = $db->lastInsertId();

		$hike_post['lat'] = ($hike_post['lat'] == '' ? null : $hike_post['lat']);
		$hike_post['lng'] = ($hike_post['lng'] == '' ? null : $hike_post['lng']);
		$hike_post['rsw_post_id'] = $rsw_post_id;
		$stmt_hike_post->execute($hike_post);

		$stmt_user_token->execute([
			'username'=>$username = "hike:".$hike_post['naam'],
			'token'=>generateRandomString($config['token']['user'], $config['token']['chars']),
			'group_id'=>$config['statics']['hike_score_group'],
		]);

		// Insert admin & post
		$user_id = $db->lastInsertId();
		$stmt_auth_module->execute([
			'user_id'=>$user_id,
			'role'=>'rsw.submit',
		]);
		$stmt_hike_auth->execute([
			'id'=>$id,
			'user_id'=>$user_id,
		]);
		$stmt_rsw_auth_subcampen->execute([
			'user_id'=>$user_id,
			'group_id'=>$config['statics']['hike_score_group'],
		]);
		$stmt_auth_post->execute([
			'user_id'=>$user_id,
			'rsw_post_id'=>$rsw_post_id,
		]);

		if($hike_post['type'] == 'Start') {
			$stmt_auth_module->execute([
				'user_id'=>$user_id,
				'role'=>'hike.route',
			]);
		}

		// Hike auth
		$auth_roles_hike = [
			'system.admin',
			'hike.correctie'
		];
		$auth_roles_score = [
			'system.admin',
			'hike.correctie',
			'rsw.correctie',
			'rsw.export'
		];
		foreach ($users as $user_id=>$roles) {
			if(has_auth($auth_roles_hike, $roles)) {
				$stmt_hike_auth->execute([
					'id'=>$id,
					'user_id'=>$user_id,
				]);
			}

			if(has_auth($auth_roles_score, $roles)) {
				$stmt_rsw_auth_subcampen->execute([
					'user_id'=>$user_id,
					'group_id'=>$config['statics']['hike_score_group'],
				]);
				$stmt_auth_post->execute([
					'user_id'=>$user_id,
					'rsw_post_id'=>$rsw_post_id,
				]);
			}
		}
	}
	//unset($config['statics']['hike_score_group']);

	// Create volgorde table
	$db->query("CREATE TABLE `hike_volgorde` (
		`route_id` INT UNSIGNED NOT NULL,
		`plaats` INT UNSIGNED NOT NULL,
		`post_id` INT UNSIGNED NOT NULL,
		PRIMARY KEY (`route_id`, `plaats`),
		CONSTRAINT FOREIGN KEY (`route_id`) REFERENCES `hike_route` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`post_id`) REFERENCES `hike_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_hike_volgorde = $db->prepare("INSERT INTO `hike_volgorde` (`route_id`, `plaats`, `post_id`) VALUES (:route_id, :plaats, :post_id);");

	for($id=1; $hike_volgorde = get_stmt_fields('hike_volgorde', $id, $stmt_hike_volgorde); $id++) {
		$stmt_hike_volgorde->execute($hike_volgorde);
	}

	$db->query("CREATE TABLE `hike_log` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`user_id` INT UNSIGNED NULL DEFAULT NULL,
		`post_id` INT UNSIGNED NOT NULL,
		`team_id` INT UNSIGNED NOT NULL,
		`time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
		`actie` enum('checkin', 'checkout', 'hit') NOT NULL,
		PRIMARY KEY (`id`),
		CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`post_id`) REFERENCES `hike_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	// Hike vragen lijst
	$db->query("CREATE TABLE `hike_vraag` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`naam` VARCHAR(50) NOT NULL,
		`vraag` VARCHAR(500) NOT NULL,
		`antwoorden` TEXT NOT NULL,
		PRIMARY KEY (`id`)
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$db->query("CREATE TABLE `hike_antwoord` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`vraag_id` INT UNSIGNED NOT NULL,
		`team_id` INT UNSIGNED NOT NULL,
		`antwoord` VARCHAR(200) NULL DEFAULT NULL,
		`status` ENUM('onbekend','auto_goed','manual_goed','auto_fout', 'manual_fout') NOT NULL DEFAULT 'onbekend',
		`time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`),
		CONSTRAINT FOREIGN KEY (`vraag_id`) REFERENCES `hike_vraag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_hike_vraag = $db->prepare("INSERT INTO `hike_vraag`
		(`id`, `naam`, `vraag`, `antwoorden`)
		VALUES (:id, :naam, :vraag, :antwoorden);");

	for($id=1; $hike_vraag = get_stmt_fields('hike_vraag', $id, $stmt_hike_vraag); $id++) {
		$stmt_hike_vraag->execute($hike_vraag);
	}

	// Hike vragen routes
	$db->query("CREATE TABLE `hike_route_vraag` (
		`route_id` INT UNSIGNED NOT NULL,
		`nummer` INT UNSIGNED NOT NULL,
		`min_plaats` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Welke post ze minimaal gehaald moeten hebben',
		`vraag_id` INT UNSIGNED NOT NULL,
		PRIMARY KEY (`route_id`, `nummer`),
		CONSTRAINT FOREIGN KEY (`route_id`) REFERENCES `hike_route` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`vraag_id`) REFERENCES `hike_vraag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$stmt_hike_route_vraag = $db->prepare("INSERT INTO `hike_route_vraag`
		(`route_id`, `nummer`, `min_plaats`, `vraag_id`)
		VALUES (:route_id, :nummer, :min_plaats, :vraag_id);");

	for($id=1; $hike_route_vraag = get_stmt_fields('hike_route_vraag', $id, $stmt_hike_route_vraag); $id++) {
		$stmt_hike_route_vraag->execute($hike_route_vraag);
	}


	// Create table for gps uuid
	$db->query("CREATE TABLE `gps_user` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`uuid` VARCHAR(36) NOT NULL,
		`user_id` INT UNSIGNED NULL,
		`team_id` INT UNSIGNED NULL,
		`interval` INT UNSIGNED NOT NULL DEFAULT 0,
		PRIMARY KEY (`id`),
		UNIQUE (`uuid`),
		CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	// Create table for gps loging
	$db->query("CREATE TABLE `gps_log` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
		`gps_id` INT UNSIGNED NOT NULL,
		`time` DATETIME NOT NULL COMMENT 'gps time',
		`latitude` DECIMAL(11,8) NULL COMMENT 'decimal degrees',
		`longitude` DECIMAL(11,8) NULL COMMENT 'decimal degrees',
		`accuracy` DECIMAL(11,2) NULL COMMENT 'meters',
		`altitude` DECIMAL(11,2) NULL COMMENT 'meters, relative to sea level',
		`altitudeAccuracy` DECIMAL(11,2) NULL COMMENT 'meters',
		`heading` DECIMAL(11,2) NULL COMMENT '0 = North, 270 = West',
		`speed` DECIMAL(11,2) NULL COMMENT 'meters per second',
		PRIMARY KEY (`id`),
		CONSTRAINT FOREIGN KEY (`gps_id`) REFERENCES `gps_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_bin;");

	$config['gps'] = [
		'token'=>'GPS',
		'cookie'=>'gps_uuid',
		'interval'=>60000
	];

	// === if MQTT host is not empty ===
	if($_POST['mqtt_host'] != '') {
		$config['mqtt'] = [
			'host' => $_POST['mqtt_host'],
			'port' => intval($_POST['mqtt_port']),
			'useSSL' => array_key_exists('mqtt_usessl', $_POST) ? (in_array($_POST['mqtt_usessl'], [true, 'true', 1, '1', 'on', 'yes']) ? true : false) : false,
			'path' => $_POST['mqtt_path'],
			'topic' => $_POST['mqtt_topic'],
			'user' => $_POST['mqtt_user'],
			'pass' => $_POST['mqtt_password']
		];
	}

	// === Noodenvelop ===
	$config['noodenvelop'] = $_POST['noodenvelop_template'];

	// === Save template ===
	$fp = fopen('template.html', 'w');
	fwrite($fp, $_POST['web_template']);
	fclose($fp);

	// === Save config to file ===
	$config_string = 'EOF_'.md5(time());

	$fp = fopen('config.php', 'w');
	fwrite($fp, sprintf("<?php\n// Config file created by: %s\n\n\$config=json_decode(\n<<<%s\n%s\n%s\n, true);\n\n?>",
		basename(__FILE__),
		$config_string,
		json_encode($config, JSON_PRETTY_PRINT),
		$config_string
	));
	fclose($fp);

	// show generated message
	echo "<h1>Huidige Config</h1><pre>".htmlspecialchars(json_encode($_POST, JSON_PRETTY_PRINT))."</pre>";
	die();
}

?>
<html>
	<head>
		<title>Setup a hike site</title>
		<style type="text/css">body{font-family:monospace;color:#000}input,textarea{width:100%}textarea{height:100px}table,td,th{border:1px solid #000;border-collapse:collapse;margin:5px;padding:5px;border-spacing:0}table th{text-align:left;vertical-align:text-top;color:#fff;background-color:#333}table td{max-width:400px}.through{text-decoration:line-through}.block{border-bottom: 1px solid #000;margin-bottom: 15px;padding-bottom: 10px;}.stripe table tr:nth-child(odd){background-color:#eee}.stripe table tr:nth-child(even){background-color:#fff}ul{padding-left:0;list-style-position:inside}.danger{color:#fff;background-color:#d9534f;border-color:#d43f3a}.danger:disabled{color:#fff;background-color:grey;border-color:#707070}#modal_back{position:absolute;z-index:1000;top:0;bottom:0;left:0;right:0;width:100%;text-align:center;background-color:#000A;position:fixed;overflow-y:scroll}#modal_block{background-color:#fff;min-width:30%}#modal_back_button{margin:10px;position:inline;left:0}#modal_block table{width:95%}.add_button{border:1px solid #000; padding: 2px; margin: 2px; cursor: pointer;display:block;width:1.5em;height: 1.5em;}.csv_table,.csv_table tr,.csv_table th,.csv_table td{border: 0}.parse_button{border:1px solid #000; padding: 2px; margin: 2px; cursor: pointer;display:block;width:3em;height: 1.5em;}</style>
	</head>
	<body>
		<form method="post" enctype="multipart/form-data">
			<table class="csv_table">
				<tbody>
					<tr>
						<td>Upload alle bestanden (letop gebruik de voorbeeld namen)</td>
						<td><input type="file" id="csvs" multiple></td>
						<td><a class="parse_button" onclick="parse_multiple_csv()">Parse</a></td>
					</tr>
				</tbody>
			</table>

			<?php
			function make_field($field) {
				if($field['type'] == 'select') {
					echo '<select ';
					foreach ($field['options'] as $key => $value) {
						if(!is_array($value)) {
							echo $key.'="'.$value.'" ';
						}
					}
					echo 'data-bind="';

					if(array_key_exists('multiple', $field['options']) && $field['options']['multiple']) {
						echo 'selectedOptions:'.$field['name'].', ';
						echo 'attr: { id: post_name+\''.$field['name'].'\', name: post_name+\''.$field['name'].'[]\' }, ';
					}
					else {
						echo 'value:'.$field['name'].', ';
						echo 'optionsCaption:\'\',';
						echo 'attr: { id: post_name+\''.$field['name'].'\', name: post_name+\''.$field['name'].'\' }, ';
					}

					echo 'options:'.$field['name'].'_options, ';
					echo 'optionsValue:i=>{return ((i.id === undefined) ? i : i.id)}, ';
					echo 'optionsText:i=>{return ((i.option_text === undefined) ? i : i.option_text())}';
					echo '" />';
				}
				elseif($field['type'] == 'textarea') {
					echo '<textarea ';
					foreach ($field['options'] as $key => $value) {
						if(!is_array($value)) {
							echo $key.'="'.$value.'" ';
						}
					}
					echo 'data-bind="value: '.$field['name'].', ';
					echo 'attr: { id: post_name+\''.$field['name'].'\', name: post_name+\''.$field['name'].'\' }"></textarea>';
				}
				else {
					echo '<input type="'.$field['type'].'" ';
					foreach ($field['options'] as $key => $value) {
						if(!is_array($value)) {
							echo $key.'="'.$value.'" ';
						}
					}
					echo 'data-bind="value: '.$field['name'].', ';
					echo 'attr: { id: post_name+\''.$field['name'].'\', name: post_name+\''.$field['name'].'\' }" />';
				}
			}


			foreach ($data_tables as $key=>$table) {
				echo '<div class="block">';
				echo '<h3>'.$table['title'].'</h3>';
				if(array_key_exists('comment', $table)) echo $table['comment'];

				echo '<table class="csv_table"><tr>';
				echo '<td>Upload</td>';
				echo '<td><input type="file" id="'.$key.'_csv" /></td>';
				if(array_key_exists('type', $table) && $table['type'] == 'text') {
					echo '<td><a href="?text_example='.$key.'" target="_blank">voorbeeld</a></td>';
					echo '<td><a class="parse_button" onclick="parse_text_'.$key.'()">Parse</a></td>';
				}
				else {
					echo '<td><a href="?csv_example='.$key.'" target="_blank">voorbeeld</a></td>';
					echo '<td><a class="parse_button" onclick="parse_csv_'.$key.'()">Parse</a></td>';
				}
				echo '</tr></table>';

				if(array_key_exists('table', $table)) {
					echo '<table id="'.$key.'">';
					echo '<tr>';
					if($table['numberd']) echo '<th>#</th>';
					foreach ($table['fields'] as $field) {
						echo '<th>'.$field['title'].'</th>';
					}
					echo '</tr>';
					echo '<tbody data-bind="foreach: '.$key.'">';
					echo '<tr>';
					if($table['numberd']) echo '<td data-bind="text: id">#</td>';
					foreach ($table['fields'] as $field) {
						echo '<td>';
						make_field($field);
						echo '</td>';
					}
					echo '</tr></tbody>';
				}
				else {
					if(array_key_exists('title', reset($table['fields']))) {
						echo '<table id="'.$key.'"  data-bind="with: '.$key.'">';
						foreach ($table['fields'] as $field) {
							echo '<tr><th>'.$field['title'].'</th><td>';
							make_field($field);
							echo '</td></tr>';
						}
					}
					else {
						echo "<div data-bind=\"with: ".$key."\">";
						make_field(reset($table['fields']));
						echo "</div>";
					}
				}
				echo '</table>';

				if(array_key_exists('table', $table)) {
					echo '<a class="add_button" onclick="add_'.$key.'()">+1</a>';
				}
				echo '</div>';
			}
			?>
			<input type="submit" name="save" value="Aanmaken" />
		</form>

		<script type="text/javascript" src="/js/jquery-3.6.1.js"></script>
		<script type="text/javascript" src="/js/papaparse.js"></script>
		<script type="text/javascript" src="/js/knockout-3.5.0.js"></script>
		<script type="text/javascript" src="/js/knockstrap.js"></script>
		<script type="text/javascript" src="/js/knockout-bindings.js"></script>
		<script type="text/javascript">
			var tables = {
				<?php foreach ($data_tables as $key=>$table) {
					echo $key.':{table:"'.$key.'", fields:'.count($table['fields']).', index:0, last_row:null},';
				} ?>
			};

			function parse_value(str) {
				try {
					return JSON.parse(str);
				} catch (e) {
					return str;
				}
			}

			<?php foreach ($data_tables as $key=>$table) {
				echo "function item_".$key."(data){";
				echo "let self=this;";
				echo "self.id = ++tables['".$key."']['index'];";
				echo "self.raw = data;";

				///TODO: Remove when form post data is from json
				if(array_key_exists('table', $table)) {
					echo "self.post_name = '".$key."'+self.id+'_';";
				}
				else {
					echo "self.post_name = '".$key."_';";
				}

				$update = '';
				$unset_check = '';
				foreach ($table['fields'] as $field) {

					if(array_key_exists('multiple', $field['options']) && $field['options']['multiple']) {
						echo "self.".$field['name']." = ko.observableArray([]);";

						$value = "parse_value(%D.".$field['name'].")";
						if(is_group_list($field)) {
							$value = "Array.from(ko.utils.arrayFilter(selfView.".$field['options']['options']."(), i=>{return ".$value."?.includes(i.".$data_tables[$field['options']['options']]['select']."())}), i=>{return i.id})";
						}
						$set_value = "self.".$field['name']."(".$value.");";

						if(is_group_list($field)) {
							$unset_check .= "selfView.".$field['options']['options']."();";
							$unset_check .= "if(self.raw.".$field['name']." !== undefined) {".str_replace('%D', 'self.raw', $set_value).";}";
						}

						$update .= "if(data.".$field['name']." !== undefined) {".str_replace('%D', 'data', $set_value)."}";
					}
					else {
						echo "self.".$field['name']." = ko.observable('');";

						$value = "%D.".$field['name'];
						if(is_group_list($field)) {
							$value = "ko.utils.arrayFilter(selfView.".$field['options']['options']."(), i=>{return i.".$data_tables[$field['options']['options']]['select']."()==".$value."})[0]?.id";
						}
						$set_value = "self.".$field['name']."(".$value.");";

						if(is_group_list($field)) {
							$unset_check .= "selfView.".$field['options']['options']."();";
							$unset_check .= "if(self.raw.".$field['name']." !== undefined) {".str_replace('%D', 'self.raw', $set_value).";}";
						}

						$update .= "if(data.".$field['name']." !== undefined) {".str_replace('%D', 'data', $set_value)."}";
					}

					if($field['type'] == 'select') {
						echo "self.".$field['name']."_options = ";
						if(is_array($field['options']['options'])) {
							echo json_encode($field['options']['options']).";";
						}
						else {
							echo "selfView.".$field['options']['options'].";";
						}
					}
				}

				echo "self.option_text = ko.computed(()=>{return ";
				if(is_array($table['select'])) {
					$f=0;
					foreach ($table['select'] as $v) {
						if($f++ > 0) echo "+' '+";
						echo "self.".$v."()";
					}
				}
				else {
					echo "self.".$table['select']."()";
				}
				echo ";});";

				if($unset_check != '') {
					echo "ko.computed(()=>{".$unset_check."});";
				}

				echo "self.update = data=>{".$update."};";
				echo "self.update(data);";
				echo "}\n\n";
			}
			?>

			var selfView = null;
			function AppViewModel() {
				var self = this;
				selfView = self;

				<?php
				$statics = '';
				foreach ($data_tables as $key=>$table) {
					if(array_key_exists('table', $table)) {
						echo "				self.".$key." = ko.observableArray([]);";

						if(array_key_exists('static', $table)) {
							foreach ($table['static'] as $data) {
								$statics .= 'selfView.'.$key.'.push(new item_'.$key.'('.json_encode($data).'));';
							}
						}
					}
					else {
						$data = [];
						foreach ($table['fields'] as $name=>$field) {
							$data[$name] = $field['default'];
						}
						echo "				self.".$key.' = ko.observable(new item_'.$key.'('.json_encode($data).'));';
					}
				}
				?>
			}

			$(document).ready(()=>{
				ko.applyBindings(new AppViewModel());
				<?php echo $statics; ?>
			});

			function parse_csv(table) {
				$("#"+table+"_csv").parse({
					config: {
						header: true,
						dynamicTyping: true,
						skipEmptyLines: 'greedy',
						step: function(results, parser) {
							tables[table].add(results.data[0]);
						}
					}
				});
			}

			function parse_multiple_csv() {
				$('#csvs').parse({
					config: {
						header: true,
						dynamicTyping: true,
						skipEmptyLines: 'greedy',
						step: function(results, parser) {
							var file_name = parser.streamer._input.name;
							var table = file_name.replace(/\.[^\.]+$/, "").replace(/^\d+\./, "");

							tables[table].add(results.data[0]);
						}
					}
				});
			};

			// Add functions
			<?php foreach ($data_tables as $key=>$table) {
				echo 'function add_'.$key.'(data, static=false){';

				if(array_key_exists('table', $table)) {
					$check_field = false;
					if(array_key_exists('check', $table)) {
						$check_field = $table['check'];
					}

					echo "var found=null;";
					if($check_field) {
						echo "if(data !== undefined && data['".$check_field."'] !== undefined){";
						echo "found = ko.utils.arrayFilter(selfView.".$key."(), item=>{";
						echo "return item.".$check_field."() == data.".$check_field.";";
						echo "})[0];";
						echo "}";
					}

					echo 'if(found==null){';
					echo 'found=new item_'.$key.'(data);';
					echo 'selfView.'.$key.'.push(found);';
					echo '}';
				}
				else {
					echo 'selfView.'.$key.'().update(data);';
				}

				echo "}\n";
				echo 'tables["'.$key.'"].add = add_'.$key.";\n";
				echo 'function parse_csv_'.$key.'() {parse_csv("'.$key.'")}'."\n";
			}?>

		</script>
	</body>
</html>
