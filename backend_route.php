<?php

require('config.php');
require_once('login.php');

define('GPX_TIME_FORMAT', 'Y-m-d\TH:i:s.u\Z');
define('MYSQL_TIME_FORMAT', 'Y-m-d H:i:s');

$start_time = DateTime::createFromFormat('U.u', microtime(TRUE), new \DateTimeZone('UTC'));

if (!in_array('hike.gps', $login_user['auth'])) {
	echo "No user cookie";
	die();
}

header("Content-type: application/gpx+xml; charset=UTF-8");
header('Content-Disposition: attachment; filename="GPX Export '.date('Y-m-d\TH:i:s\Z').'.gpx"');

// Responce header
echo <<<HEADER
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>

<gpx xmlns="http://www.topografix.com/GPX/1/1"
	xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3"
	xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
	creator="Oregon 400t"
	version="1.1"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd">
HEADER;

// Info over device
echo  "<metadata><name>GPX Export ".date('Y-m-d\TH:i:s\Z')."</name><link href=\"https://".$_SERVER['SERVER_NAME']."/\"><text>Team GPS tracking</text></link><time>".$start_time->format(GPX_TIME_FORMAT)."</time></metadata>";

if (array_key_exists('posten', $_REQUEST)) {
	// laat alle posten als Waypoint (als ze een gps addres hebben)
	$sql = "SELECT *
	FROM `hike_post`
	WHERE `lat` IS NOT NULL
	AND `lng` IS NOT NULL;";

	foreach($db->query($sql) as $row) {
		if(!in_array($row['id'], $login_user['posten']))
			continue;

		echo "<wpt lat=\"".$row['lat']."\" lon=\"".$row['lng']."\">";
		echo "<name>".$row['naam']."</name>";
		echo "<type>".$row['type']."</type>";
		echo "</wpt>";
	}
}

if (array_key_exists('team', $_REQUEST)) {
	// Laat GPS gegevens van UUID
	$stmt_user = $db->prepare("SELECT * FROM `gps_user` WHERE `team_id` = :team_id;");
	$stmt_user->execute([
		'team_id'=>$_REQUEST['team'],
	]);
	$gps_users = $stmt_user->fetchAll();

	$stmt_log = $db->prepare("SELECT * FROM `gps_log` WHERE `gps_id` = :gps_id;");

	foreach($gps_users as $row) {
		if(!array_key_exists($row['team_id'], $login_user['teams']))
			continue;

		$gps_id = $row['id'];
		$stmt_log->execute([
			'gps_id'=>$gps_id,
		]);
		$gps_logs = $stmt_log->fetchAll();

		// Build the track
		echo "<trk><name>".$row['uuid']."</name><trkseg>";
		foreach($gps_logs as $log) {
			echo "<trkpt lat=\"".$log['latitude']."\" lon=\"".$log['longitude']."\">";
			echo "<time>".DateTime::createFromFormat(MYSQL_TIME_FORMAT, $log['time'])->format(GPX_TIME_FORMAT)."</time>";
			echo "</trkpt>";
		}
		echo "</trkseg></trk>";
	}
}

// Tail
echo "</gpx>";

?>
