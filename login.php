<?php

require('config.php');
require_once('lib/database.php');

// Connect to database
try {
	$db = new DataBase($config['db']);
}
catch (\PDOException $e) {
	throw new \PDOException($e->getMessage(), (int)$e->getCode());
	die();
}

$login_user = [
	'id' =>-1,
	'auth' => [],
	'posten' => [],
	'group' => [],
	'rsw' => [],
	'rsw_post' => [],
	'teams' => []
];

$login_team = [
	'id'=>-1
];

$gps_user = [
	'id'=>-1
];


$all_subcamps = [];
$sql = "SELECT * FROM `subcamp`;";
foreach($db->query($sql) as $row) {
	$all_subcamps[$row['id']] = $row['name'];
}

$all_teams = [];
$sql = "SELECT * FROM `team`;";
foreach($db->query($sql) as $row) {
	$all_teams[$row['id']] = $row;
}

function check_auth_with_subcamp($groep, $user_post, $team = null) {
	global $login_user;

	if(!array_key_exists($groep, $user_post)) {
		return false;
	}

	if($team) {
		return array_key_exists($team, $login_user['teams']);
	}
	else {
		return true;
	}
}

// Check login actie
if(array_key_exists('username', $_REQUEST) && array_key_exists('password', $_REQUEST)) {
	$stmt_user_pass = $db->prepare("SELECT * FROM `user` WHERE `is_active` = true AND `username` = :username AND `password` = SHA1(:password);");
	$stmt_user_pass->execute([
		'username'=>$_REQUEST['username'],
		'password'=>$_REQUEST['password'],
	]);
	$rows = $stmt_user_pass->fetchAll();

	foreach($rows as $t) {
		foreach($t as $k => $v) {
			$login_user[$k] = $v;
		}

		setcookie($config['token']['cookie'], $login_user['token'], time() + (86400 * 30), "/"); // 86400 = 1 day
	}
}

// Check token beschikbaar
if(array_key_exists($config['token']['cookie'], $_REQUEST) || array_key_exists($config['token']['cookie'], $_COOKIE)) {
	$token = '';
	if(array_key_exists($config['token']['cookie'], $_COOKIE))
		$token = $_COOKIE[$config['token']['cookie']];

	// Als er een nieuwe token gescant word deze overschijven
	if(array_key_exists('token', $_GET))
		$token = $_GET['token'];

	// Cookie laden om eventuele gebruikers rechten te laden
	$token_cookie = '';
	if(array_key_exists($config['token']['cookie'], $_COOKIE))
		$token_cookie = $_COOKIE[$config['token']['cookie']];

	// Reset coockie to prevent time out
	$set_token = $token_cookie;

  // Check logout
	if($token == 'LOGOUT') {
		setcookie($config['token']['cookie'], null, -1, '/');
		setcookie($config['gps']['cookie'], null, -1, '/');

		$token = '';
		$token_cookie = '';
	}

  // laat gegevens van de user
	$stmt_user_by_token = $db->prepare("SELECT * FROM `user` WHERE `is_active` = true AND `token` = :token;");
	$stmt_user_by_token->execute([
		'token'=>$token_cookie,
	]);
	$rows = $stmt_user_by_token->fetchAll();

	foreach($rows as $t) {
		foreach($t as $k => $v) {
			$login_user[$k] = $v;
		}

		$set_token = $login_user['token'];
	}

  // Als er een nieuw user token is ingesteld neem deze dan over
	$stmt_user_by_token->execute([
		'token'=>$token,
	]);
	$rows = $stmt_user_by_token->fetchAll();

	foreach($rows as $t) {
		foreach($t as $k => $v) {
			$login_user[$k] = $v;
		}

		$set_token = $login_user['token'];
	}

 // Laat gegeven van eventueel een team
	$stmt_team_by_token = $db->prepare("SELECT * FROM `team` WHERE (`token` = :token OR `token` = :token_cookie) AND `canceled` IS NULL;");
	$stmt_team_by_token->execute([
		'token'=>$token,
		'token_cookie'=>$token_cookie,
	]);
	$rows = $stmt_team_by_token->fetchAll();

	foreach($rows as $t) {
		foreach($t as $k => $v) {
			$login_team[$k] = $v;
		}

		if($login_user['id'] == -1 && $login_team['token'] != $token_cookie) {
			$set_token = $login_team['token'];
		}

		// Als een team opgegevens is ken deze alleen voor het eige team de hike info laden
		if($login_user['id'] == -1) {
			$login_user['auth'][] = 'hike.view';

			// Als een team opgegeven is kan deze vragen beantwoorden
			$login_user['auth'][] = 'hike.antwoord';
		}
	}

	setcookie($config['token']['cookie'], $set_token, time() + (86400 * 30), "/");

	// Rechten laden van eventueel de gebruiker
	if($login_user['id'] > 0) {
		// Laat alle modules van de user
		$stmt_user_modules = $db->prepare("SELECT * FROM `auth` WHERE `user_id` = :user_id;");
		$stmt_user_modules->execute([
			'user_id'=>$login_user['id'],
		]);
		$rows = $stmt_user_modules->fetchAll();

		foreach($rows as $t) {
			$login_user['auth'][] = $t['module'];
		}

		// Laat alle groepen van de user
		$stmt_group_user = $db->prepare("SELECT * FROM `group_user` WHERE `user_id` = :user_id;");
		$stmt_group_user->execute([
			'user_id'=>$login_user['id'],
		]);
		$rows = $stmt_group_user->fetchAll();

		foreach($rows as $t) {
			$login_user['group'][] = $t['group_id'];
		}

		// Laad rechten voor hike
		$stmt_hike_auth = $db->prepare("SELECT * FROM `hike_auth` WHERE `user_id` = :user_id;");
		$stmt_hike_auth->execute([
			'user_id'=>$login_user['id'],
		]);
		$rows = $stmt_hike_auth->fetchAll();

		foreach($rows as $t) {
			$login_user['posten'][] = $t['post_id'];
		}

		// Als er rechten voor posten zijn voeg dan checkin en uit rechten toe
		if(count($login_user['posten']) > 0) {
			 // Als we toegang hebben tot een post mogen we ook zien wat er om ons heen gebeurt
			$login_user['auth'][] = 'hike.view';

			$login_user['auth'][] = 'hike.checkin';
			$login_user['auth'][] = 'hike.checkout';
		}

		// Laad rechten voor rsw
		$stmt_rsw_auth = $db->prepare("SELECT * FROM `rsw_auth` WHERE `user_id` = :user_id;");
		$stmt_rsw_auth->execute([
			'user_id'=>$login_user['id'],
		]);
		$rows = $stmt_rsw_auth->fetchAll();

		foreach($rows as $t) {
			$login_user['rsw'][$t['group_id']][$t['subcamp_id']] = $all_subcamps[$t['subcamp_id']];

			foreach ($all_teams as $team_id => $team) {
				if($team['subcamp_id'] == $t['subcamp_id']) {
					// Forbit acces to own teams for non system user
					if(in_array('system.admin', $login_user['auth']) || !in_array($team['group_id'], $login_user['group'])) {
						$login_user['teams'][$team_id] = $team['subcamp_id'];
					}
				}
			}
		}

		// Laad alle rechten die speciaal geset zijn (Post users)
		$stmt_auth_post = $db->prepare("SELECT
			rsw_post_id AS post_id,
			(select p.`groep_id` from `rsw_post` p where p.`id` = `rsw_post_id`) AS groep_id
			FROM `rsw_auth_post` a
			WHERE a.`user_id` = :user_id;");
		$stmt_auth_post->execute([
			'user_id'=>$login_user['id'],
		]);
		$rows = $stmt_auth_post->fetchAll();

		foreach($rows as $t) {
			$login_user['rsw_post'][$t['post_id']] = true;
		}

		// Als de gebruiker heeft op 1 rsw post dan heeft deze ook recht om RSW info te kunnen zien
		if(count($login_user['rsw_post']) > 0) {
			$login_user['auth'][] = 'rsw.view';
		}

		if(in_array('rsw.export', $login_user['auth'])) {
			$login_user['auth'][] = 'rsw.overzicht';
		}

		if(in_array('hike.correctie', $login_user['auth'])) {
			$login_user['auth'][] = 'hike.antwoord';
		}

		//
		if(in_array('system.admin', $login_user['auth'])) {
			$login_user['auth'][] = 'system.backup';
			$login_user['auth'][] = 'user.auth';
			$login_user['auth'][] = 'rsw.toevoegen';
			$login_user['auth'][] = 'rsw.onderdeel_update';
			$login_user['auth'][] = 'rsw.overzicht';
			$login_user['auth'][] = 'rsw.submit';
			$login_user['auth'][] = 'rsw.correctie';
			$login_user['auth'][] = 'rsw.view';
			$login_user['auth'][] = 'rsw.export';
			$login_user['auth'][] = 'rsw.comments';
			$login_user['auth'][] = 'rsw.remove_comment';
			$login_user['auth'][] = 'rsw.formulier';
			$login_user['auth'][] = 'rsw.tentbriefjes';
			$login_user['auth'][] = 'team.update';
			$login_user['auth'][] = 'team.inschrijving';
			$login_user['auth'][] = 'hike.antwoord';
			$login_user['auth'][] = 'hike.checkin';
			$login_user['auth'][] = 'hike.checkout';
			$login_user['auth'][] = 'hike.correctie';
			$login_user['auth'][] = 'hike.gps';
			$login_user['auth'][] = 'hike.punten';
			$login_user['auth'][] = 'hike.route';
			$login_user['auth'][] = 'hike.view';
			$login_user['auth'][] = 'hike.formulier';
			$login_user['auth'][] = 'qrcodes.teams';
			$login_user['auth'][] = 'qrcodes.rsw';
			$login_user['auth'][] = 'qrcodes.users';
			// $login_user['auth'][] = 'klassement.overzicht'; // Expres uit omdat deze pagina expliciet aangezet moet worden
		}

		// Verwijder dubbele rechten
		$t = array_unique($login_user['auth']);
		$login_user['auth'] = [];
		foreach ($t as $v) {
			$login_user['auth'][] = $v;
		}
	}
}

/// === Handle GPS Token

if(array_key_exists($config['gps']['cookie'], $_COOKIE)) {
	$gps_uuid = $_COOKIE[$config['gps']['cookie']];

	$stmt = $db->prepare("SELECT * FROM `gps_user` WHERE `uuid` = :uuid;");
	$stmt->execute([
		'uuid'=>$gps_uuid
	]);

	foreach($stmt->fetchAll() as $row) {
		foreach ($row as $k => $v) {
			$gps_user[$k] = $v;
		}

		setcookie($config['gps']['cookie'], $gps_user['uuid'], time() + (86400 * 30), "/");
	}
}
