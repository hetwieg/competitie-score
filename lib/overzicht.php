<?php

if(in_array('hike.correctie', $login_user['auth'])) {
$template_replace['POPUP'] = <<<EOF_POPUP
<!-- Popup om team tijden aan te passen? -->
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_post_tijden }">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="hike-tijden-label">
					Tijden: <span data-bind="text: team">[team.team]</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Aankomst</th>
							<th>Vertrek</th>
							<th>Score</th>
							<th>Gelopen</th>
							<th>Post</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: route">
						<tr data-bind="visible: post.auth, css:'table-'+item_state()">
							<td data-bind="text: post.naam"></td>
							<td><input type="text" class="form-control" data-bind="visible: post.type() != 'Start', date: checkin, dateFormat:'HH:mm'" /></td>
							<td><input type="text" class="form-control" data-bind="visible: (post.type() != 'Finish' && post.type() != 'Tag'), date: checkout, dateFormat:'HH:mm'" /></td>
							<td><input type="number" class="form-control" min="0" data-bind="visible: post.max_punten() > 0, value: score, attr:{'max':post.max_punten()}" /></td>
							<td><span data-bind="visible: post.type() != 'Start', time: tijd_gelope_naar_post"></span></td>
							<td><span data-bind="visible: post.type() == 'Post', time: tijd_op_post"></span></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>Totaal:</th>
							<th colspan="2"></th>
							<th><span data-bind="text: totaal_hike_score"></span></th>
							<th><span data-bind="time: totaal_gelope"></span></th>
							<th><span data-bind="time: totaal_op_post"></span></th>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="modal-footer">
				<span class="badge badge-default">Niet bezocht</span>
				<span class="badge badge-info">Op post</span>
				<span class="badge badge-success">Vertrokke</span>
				<span class="badge badge-danger">Tijden kloppen niet</span>

				<button type="button" class="btn btn-primary" data-bind="click: send_tijden">
				 Sluiten
				</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;

}
else {

$template_replace['POPUP'] = <<<EOF_POPUP
<!-- Popup om team tijden aan te passen? -->
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_post_tijden }">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="hike-tijden-label">
					Tijden: <span data-bind="text: naam">[team.naam]</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Aankomst</th>
							<th>Vertrek</th>
							<th>Score</th>
							<th>Gelopen</th>
							<th>Post</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: route">
						<tr data-bind="visible: post.auth, css:'table-'+item_state()">
							<td data-bind="text: post.naam"></td>
							<td><span data-bind="visible: post.type() != 'Start', date: checkin, dateFormat:'HH:mm'"></span></td>
							<td><span data-bind="visible: (post.type() != 'Finish' && post.type() != 'Tag'), date: checkout, dateFormat:'HH:mm'"></span></td>
							<td><span data-bind="visible: post.max_punten() > 0, text: score, attr:{'max':post.max_punten()}"></span></td>
							<td><span data-bind="visible: post.type() != 'Start', time: tijd_gelope_naar_post"></span></td>
							<td><span data-bind="visible: post.type() == 'Post', time: tijd_op_post"></span></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>Totaal:</th>
							<th colspan="2"></th>
							<th><span data-bind="text: totaal_hike_score"></span></th>
							<th><span data-bind="time: totaal_gelope"></span></th>
							<th><span data-bind="time: totaal_op_post"></span></th>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="modal-footer">
				<span class="badge badge-default">Niet bezocht</span>
				<span class="badge badge-info">Op post</span>
				<span class="badge badge-success">Vertrokke</span>
				<span class="badge badge-danger">Tijden kloppen niet</span>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}

?>
<div class="row m-1" data-bind="foreach: subkamp">
	<div class="m-1" data-bind="visible: teams().length > 0">
		<h4 data-bind="text: naam"></h4>
		<table class="table table-striped table-responsive">
			<thead>
				<tr>
					<th>Nr</th>
					<th>Team</th>
					<th>laaste actie</th>
					<th>
						<span class="badge badge-default" data-toggle="tooltip" title="Niet bezocht">N</span>
						<span class="badge badge-info" data-toggle="tooltip" title="Op post">O</span>
						<span class="badge badge-success" data-toggle="tooltip" title="Vertrokke">V</span>
						<span class="badge badge-danger" data-toggle="tooltip" title="Tijden kloppen niet">E</span>
					</th>
					<th>Totaal Post</th>
					<th>Totaal Lopen</th>
					<th>Totaal</th>
				</tr>
			</thead>
			<tbody data-bind="foreach: teams">
				<tr data-bind="visible: team">
					<td data-bind="text: nummer"></td>
					<td data-bind="text: team"></td>
					<td data-bind="text: route.laaste_actie"></td>
					<td data-bind="click: popup_tijden, foreach: route">
						<span class="badge" data-toggle="tooltip" data-bind="visible: post.type() == 'Post', text: '#', attr:{'title': post.naam}, css:'badge-'+item_state()"></span>
					</td>
					<td data-bind="time: totaal_op_post"></td>
					<td data-bind="time: totaal_gelope"></td>
					<td data-bind="time: totaal_op_post() + totaal_gelope()"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
