<?php

require_once(__DIR__.'/../TCPDF/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class INSPECTIE_PDF extends TCPDF {
    //Page header
    public function Header() {

    }

    public function Footer() {

    }
}

function tentbriefjes() {
    global $return_data;

    $pdf = new INSPECTIE_PDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Administratie');
	$pdf->SetTitle('Tentinspectie');
	$pdf->SetSubject('Tentinspectie');
	$pdf->SetKeywords('Tentinspectie');

	// set header and footer fonts
	$pdf->setHeaderFont(false);
	$pdf->setFooterFont(false);

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetHeaderMargin(0);
	$pdf->SetFooterMargin(0);

	// set auto page breaks
	$pdf->SetAutoPageBreak(FALSE, 0);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    $pdf->AddPage();

    for($row=0;$row<2;$row++) {
        for($col=0;$col<2;$col++) {
            $w = ($pdf->getPageWidth()/2);
            $h = ($pdf->getPageHeight()/2);
            $x = $row * $w;
            $y = $col * $h;

            $pdf->ImageSVG(__DIR__.'/../../css/warning.svg', $x+$w-($w/4)-10, $y+10, $w/4, $w/4);

            $pdf->SetFont('helvetica', 'B', 16);
            $pdf->MultiCell('', '', "Ploeg: ", 0, 'L', false, 1, $x+10, $y+10);

            $txt = "Wij, de jury, hebben jullie tent gecontroleerd. Hierbij hebben wij verboden items gevonden. De volgende dingen zijn in beslag genomen:";
            $pdf->SetFont('helvetica', '', 14);
            $pdf->MultiCell($w-($w/4)-20, '', $txt, 0, 'L', false, 1, $x+10, $y+20);

            $txt = "Je leiding is ingelicht en zal voor een oplossing zorgen.";
            $pdf->SetFont('helvetica', '', 14);
            $pdf->MultiCell($w-20, '', $txt, 0, 'L', false, 1, $x+10, $y+$h-20);

            $style = array(
                'width' => 0.25,
                'cap' => 'butt',
                'join' => 'miter',
                'dash' => 0,
                'color' => array(0, 0, 0)
            );
            $pdf->Line($x+10, $y+55, $x+$w-20, $y+55, $style);
            $pdf->Line($x+10, $y+65, $x+$w-20, $y+65, $style);
            $pdf->Line($x+10, $y+75, $x+$w-20, $y+75, $style);

            $html = "";
            $pdf->writeHTMLCell($w, $h, $x, $y, $html, 1);
        }
    }

    $return_data[] = ['pdf'=>['name'=>'Tentbriefjes.pdf', 'file'=>base64_encode($pdf->Output('', 'S'))]];
}
