<?php

require_once(__DIR__.'/../TCPDF/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class SCORE_PDF extends TCPDF {
    public $print_title = '';
    public $print_post = '';
    public $print_onderdelen = [];

    public $col_width = 0;
    public $hand_font = '';

    public function getCSS() {
        $css = '<style>
        .k {
            width: 17mm;
            margin-right:1mm
        }
        .v {
            width: 40mm;
            border-bottom: 1px solid black;
        }
        .i {
            font-size:10
            color: red;
        }
        i {
            font-family: '.$this->hand_font.';
            text-align: center;
            color: blue;
        }

        body {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"
        }

        th {
            text-align: center;
            min-width: '.$this->col_width.'mm;
        }

        .field {
            text-align: left;
            white-space: nowrap;
        }
        </style>';
        return $css;
    }

    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);

        // set bacground image
        $img_file = 'css/back.jpg';
        //$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

        $this->SetFont('helvetica', '', 12);
        $invuller_info = $this->getCSS().'<table>';
        $invuller_info .= '<tr><td class="k">invuller</td><td class="v">&nbsp;</td></tr>';
        $invuller_info .= '<tr><td class="k">mobiel</td><td class="v">&nbsp;</td></tr>';
        $invuller_info .= '<tr><td colspan="2" class="i">Vergeet de app niet in te vullen!</td></tr>';
        $invuller_info .= '</table>';
        $this->writeHTMLCell(55, '', $this->getPageWidth()-60, 5, $invuller_info);

        $this->setXY(PDF_MARGIN_LEFT, PDF_MARGIN_HEADER);

        /// Make formulier header
        $t = explode("\n", $this->print_title, 2);
        $this->SetFont('helvetica', 'B', 20);
        $this->Write(0, 'Score formulier '.$t[0], '', 0, 'L', true, 0, false, false, 0);

        if(count($t) > 0) {
            $this->SetFont('helvetica', '', 16);
            $this->Write(0, $t[1], '', 0, 'L', true, 0, false, false, 0);
        }

        $this->SetFont('helvetica', '', 10);
        $this->setListIndentWidth(4);

        if(strlen($this->print_post) > 0) {
            $this->Write(0, ''.$this->print_post, '', 0, 'L', true, 0, false, false, 0);
        }

        // -----------------------------------------------------------------------------
        $tbl = $this->getCSS().'<table cellspacing="0" cellpadding="1" border="0">';
        $i=1;
        foreach($this->print_onderdelen as $onderdeel) {
            $tbl .= '<tr>';
            $tbl .= '<th width="5mm">'.$i++.'</th>';
            $tbl .= '<td width="40mm">'.$onderdeel['naam'].'</td>';
            $tbl .= '<td width="140mm">'.$onderdeel['uitleg'].'</td>';
            $tbl .= '</tr>';
        }
        $tbl .=  '</table>';
        if($i > 1) {
            $this->writeHTML($tbl, true, false, true, false, '');
        }

        $this->SetMargins(PDF_MARGIN_LEFT, $this->GetY(), PDF_MARGIN_RIGHT);


        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

    public function Footer() {

    }

    public function GetHTMLHeight($html) {
        $tmp_pdf = clone $this;
        $tmp_pdf->AddPage();
        $tmp_pdf->writeHTML($html, true, false, true, false, '');
        return $tmp_pdf->getY();
    }
}

function format_value($val, $o) {
    if(array_key_exists('format', $o) && $val != null) {
        $format = $o['format'];
        $tmp = DateTime::createFromFormat(MYSQL_TIME_FORMAT, $val);
        $tmp->setTimezone(new DateTimeZone('Europe/Amsterdam'));
        return $tmp->format($format);
    }
    else {
        return $val;
    }
}

function make_form_for_groep($groep) {
    global $db;
    global $login_user;
    global $return_data;
    global $args;

    // create new PDF document
    $pdf = new SCORE_PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // page_split/page_head/list_item
    $page_for_subcamp = true;
    $pages = [];

    switch ($groep['user']) {
    case 'Hike':
        $page_for_subcamp = false;
        $pdf->print_onderdelen = [];

        $stmt = $db->prepare("SELECT p.*, r.`max_punten`
            FROM  `hike_post` p
            LEFT JOIN `rsw_post` r ON  p.`rsw_post_id` = r.`id`
            WHERE p.`id` IN (select `post_id` from `hike_auth` where `user_id` = :user_id);");
        $stmt->execute([
            'user_id'=>$login_user['id'],
        ]);
        foreach ($stmt->fetchAll() as $hike_post) {
            $t = [
                ["caption"=>"#", "field"=>"nummer"],
                ["caption"=>"naam", "field"=>"team"],
            ];
            $p = [];

            $type = $hike_post['type'];

            if($type == 'Tag') {
                $p[] = ["naam"=>"tijd", "uitleg"=>"De aankomst tijd."];
                $t[] = ["caption"=>"tijd", "hike_id"=>$hike_post['id'], "hike_field"=>"checkin", "format"=>"H:i"];
            }

            if($type == 'Post' || $type == 'Finish') {
                $p[] = ["naam"=>"check in", "uitleg"=>"De aankomst tijd."];
                $t[] = ["caption"=>"check in", "hike_id"=>$hike_post['id'], "hike_field"=>"checkin", "format"=>"H:i"];
            }

            if($type == 'Post' || $type == 'Start') {
                $p[] = ["naam"=>"check uit", "uitleg"=>"De vertrek tijd."];
                $t[] = ["caption"=>"check uit", "hike_id"=>$hike_post['id'], "hike_field"=>"checkout", "format"=>"H:i"];
            }

            if($hike_post['max_punten'] > 0) {
                $p[] = ["naam"=>"score", "uitleg"=>"De behaalde score (max: ".$hike_post['max_punten'].")"];
                $t[] = ["caption"=>"score", "hike_score"=>$hike_post['rsw_post_id']];
            }
            $pages[] = ["table"=>$t, "title"=>$groep['naam']."\n".$hike_post['naam'], "head"=>$p];
        }
        break;

    case 'Post&Subkamp':
    case 'Post':
        $page_for_subcamp = $groep['user'] == 'Post&Subkamp';

        foreach ($groep['onderdeel'] as $onderdeel_id => $onderdeel) {
            $t = [
                ["caption"=>"#", "field"=>"nummer"],
                ["caption"=>"naam", "field"=>"team"],
                ["caption"=>"score", "score_id"=>$onderdeel_id],
            ];
            $p = [["col"=>"", "naam"=>"score", "uitleg"=>$onderdeel['uitleg']]];
            $pages[] = ["table"=>$t, "title"=>$groep['naam']."\n".$onderdeel['naam'], "head"=>$p];
        }
        break;

    case 'Event':
        $page_for_subcamp = false;

        $i = 1;
        $t = [["caption"=>"#", "field"=>"nummer"]];
        foreach ($groep['onderdeel'] as $onderdeel_id => $onderdeel) {
            $t[] = ["caption"=>$i++, "score_id"=>$onderdeel_id];
        }

        $pages[] = ["table"=>$t, "title"=>$groep['naam'], "head"=>$groep['onderdeel']];
        break;

    case 'Subkamp':
    case 'Post/Subkamp':
    default:
        $page_for_subcamp = true;

        $i = 1;
        $t = [["caption"=>"#", "field"=>"nummer"]];
        foreach ($groep['onderdeel'] as $onderdeel_id => $onderdeel) {
            $t[] = ["caption"=>$i++, "score_id"=>$onderdeel_id];
        }

        $pages[] = ["table"=>$t, "title"=>$groep['naam'], "head"=>$groep['onderdeel']];
        break;
    }

    $subkampen = [];
    {
        $stmt = $db->prepare("SELECT * FROM `subcamp`;");
        $stmt->execute();

        foreach($stmt->fetchAll() as $row) {
            $row['team'] = [];

            if($page_for_subcamp) {
                $subkampen[$row['id']] = $row;
            }
            else {
                $subkampen[0] = $row;
                break;
            }
        }
    }

    // Teams
    {
        $stmt_rsw = $db->prepare("SELECT :team_id as team, p.`id` as post_id, p.`groep_id`,
            (select `time` from `rsw_score` where `post_id`=  p.`id` and `team_id` = team order by `id` desc limit 1) AS `time`,
            (select `score` from `rsw_score` where `post_id`=  p.`id` and `team_id` = team order by `id` desc limit 1) AS `score`
            FROM `rsw_post` p
            WHERE p.`groep_id` IN (select `group_id` from `rsw_auth` where `user_id` = :user_id);");

        $stmt_log = $db->prepare("SELECT :team_id AS team, p.`id` AS post_id,
            (select `time` from `hike_log` where `post_id`=  p.`id` and `team_id` = team and `actie` = 'checkin' order by `id` desc limit 1) AS `checkin`,
            (select `time` from `hike_log` where `post_id`=  p.`id` and `team_id` = team and `actie` = 'checkout' order by `id` desc limit 1) AS `checkout`,
            (select `score` from `rsw_score` where `post_id`=  p.`rsw_post_id` and `team_id` = team order by `id` desc limit 1) AS `score`
            FROM `hike_post` p
            WHERE p.`id` IN (select `post_id` from `hike_auth` where `user_id` = :user_id);");

        $stmt = $db->prepare("SELECT * FROM `team`;");
        $stmt->execute();
        foreach($stmt->fetchAll() as $row) {
            $a = $row;

            $stmt_rsw->execute([
                'team_id'=>$row['id'],
                'user_id'=>$login_user['id'],
            ]);
            foreach($stmt_rsw->fetchAll() as $rsw) {
                $row['score'][$rsw['post_id']] = $rsw;
            }

            $stmt_log->execute([
                'team_id'=>$row['id'],
                'user_id'=>$login_user['id'],
            ]);
            foreach($stmt_log->fetchAll() as $log) {
                $row['hike_log'][$log['post_id']] = $log;
            }

            if($page_for_subcamp) {
                $subkampen[$row['subcamp_id']]['team'][$row['id']] = $row;
            }
            else {
                $subkampen[0]['team'][$row['id']] = $row;
            }
        }
    }

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Administratie');
    $pdf->SetTitle('Score formulier '.$groep['naam']);
    $pdf->SetSubject('Score formulier '.$groep['naam']);
    $pdf->SetKeywords('RSW,Score formulier, '.$groep['naam']);

    // set header and footer fonts
    $pdf->setHeaderFont(false);
    $pdf->setFooterFont(false);

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // add handwriting
    $pdf->hand_font = TCPDF_FONTS::addTTFfont(__DIR__.'/Smile Kids.ttf', 'TrueTypeUnicode', '', 96);
    $pdf->AddFont($pdf->hand_font);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    foreach ($pages as $t) {
        $pdf->print_title = $t["title"];
        $pdf->print_onderdelen = $t["head"];
        $onderdelen = $t["table"];

        $colums = count($onderdelen);
        $pdf->col_width = min(18, 175/$colums);

        foreach ($subkampen as $subkamp_id => $subkamp) {
            // -----------------------------------------------------------------------------
            $tbl_head = $pdf->getCSS().'<table cellspacing="0" cellpadding="2" border="1">';
            $tbl_head .= '<tr>';
            for ($i=0; $i < $colums; $i++) {
                if($onderdelen[$i]['caption'] == '#') {
                    $tbl_head .= '<th width="10mm">#</th>';
                }
                else {
                    $tbl_head .= '<th>'.$onderdelen[$i]['caption'].'</th>';
                }
            }
            $tbl_head .= '</tr>';

            $tbl = '';
            $rows = 0;
            $page_y_start = 0;
            foreach ($subkamp['team'] as $team_id => $team) {
                if($tbl == '') {
                    $pdf->AddPage();
                    $page_y_start = $pdf->GetY();
                    $rows = 0;

                    if($page_for_subcamp) {
                        $pdf->SetFont('helvetica', 'B', 16);
                        $pdf->Write(0, $subkamp['name'], '', 0, 'L', true, 0, false, false, 0);
                    }

                    $pdf->SetFont('helvetica', 'B', 14);
                    $tbl = $tbl_head;
                }

                $tbl .= '<tr>';
                $rows++;

                $i=0;
                foreach ($onderdelen as $onderdeel) {
                    $i++;

                    if($onderdeel['caption'] == "#") {
                        $tbl .= '<th>'.format_value($team[$onderdeel['field']], $onderdeel).'</th>';
                    }
                    else if(array_key_exists('field', $onderdeel)) {
                        $tbl .= '<td class="field">'.format_value($team[$onderdeel['field']], $onderdeel).'</td>';
                    }
                    else if(array_key_exists('score_id', $onderdeel)) {
                        $tbl .= '<td><i>'.format_value($team['score'][$onderdeel['score_id']]['score'], $onderdeel).'</i></td>';
                    }
                    else if(array_key_exists('hike_id', $onderdeel)) {
                        $tbl .= '<td><i>'.format_value($team['hike_log'][$onderdeel['hike_id']][$onderdeel['hike_field']], $onderdeel).'</i></td>';
                    }
                    else {
                        $tbl .= '<td></td>';
                    }
                }

                $tbl .= '</tr>';
                if(($page_y_start + (10*$rows) + 30) > $pdf->getPageHeight()) {
                    if($pdf->GetHTMLHeight($tbl.'<tr><td>&nbsp;</td></tr></table>') + PDF_MARGIN_FOOTER > $pdf->getPageHeight()) {
                        $tbl .= '</table>';

                        $pdf->writeHTML($tbl, true, false, true, false, '');
                        $tbl = '';
                    }
                }
            }

            if($tbl != '') {
                $tbl .=  '</table>';

                $pdf->SetFont('helvetica', 'B', 14);
                $pdf->writeHTML($tbl, true, false, true, false, '');
            }
        }
    }


    //Close and output PDF document
    $return_data[] = ['pdf'=>['name'=>'Score formulier '.$groep['naam'].'.pdf', 'file'=>base64_encode($pdf->Output('', 'S'))]];
}
