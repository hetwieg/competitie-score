<?php

// Render de user elementen
if(in_array('rsw.submit', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup voor beoordelen RSW onderdeel -->
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_beoordeel }">
	<div class="modal-dialog" role="document">
		<div class="modal-content" data-bind="with: current_beoordeling">
			<div class="modal-header">
				<h5 class="modal-title" id="hike-tijden-label" data-bind="css: {strikeout: team.canceled, own_group: team.own_group}">
					<span data-bind="text: onderdeel().naam"></span>:
					<span data-bind="text: team.nummer"></span>.
					<span data-bind="text: team.team">[team.team]</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p class="paperforms">Vergeet niet het papieren formulier in te vullen!</p>
				<!-- ko foreach: score -->
				<div class="form-group">
					<label data-bind="text: post.naam">[naam]</label>
					<!-- ko if: post.custom_set -->
					<select class="form-control" data-bind="options: post.custom, optionsValue: function(i) {return i.value;}, optionsText: function(i) {return i.name;}, value: score, optionsCaption: 'Kies...', valueAllowUnset: true"></select>
					<!-- /ko -->
					<!-- ko ifnot: post.custom_set -->
					<input type="number" class="form-control" min="0" data-bind="value: score, attr:{'max':post.max_punten()}" />
					<small class="form-text text-muted float-right">Max:
						<i data-bind="text: post.max_punten()"></i>
					</small>
					<!-- /ko -->
					<small class="form-text text-muted" data-bind="html: post.uitleg">[uitleg]</small>
				</div>
				<!-- /ko -->
				<div class="form-group">
					<label>Nieuwe opmerking</label>
					<textarea class="form-control" data-bind="value: opmerking"></textarea>
				</div>
				<div class="form-group" data-bind="visible: opmerkingen().length > 0">
					<label>Opmerkingen</label>
					<!-- ko foreach: opmerkingen -->
					<div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false">
						<div class="toast-header">
							<strong class="mr-auto">
								<span data-bind="text: user().username">[user]</span>
							</strong>
						</div>
						<div class="toast-body" data-bind="text: opmerking">[Opmerking]</div>
					</div>
					<!-- /ko -->
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-bind="click: team.send_beoordeling">
				 Opslaan
				</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}

if(in_array('user.auth', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup om users te mange -->
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_manage_users }">
	<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content" data-bind="with: current_post">
			<div class="modal-header">
				<h5 class="modal-title">Gebruikers beheren</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				...
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-bind2="click: save_users">Opslaan</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}

if(in_array('hike.correctie', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup om team tijden aan te passen? -->
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_post_tijden }">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="hike-tijden-label" data-bind="css: {strikeout: canceled, own_group: own_group}">
					Tijden:
					<span data-bind="text: nummer"></span>.
					<span data-bind="text: team">[team]</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Aankomst</th>
							<th>Vertrek</th>
							<th>Score</th>
							<th>Gelopen</th>
							<th>Post</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: route">
						<tr data-bind="visible: post.auth, class:'table-'+item_state()">
							<td data-bind="text: post.naam"></td>
							<td>
								<input type="text" class="form-control" data-bind="visible: post.type() != 'Start', date: checkin, dateFormat:'HH:mm'" />
							</td>
							<td>
								<input type="text" class="form-control" data-bind="visible: (post.type() != 'Finish' && post.type() != 'Tag'), date: checkout, dateFormat:'HH:mm'" />
							</td>
							<td>
								<div data-bind="visible: post.max_punten() > 0">
									<input type="number" class="form-control" min="0" data-bind="value: score, attr:{'max':post.max_punten()}" />
									<small class="form-text text-muted float-right">Max:
										<i data-bind="text: post.max_punten()"></i>
									</small>
								</div>
							</td>
							<td>
								<span data-bind="visible: post.type() != 'Start', time: tijd_gelope_naar_post"></span>
							</td>
							<td>
								<span data-bind="visible: post.type() == 'Post', time: tijd_op_post"></span>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>Totaal:</th>
							<th colspan="2"></th>
							<th>
								<span data-bind="text: totaal_hike_score"></span>
							</th>
							<th>
								<span data-bind="time: totaal_gelope"></span>
							</th>
							<th>
								<span data-bind="time: totaal_op_post"></span>
							</th>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="modal-footer">
				<span class="badge badge-default">Niet bezocht</span>
				<span class="badge badge-info">Op post</span>
				<span class="badge badge-success">Vertrokke</span>
				<span class="badge badge-danger">Tijden kloppen niet</span>
				<button type="button" class="btn btn-primary" data-bind="click: send_tijden">
				 Opslaan
				</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}

if(in_array('hike.antwoord', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup om team antworden aan te bekijken / beoordelen? -->
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_hike_vragen }">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="hike-tijden-label" data-bind="css: {strikeout: canceled, own_group: own_group}">
					Hike vragen:
					<span data-bind="text: nummer"></span>.
					<span data-bind="text: team">[team]</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" data-bind="foreach: route.vragen">
				<div class="form-group">
					<label for="andwoord">
						<b data-bind="text: vraag.naam"></b>
						<span data-bind="text: vraag.vraag"></span>
					</label>
					<div class="input-group">
						<input type="text" class="form-control" id="andwoord" data-bind="value: antwoord" />
						<div class="input-group-append">
							<div class="input-group-text" data-bind="class: 'bg-'+color()">&nbsp;</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-bind="click: send_hike_vragen">
				 Opslaan
				</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}

if(in_array('hike.view', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup met alle vertrokken teams -->
<div role="dialog" id="post-vertrokken" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_post_vertrokken }">
	<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content" data-bind="with: current_post">
			<div class="modal-header">
				<h5 class="modal-title">Vertrokken teams</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table">
					<thead>
						<tr>
							<th>Nr</th>
							<th>Naam</th>
							<th data-bind="visible: \$root.current_post().max_punten() > 0">Score</th>
							<th>Roete</th>
							<th>Wanneer</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: vertrokken_teams">
						<tr data-bind="css: {strikeout: team.canceled, own_group: team.own_group}">
							<td data-bind="text: team.nummer"></td>
							<td data-bind="text: team.team"></td>
							<td data-bind="text: score, visible: \$root.current_post().max_punten() > 0"></td>
							<td data-bind="text: team.route.naam"></td>
							<td data-bind="timer: checkout"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
EOF_POPUP;
}

if(in_array('hike.checkin', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup checkin -->
<div role="dialog" id="post-checkin" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_post_checkin }">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="post-checkin-label" data-bind="css: {strikeout: canceled, own_group: own_group}">
					Check in:
					<span data-bind="text: nummer"></span>.
					<span data-bind="text: team">[team]</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<!-- ko if: \$root.current_post().type() == 'Tag' -->
				Door op check in te klikken word
				<b data-bind="text: team"></b> gemakeerd als deze post bezocht.
				<!-- /ko -->

				<!-- ko if: \$root.current_post().type() != 'Tag' -->
				Door op check in te klikken word
				<b data-bind="text: team"></b> aan uw post toegevoegd.
				<!-- /ko -->

				<div class="mt-2" data-bind="visible: ingecheckt_current_post">
					<b class="text-danger">Dit team is
						<span data-bind="timer: ingecheckt_current_post"></span> op uw post ingecheckt
					</b>
					<br />
					Door dit team opnieuw op uw post in te chekken word de vorige tijd van check in verwijdert
				</div>

				<div class="row">
					<p class="paperforms">Vergeet niet het papieren formulier in te vullen!</p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Annuleren</button>
				<button type="button" class="btn btn-success" data-bind="click: send_checkin">Check in</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}

if(in_array('hike.checkout', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup checkout -->
<div role="dialog" id="post-checkout" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_post_checkout }">
	<div class="modal-dialog" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="post-vertrek-label" data-bind="css: {strikeout: canceled, own_group: own_group}">
					Check uit voor:
					<span data-bind="text: nummer"></span>.
					<span data-bind="text: team">[team]</span>
					(<span data-bind="text: route.naam"></span>)
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row" data-bind="visible: \$root.current_post().last_checkout()">
					<label class="col-sm-12 col-form-label">Tijd tussen vorige checkout:
						<b data-bind="timer: \$root.current_post().last_checkout()"></b>
					</label>
				</div>

				<div class="row" data-bind="visible: (\$root.current_post().type() == 'Start' && \$root.auth.indexOf('hike.route') > -1)">
					<label class="col-sm-2 col-form-label" for="team_route">Roete</label>
					<div class="col-sm-10">
						<select class="form-control" id="team_route" aria-describedby="team_routeHelp" placeholder="" data-bind="options: \$root.routes, optionsText: 'naam', optionsValue: 'id', value: checkout_route"></select>
						<small id="team_routeHelp" class="form-text text-muted">
							Welke route lecht dit team af
						</small>
					</div>
				</div>

				<div class="row" data-bind="visible: \$root.current_post().max_punten() > 0">
					<label class="col-sm-2 col-form-label" for="team_score_vertrokke">Punten</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" id="team_score_vertrokke" aria-describedby="team_score_vertrokkeHelp" placeholder="" min="0" data-bind="value: checkout_score, attr:{'max':\$root.current_post().max_punten()}">
							<small class="form-text text-muted float-right">Max:
								<i data-bind="text: \$root.current_post().max_punten()"></i>
							</small>
							<small id="team_score_vertrokkeHelp" class="form-text text-muted">
							Punten behaald voor opdrachten op deze post
						</small>
					</div>
				</div>

				<div class="row">
					<p class="paperforms">Vergeet niet het papieren formulier in te vullen!</p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Annuleren</button>
				<button type="button" class="btn btn-success" data-bind="click: send_checkout">Check uit</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}

if(in_array('team.inschrijving', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup inschrijving -->
<div role="dialog" id="post-checkout" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_inschrijving }">
	<div class="modal-dialog" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="post-vertrek-label" data-bind="css: {strikeout: canceled, own_group: own_group}">Inschrijving</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group" data-bind="visble: token">
					<label>Token</label>
					<input type="text" class="form-control" data-bind="value: token" disabled />
				</div>
				<div class="form-group">
					<label>Naam</label>
					<input type="text" class="form-control" data-bind="value: naam" />
				</div>
				<div class="form-group">
					<label>Team naam</label>
					<input type="text" class="form-control" data-bind="value: team" />
				</div>
				<div class="form-group">
					<label>Nummer</label>
					<input type="number" class="form-control" data-bind="value: nummer" />
				</div>
				<div class="form-group">
					<label>Groep</label>
					<select class="form-control" data-bind="options: \$root.groups, optionsText: 'name', optionsValue: 'id', value: group_id, optionsCaption: 'Kies...'"></select>
				</div>
				<div class="form-group">
					<label>Contact</label>
					<input type="text" class="form-control" data-bind="value: contact" />
				</div>
				<div class="form-group">
					<label>Speltak</label>
					<input type="text" class="form-control" data-bind="value: speltak" />
				</div>
				<div class="form-group">
					<label>Vervallen</label>
					<input type="datetime" class="form-control" data-bind="date: canceled, dateFormat:'DD-MM-YYYY HH:mm'" />
				</div>
				<div class="form-group">
					<label>Subkamp</label>
					<select class="form-control" data-bind="options: \$root.subcamp, optionsText: 'name', optionsValue: 'id', value: subcamp_id, optionsCaption: 'Kies...'"></select>
				</div>
				<div class="form-group">
					<label>Roete</label>
					<select class="form-control" data-bind="options: \$root.routes, optionsText: 'naam', optionsValue: 'id', value: route_id, optionsCaption: 'Kies...'"></select>
				</div>
				<div class="form-group">
					<label>Joker</label>
					<select class="form-control" data-bind="options: \$root.jokers, optionsText: 'naam', optionsValue: 'id', value: joker_id, optionsCaption: 'Kies...'"></select>
					<code class="form-text text-muted float-right" data-bind="visible: joker_max() == 0">Joker geeft geen extra punten!</code>
					<small class="form-text text-muted float-right" data-bind="visible: joker_max() > 0">Max:
						<i data-bind="text: joker_max()"></i>
					</small>
					<small>De joker zorgt voor procentuele extra punten</small>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-bind="click: get_inschrijving">Print inschrijving</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
					Annuleren
				</button>
				<button type="button" class="btn btn-success" data-bind="click: update_team">Update</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}

if(in_array('rsw.overzicht', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup voor het toevoegen van een onderdeel -->
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_opmerkingen }">
	<div class="modal-dialog" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="hike-tijden-label" data-bind="css: {strikeout: canceled, own_group: own_group}">
					Opmerkingen:
					<span data-bind="text: nummer"></span>.
					<span data-bind="text: team">[team]</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<!-- ko foreach: opmerkingen -->
				<div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false">
					<div class="toast-header">
						<strong class="mr-auto">
							<span data-bind="text: onderdeel().naam">[Onderdeel]</span>
							(<span data-bind="text: user().username">[user]</span>)
						</strong>
						<button class="btn btn-sm btn-danger" aria-label="Remove" data-bind="click: remove">
							<svg
								xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
								<path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
								<path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
							</svg>
						</button>
					</div>
					<div class="toast-body" data-bind="text: opmerking">[Opmerking]</div>
				</div>
				<!-- /ko -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-bind="click: close_opmerkingen">Sluiten</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}

if(in_array('rsw.overzicht', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup voor het toevoegen van een onderdeel -->
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_score }">
	<div class="modal-dialog" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="hike-tijden-label" data-bind="css: {strikeout: canceled, own_group: own_group}">
					Punten:
					<span data-bind="text: nummer"></span>.
					<span data-bind="text: team">[team]</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<!-- ko foreach: onderdelen -->
				<div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false">
					<div class="toast-header" style="height: 3em">
						<strong class="mr-auto" data-bind="text: onderdeel().naam">[Onderdeel]</strong>
						<img src="/css/joker.svg" class="h-100" alt="Joker" data-bind="visible: is_joker"/>
					</div>
					<div class="toast-body">
						<table class="table">
							<tbody data-bind="foreach: score">
								<tr>
									<td data-bind="text: post.naam"></td>
									<td data-bind="text: post.max_punten"></td>
									<td data-bind="text: score"></td>
								</tr>
							</tbody>
							<tbody data-bind="hidden: onderdeel().max_punten_verschil() == 1">
								<tr>
									<th>Subtotaal</th>
									<td data-bind="text: onderdeel().posten.max_punten"></td>
									<td data-bind="text: posten_score_voor_max_punten"></td>
								</tr>
								<tr data-bind="visible: onderdeel().dynamic_min && false">
									<td>Minimum</td>
									<td data-bind="text: onderdeel().posten.dynamic_min"></td>
									<td></td>
								</tr>
								<tr data-bind="visible: onderdeel().dynamic">
									<td>Maximum</td>
									<td data-bind="text: onderdeel().posten.dynamic_max"></td>
									<td></td>
								</tr>
								<tr>
									<td>Correctie</td>
									<td data-bind="text: onderdeel().max_punten"></td>
									<td>/
										<span data-bind="text: onderdeel().max_punten_verschil"></span>
									</td>
								</tr>
							</tbody>
							<tbody data-bind="visible: is_joker">
								<tr>
									<th>Subtotaal</th>
									<td data-bind="text: onderdeel().max_punten"></td>
									<td data-bind="text: posten_score_round"></td>
								</tr>
								<tr>
									<td>Joker</td>
									<td data-bind="text: joker_max"></td>
									<td data-bind="text: joker_score_round"></td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<th>Totaal</th>
									<td>&nbsp;</td>
									<th data-bind="text: totaal_score_round"></th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- /ko -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-bind="click: close_score">Sluiten</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}

if(in_array('rsw.onderdeel_update', $login_user['auth'])) {
$template_replace['POPUP'] .= <<<EOF_POPUP
<!-- Popup voor het toevoegen van een onderdeel -->
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_posten_toevoegen }">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" data-bind="with: current_onderdeel">
			<div class="modal-header">
				<h5 class="modal-title" id="hike-tijden-label">
					Posten binnen:
					<span data-bind="text: naam"></span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Joker:</label>
					<select class="form-control" data-bind="options: \$root.jokers, optionsText: 'naam', optionsValue: 'id', value: joker_id, optionsCaption: 'Kies...'"></select>
				</div>
				<div class="form-group">
					<label>Max punten:</label>
					<input type="text" class="form-control" data-bind="value: max_punten" />
				</div>
				<div class="form-group">
					<label>Dienamise punten:
						<small>bereken aan de hand van de minimale en maximale verdiende score</small>
					</label>
					<div class="form-row">
						<div class="col-md-2 mb-2">
							<label for="group_dynamic">Gebruik:</label>
							<input type="checkbox" class="form-control" id="group_dynamic" data-bind="checked: dynamic">
						</div>
						<div class="col-md-5 mb-5" data-bind="visible: false">
							<label for="group_dynamic_min">Min:</label>
							<input type="number" class="form-control" id="group_dynamic" data-bind="value: dynamic_min, enable: dynamic">
							<small>
								<ul>
									<li>laat leeg voor laagste score (<span data-bind="text: posten.min_score"></span>)</li>
									<li>Positief wordt gebruikt als waarde</li>
									<li>negatief wordt bij de laagste score opgetelt</li>
								</ul>
								Waarde: <span data-bind="text: posten.dynamic_min"></span>
							</small>
						</div>
						<div class="col-md-5 mb-5">
							<label for="group_dynamic_max">Max:</label>
							<input type="number" class="form-control" id="group_dynamic" data-bind="value: dynamic_max, enable: dynamic">
							<small>
								<ul>
									<li>laat leeg voor hoogste score (<span data-bind="text: posten.max_score"></span>)</li>
									<li>Positief wordt gebruikt als waarde</li>
									<li>negatief wordt van de hoogste score afgehaald</li>
								</ul>
								Waarde: <span data-bind="text: posten.dynamic_max"></span>
							</small>
						</div>
					</div>
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>Naam</th>
							<th>Uitleg</th>
							<th>Custom</th>
							<th>Max Punten</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: posten">
						<tr>
							<td>
								<input type="text" class="form-control" data-bind="value: naam" />
							</td>
							<td>
								<input type="text" class="form-control" data-bind="value: uitleg" />
							</td>
							<td>
								<input type="text" class="form-control" data-bind="value: custom_json" />
								<div data-bind2="handsontable: {data: custom,contextMenu:true,colHeaders:['Tekst','Punten'],columns:[{data:'name'},{data:'value',type:'numeric'}]}"></div>
							</td>
							<td>
								<input type="number" class="form-control" min="1" data-bind="value: max_punten" />
							</td>
						</tr>
					</tbody>
				</table>
				<small>Letop: het wijzigen van het max aantal punten veranderd geen punten uit het verleden</small>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bind="click: new_onderdeel">+1</button>
				<button type="button" class="btn btn-primary" data-bind="click: save_onderdeel">Opslaan</button>
			</div>
		</div>
	</div>
</div>
EOF_POPUP;
}
?>

<?php if(in_array('rsw.view', $login_user['auth'])) { ?>
<script type="text/html" id="punten_onderdeel">
	<div class="" data-bind="with: $root.current_onderdeel">
		<div class="heading">
			<h3 class="username" data-bind="with: $root.user_user"><span data-bind="text: username"></span></h3>
			<h2 data-bind="text: naam"></h2>
		</div>
		<div class="body">
			<div class="progress" data-bind="foreach: {data:team_verloop, as:'item'}">
				<div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" data-bind="style: {width: item.value+'%' }, attr:{'aria-valuenow': item.value }, css: {active: item.animated, 'progress-bar-striped': item.striped}, class: 'bg-'+item.type"></div>
			</div>
			<!-- Aanwezige teams -->
			<div data-bind="visible: tebeordelen_teams().length > 0">
				<h3>Te beordelen teams</h3>
				<table class="table">
					<thead>
						<tr>
							<th>Nr</th>
							<th>Naam</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: tebeordelen_teams">
						<tr data-bind="class: 'table-'+item_state(), css: {strikeout: team.canceled, own_group: team.own_group}">
							<td data-bind="text: team.nummer"></td>
							<td data-bind="text: team.team"></td>
							<td>
								<?php if(in_array('rsw.submit', $login_user['auth'])) { ?>
								<button type="button" class="btn btn-sm btn-secondary" data-bind="click: popup_beoordeel">Beoordeel</button>
								<?php } ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div data-bind="visible: tebeordelen_teams().length == 0">
				<h3>Te beordelen teams</h3>
				Momenteel zijn alle teams beordeeld
			</div>
			<div class="btn-group mr-2" role="group">
				<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Beordeel ander team
				</button>
				<div class="dropdown-menu">
					<!-- ko foreach: $root.subcamp -->
					<h6 class="dropdown-header" data-bind="text: name, visible: teams().length > 0"></h6>
					<!-- ko foreach: teams -->
					<a class="dropdown-item" href="#" data-bind="click: popup_beoordeel, css: {strikeout: canceled, own_group: own_group}">
						<span data-bind="text: nummer"></span>.
						<span data-bind="text: team"></span>
					</a>
					<!-- /ko -->
					<!-- /ko -->
				</div>
			</div>
			<?php if(in_array('rsw.toevoegen', $login_user['auth'])) { ?>
			<div class="btn-group mr-2" role="group">
				<button class="btn btn-secondary" data-bind="click: popup_posten_toevoegen">Posten beheren</button>
			</div>
			<?php } ?>
			<?php if(in_array('rsw.formulier', $login_user['auth'])) { ?>
			<div class="btn-group mr-2" role="group">
				<button class="btn btn-secondary" data-bind="click: posten_printen">
					Formulier printen
					<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" data-bind="visible: $root.pdf_loading"></span>
				</button>
			</div>
			<?php } ?>
			<?php if(in_array('user.auth_edit', $login_user['auth'])) { ?>
			<div class="btn-group mr-2" role="group">
				<button class="btn btn-secondary" data-bind="click: popup_manage_users">Gebruikers beheren</button>
			</div>
			<?php } ?>
		</div>
	</div>
</script>
<?php } ?>

<?php if(in_array('hike.punten', $login_user['auth'])) { ?>
<script type="text/html" id="hike_punten">
	<?php include(__DIR__.'/punten.php'); ?>
	<button type="button" class="btn btn-success" data-bind="click: $root.save_hike">Opslaan</button>
</script>
<?php } ?>

<?php if(in_array('hike.gps', $login_user['auth'])) { ?>
<script type="text/html" id="hike_gps">
	<div class="heading">
		<h3 class="username" data-bind="with: $root.user_user"><span data-bind="text: username"></span></h3>
		<h3>Hike locatieinfo</h3>
	</div>
	<div class="body">
		<div id="hike_map" style="width:100%;height:500px;"></div>
	</div>
</script>
<?php } ?>

<?php if(in_array('hike.view', $login_user['auth'])) { ?>
<script type="text/html" id="hike_onderdeel">
	<div class="" data-bind="with: $root.current_post">
		<div class="heading">
			<h3 class="username" data-bind="with: $root.user_user"><span data-bind="text: username"></span></h3>
			<h2 data-bind="text: naam"></h2>
		</div>
		<div class="body">
			<div class="progress" data-bind="foreach: {data:team_verloop, as:'item'}">
				<div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"
					data-bind="style: {width: item.value+'%' }, attr:{'aria-valuenow': item.value }, css: {active: item.animated, 'progress-bar-striped': item.striped}, class: 'bg-'+item.type"></div>
			</div>
			<div data-bind="visible: type() != 'Tag'">
				<!-- Aanwezige teams -->
				<div data-bind="visible: aanwezige_teams().length > 0">
					<h3>Aanwezig</h3>
					<table class="table">
						<thead>
							<tr>
								<th>Nr</th>
								<th>Naam</th>
								<th>Roete</th>
								<th data-bind="visible: type() != 'Start'">Tijd</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody data-bind="foreach: aanwezige_teams">
							<tr data-bind="css: {strikeout: team.canceled, own_group: team.own_group}">
								<td data-bind="text: team.nummer"></td>
								<td data-bind="text: team.team"></td>
								<td data-bind="text: team.route.naam"></td>
								<td data-bind="timer: checkin, visible: $parent.type() != 'Start'"></td>
								<td>
									<!-- ko if: $parent.type() != 'Finish' -->
									<button type="button" class="btn btn-sm btn-secondary" data-bind="click: team.popup_checkout">Check uit</button>
									<!-- /ko -->
									<!-- ko if: $root.auth.indexOf('hike.correctie') > -1 -->
									<button type="button" class="btn btn-sm btn-secondary" data-bind="click: team.popup_tijden">Tijden</button>
									<!-- /ko -->
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div data-bind="visible: aanwezige_teams().length == 0">
					<h3>Aanwezig</h3>
					Momenteel is er niemand aanwezig op uw post
				</div>
			</div>

			<!-- Verwachte teams -->
			<div data-bind="visible: type() != 'Start' && verwachte_teams().length > 0">
				<h3>Verwacht</h3>
				<table class="table">
					<thead>
						<tr>
							<th>Nr</th>
							<th>Naam</th>
							<th>Roete</th>
							<th>Vertokke op vorige post</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: verwachte_teams" data-bind="css: {strikeout: team.canceled, own_group: team.own_group}">
						<tr>
							<td data-bind="text: team.nummer"></td>
							<td data-bind="text: team.team"></td>
							<td data-bind="text: team.route.naam"></td>
							<td data-bind="timer: team.route.laatste_checkout().checkout"></td>
							<td>
								<button class="btn btn-sm btn-secondary" data-bind="click: team.popup_checkin">Check in</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div data-bind="visible: verwachte_teams().length == 0 && type() != 'Start'">
				<h3>Verwacht</h3>
				Volgens de vertrektijden is er niemand verwacht op uw post, maar er kunnen ploegen aankomen die niet op de vorige post zijn geweest.
			</div>

			<!-- Post acties -->
			<div class="btn-toolbar" role="toolbar" aria-label="Post acties">
			<div class="btn-group mr-2" role="group" data-bind="visible: type() == 'Start'">
					<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Check uit voor een ander team
					</button>
					<div class="dropdown-menu">
						<!-- ko foreach: $root.subcamp -->
						<h6 class="dropdown-header" data-bind="text: name, visible: teams().length > 0"></h6>
						<!-- ko foreach: teams -->
						<a class="dropdown-item" href="#" data-bind="click: popup_checkout, css: {strikeout: canceled, own_group: own_group}">
							<span data-bind="text: nummer"></span>.
							<span data-bind="text: team"></span>
						</a>
						<!-- /ko -->
						<!-- /ko -->
					</div>
				</div>
				<div class="btn-group mr-2" role="group" data-bind="visible: type() != 'Start'">
					<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Check in voor een ander team
					</button>
					<div class="dropdown-menu">
						<!-- ko foreach: $root.subcamp -->
						<h6 class="dropdown-header" data-bind="text: name, visible: teams().length > 0"></h6>
						<!-- ko foreach: teams -->
						<a class="dropdown-item" href="#" data-bind="click: popup_checkin, css: {strikeout: canceled, own_group: own_group}">
							<span data-bind="text: nummer"></span>.
							<span data-bind="text: team"></span>
						</a>
						<!-- /ko -->
						<!-- /ko -->
					</div>
				</div>
				<div class="btn-group mr-2" role="group" data-bind="visible: type() != 'Finish'">
					<button type="button" class="btn btn-info" data-bind="click: popup_vertrokken">Toon vertrokke teams</button>
				</div>
				<div class="btn-group mr-2" role="group" data-bind="visible: $root.post_auth_count() > 1">
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Wissel van post
				</button>
					<div class="dropdown-menu">
						<!-- ko foreach: $root.posten -->
						<a class="dropdown-item" href="#" data-bind="visible: auth,click: $root.current_post">
							<span data-bind="text: naam"></span>
						</a>
						<!-- /ko -->
					</div>
				</div>
				<?php if(in_array('hike.formulier', $login_user['auth'])) { ?>
				<div class="btn-group mr-2" role="group" data-bind="with: $root.current_onderdeel">
					<button class="btn btn-secondary" data-bind="click: posten_printen">
						Formulier printen
						<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" data-bind="visible: $root.pdf_loading"></span>
					</button>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</script>
<?php } ?>

<?php if(in_array('team.inschrijving', $login_user['auth'])) { ?>
<script type="text/html" id="team_inschrijving">
	<div class="heading">
		<h3 class="username" data-bind="with: $root.user_user"><span data-bind="text: username"></span></h3>
		<h3>Niet ingescreven teams</h3>
	</div>
	<div class="body">
		<div class="progress" data-bind="foreach: {data:$root.ingeschreven_teams, as:'item'}">
			<div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" data-bind="style: {width: item.value+'%' }, attr:{'aria-valuenow': item.value }, css: {active: item.animated, 'progress-bar-striped': item.striped}, class: 'bg-'+item.type"></div>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th>Nr</th>
					<th>Naam</th>
					<th>Groep</th>
					<th>Speltak</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody data-bind="foreach: $root.teams">
				<tr data-bind="hidden: is_compleet, css: {strikeout: canceled, own_group: own_group}">
					<td data-bind="text: nummer"></td>
					<td data-bind="text: team"></td>
					<td data-bind="with: group"><span data-bind="text: name"></span></td>
					<td data-bind="text: speltak"></td>
					<td>
						<button class="btn btn-sm btn-secondary" data-bind="click: popup_inschrijving">Schrijf in</button>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="btn-toolbar" role="toolbar" aria-label="team acties">
			<div class="btn-group mr-2" role="group">
				<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Schrijf ander team in
				</button>
				<div class="dropdown-menu">
					<!-- ko foreach: $root.subcamp -->
					<h6 class="dropdown-header" data-bind="text: name, visible: teams().length > 0"></h6>
					<!-- ko foreach: teams -->
					<a class="dropdown-item" href="#" data-bind="click: popup_inschrijving, css: {strikeout: canceled, own_group: own_group}">
						<span data-bind="text: nummer"></span>.
						<span data-bind="text: team"></span>
					</a>
					<!-- /ko -->
					<!-- /ko -->
				</div>
			</div>
			<div class="btn-group mr-2" role="group">
				<button type="button" class="btn btn-info" aria-haspopup="true" aria-expanded="false" data-bind="click: $root.popup_new_team">
					Nieuw team
				</button>
			</div>
		</div>
	</div>
</script>
<?php } ?>

<?php if(in_array('rsw.overzicht', $login_user['auth'])) { ?>
<script type="text/html" id="rsw_overzicht">
	<div class="heading">
		<h3 class="username" data-bind="with: $root.user_user"><span data-bind="text: username"></span></h3>
		<h3>Score overzicht</h3>
	</div>
	<div class="body">
		<div class="row m-1" data-bind="foreach: $root.subcamp">
			<div class="m-1" data-bind="visible: teams().length > 0">
				<h4 data-bind="text: name"></h4>
				<table class="table table-striped table-responsive">
					<thead>
						<tr>
							<th>Nr</th>
							<th>Naam</th>
							<th>Punten</th>
							<th>Opm.</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: teams">
						<tr data-bind="css: {strikeout: canceled, own_group: own_group}">
							<td data-bind="text: nummer"></td>
							<td data-bind="text: team"></td>
							<td>
								<button class="btn btn-sm btn-secondary" data-bind="click: popup_score">
									<span data-bind="text: totaal_score_round"></span>
								</button>
							</td>
							<td>
								<button class="btn btn-sm btn-info" data-bind="visible: opmerkingen().length > 0, click: popup_opmerkingen">
									<span data-bind="text: opmerkingen().length"></span>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</script>
<?php } ?>

<?php if(in_array('system.admin', $login_user['auth'])) { ?>
<script type="text/html" id="tools_overzicht">
	<div class="heading">
		<h3 class="username" data-bind="with: $root.user_user"><span data-bind="text: username"></span></h3>
		<h3>Tools</h3>
	</div>
	<div class="body">
		<p>Op deze pagina staan handige tools die te gebruiken zijn tijdens de RSW</p>
		<div>
			<h5>Lijsten</h5>
			<?php if(in_array('qrcodes.teams', $login_user['auth'])) { ?>
			<button type="button" class="btn btn-success" data-bind="click: $root.qrcodes_printen_teams">
				QR'codes teams
				<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" data-bind="visible: $root.pdf_loading"></span>
			</button>
			<?php } ?>

			<?php if(in_array('qrcodes.rsw', $login_user['auth'])) { ?>
			<div class="btn-group">
				<button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					QR'codes onderdelen
					<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" data-bind="visible: $root.pdf_loading"></span>
				</button>
				<div class="dropdown-menu" data-bind="foreach: $root.onderdelen">
					<a class="dropdown-item" data-bind="click: qrcodes_printen,text: naam">[NAAM]</a>
				</div>
			</div>
			<?php } ?>

			<?php if(in_array('qrcodes.users', $login_user['auth'])) { ?>
			<button type="button" class="btn btn-danger" data-bind="click: $root.qrcodes_printen_users">
				QR'codes users
				<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" data-bind="visible: $root.pdf_loading"></span>
			</button>
			<?php } ?>
		</div>
		<div>
			<h5>Formulieren</h5>
			<?php if(in_array('rsw.tentbriefjes', $login_user['auth'])) { ?>
			<button type="button" class="btn btn-secondary" data-bind="click: $root.pdf_tentbriefjes">
				Tentinspectie briefjes
				<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" data-bind="visible: $root.pdf_loading"></span>
			</button>
			<?php } ?>
			<?php if(in_array('rsw.formulier', $login_user['auth'])) { ?>
			<div class="btn-group" role="group">
				<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Score formulieren
					<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" data-bind="visible: $root.pdf_loading"></span>
				</button>
				<div class="dropdown-menu" data-bind="foreach: $root.onderdelen">
					<a class="dropdown-item" data-bind="click: posten_printen,text: naam">[NAAM]</a>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</script>
<?php } ?>

<?php if(in_array('klassement.overzicht', $login_user['auth'])) { ?>
<script type="text/html" id="klassement_overzicht">
	<div class="heading">
		<h3 class="username" data-bind="with: $root.user_user"><span data-bind="text: username"></span></h3>
		<h3>Klassement</h3>
	</div>
	<div class="body">
		<table class="table table-striped table-responsive">
			<thead>
				<tr>
					<th>Plaats</th>
					<th>#</th>
					<th>Ploeg</th>
					<th>Groep</th>
					<!-- ko foreach: $root.onderdelen -->
					<th data-bind="text: naam"></th>
					<!-- /ko -->
					<th>Joker</th>
					<th>Totaal</th>
				</tr>
			</thead>
			<tbody data-bind="foreach: $root.klassement">
				<tr data-bind="css: {strikeout: canceled, own_group: own_group}">
					<td data-bind="text: plaats"></td>
					<td data-bind="text: nummer"></td>
					<td data-bind="text: team"></td>
					<td data-bind="with: group"><span data-bind="text: name"></span></td>
					<!-- ko foreach: onderdelen -->
					<td data-bind="text: posten_score_round"></td>
					<!-- /ko -->
					<td data-bind="text: joker_score_round"></td>
					<td>
						<button class="btn btn-sm btn-secondary" data-bind="click: popup_score">
							<span data-bind="text: totaal_score_round"></span>
						</button>
						<span class="hidden-selectable" data-bind="text: totaal_score_round"></span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</script>
<?php } ?>

<div class="tabbable" data-bind="visible: tabs().length > 1">
	<!-- Only required for left/right tabs -->
	<ul class="nav nav-tabs" data-bind="foreach: tabs">
		<li class="nav-link" data-bind="css: {active: active, 'bg-secondary': active}, class: 'bg-'+color">
			<a href="#" data-bind="click: $root.activate_tab, text: title, css: {'text-dark': active}"></a>
		</li>
	</ul>
	<div class="tab-content" data-bind="foreach: tabs">
		<div class="tab-pane" data-bind="css: {active: active}, template:{name: template }"></div>
	</div>
</div>

<div class="tabbable" data-bind="visible: tabs().length == 1">
	<div class="tab-content" data-bind="foreach: tabs">
		<div class="tab-pane" data-bind="css: {active: active}, template:{name: template }"></div>
	</div>
</div>
