<?php

$template_replace['POPUP'] = <<<EOF_POPUP

EOF_POPUP;

?>
<div class="row" data-bind="foreach: subkamp">
	<div class="" data-bind="visible: teams().length > 0">
		<h4 data-bind="text: naam"></h4>
		<table class="table table-striped table-responsive">
			<thead>
				<tr>
					<th>Nr</th>
					<th>Team</th>
					<th>laaste actie</th>
					<th>
						<span class="rounded bg-default p-1" data-toggle="tooltip" title="Niet bezocht">N</span>
						<span class="rounded bg-warning p-1" data-toggle="tooltip" title="Op post">O</span>
						<span class="rounded bg-success p-1" data-toggle="tooltip" title="Vertrokke">V</span>
						<span class="rounded bg-danger p-1" data-toggle="tooltip" title="Tijden kloppen niet">E</span>
					</th>
					<th>Totaal Post</th>
					<th>Totaal Lopen</th>
					<th>Totaal</th>
				</tr>
			</thead>
			<tbody data-bind="foreach: teams">
				<tr>
					<td data-bind="text: nummer"></td>
					<td data-bind="text: team"></td>
					<td data-bind="text: route.laaste_actie"></td>
					<td data-bind="click: popup_tijden, foreach: route">
						<span class="mw-10 rounded p-1" data-toggle="tooltip" data-bind="visible: post.type() == 'Post', text: '#', attr:{'title': '#'}, css:'bg-'+item_state()"></span>
					</td>
					<td data-bind="time: totaal_op_post"></td>
					<td data-bind="time: totaal_gelope"></td>
					<td data-bind="time: totaal_op_post() + totaal_gelope()"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
