<?php

require_once(__DIR__.'/TCPDF/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class QR_PDF extends TCPDF {
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $img_file = 'css/back.jpg';
        //$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

    public function Footer() {

    }
}

function add_qrcode($pdf, $token, $name, $x=0, $y=0, $w=50, $h=50) {
	// set style for barcode
	$style = array(
		'border' => 2,
		'vpadding' => 'auto',
		'hpadding' => 'auto',
		'fgcolor' => array(0,0,0),
		'bgcolor' => false, //array(255,255,255)
		'module_width' => 1, // width of a single module in points
		'module_height' => 1 // height of a single module in points
	);

	$url = $_SERVER['HTTP_HOST'].'/'.$token;
	$pdf->write2DBarcode($url, 'QRCODE,M', $x, $y, $w, $h, $style, 'N');

	$pdf->SetFont('helvetica', '', 10);
	$pdf->Text($x, $y+$h-5, $name);
}

function print_qrcodes($tokens=[]) {
    global $return_data;

	// create new PDF document
	$pdf = new QR_PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Administratie');
	$pdf->SetTitle('QRCodes');
	$pdf->SetSubject('QRCodes');
	$pdf->SetKeywords('QRCodes');

	// set header and footer fonts
	$pdf->setHeaderFont(false);
	$pdf->setFooterFont(false);

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	$min_size = 50;
	$marge = 0;
	$padding = 8;
	$x = 0;
	$y = $pdf->getPageHeight()+$padding+1;
	$max_y = 0;

	$pdf->SetMargins($padding, PDF_MARGIN_TOP, $padding);

	foreach ($tokens as $token=>$name) {
		$size = max($min_size, 2*strlen($token));

		if($x+$size+$padding > $pdf->getPageWidth() - $padding) {
			$x = $padding;
			$y += $max_y+$marge;
			$max_y = 0;
		}

		if($y+$size+$marge > $pdf->getPageHeight() - $padding) {
			$pdf->AddPage();
			$x = $padding;
			$y = $padding;
			$max_y = 0;
		}

		add_qrcode($pdf, $token, $name, $x, $y, $size, $size);
		$x += $size + $marge;
		$max_y = max($max_y, $size);
	}

    //Close and output PDF document
    $return_data[] = ['pdf'=>['name'=>'QRCodes.pdf', 'file'=>base64_encode($pdf->Output('', 'S'))]];
}
