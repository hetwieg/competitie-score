<?php
$template_replace['POPUP'] .= <<<EOF_POPUP
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_hike_berekening }">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="hike-tijden-label">
					Hike berekening: <span data-bind="text: nummer"></span>. <span data-bind="text: team">[team.team]</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<pre>
minimaal: 0
maximaal: [max_punten_tijd]

([max_punten_tijd] / ([max_tijd] - [min_tijd])) * ([max_tijd] - <b>[loop_tijd]</b>)
- (<b>[posten_skiped]</b> * [strafpunten_per_post])

----------

+ (<b>[posten]</b> * [punten_per_post])

---------------------------------------------------------------

minimaal: 0
maximaal: <span data-bind="text: \$root.hikeMaxPuntenLopen"></span>

(<span data-bind="text: \$root.hikeMaxPuntenLopen"></span> / (<span data-bind="date: \$root.hikeMaxTijd,dateFormat:'HH:mm'"></span> - <span data-bind="date: \$root.hikeMinTijd,dateFormat:'HH:mm'"></span>)) * (<span data-bind="date: \$root.hikeMaxTijd,dateFormat:'HH:mm'"></span> - <span data-bind="time: totaal_gelope"></span>) = <b data-bind="text: hike_tijd_score_round"></b>
 - (<span data-bind="text: hike_posten_checkin_skiped">[posten_skiped]</span> * <span data-bind="text: \$root.hikePostMinPunten"></span>) = <b data-bind="text: hike_posten_checkin_skiped_score"></b>

----------

 + (<span data-bind="text: hike_posten_checkin">[posten]</span> * <span data-bind="text: \$root.hikePostPunten"></span>) = <b data-bind="text: hike_posten_checkin_score"></b>

---------------------------------------------------------------

<b data-bind="text: hike_totaal_score"></b>

</pre>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
EOF_POPUP;
?>


<div class="heading">
		<h3 class="username" data-bind="with: $root.user_user"><span data-bind="text: username"></span></h3>
		<h3>Hike punten</h3>
	</div>
	<div class="body">
		<h4>Roetes</h4>
		<table class="table table-striped table-responsive">
			<thead>
				<tr>
					<th>Naam</th>
					<th>snelste gelope</th>
					<th>gemidelt gelope</th>
					<th>langste gelope</th>
				</tr>
			</thead>
			<tbody data-bind="foreach: $root.routes">
				<tr>
					<td data-bind="text: naam">[naam]</td>
					<td><span data-bind="time: snelste_gelope, hidden: snelste_gelope() < 0">[time]</span><span data-bind="visible: snelste_gelope() < 0">&infin;</span></td>
					<td data-bind="time: avg_gelope_round">[time]</td>
					<td data-bind="time: langste_gelope">[time]</td>
				</tr>
			</tbody>
		</table>

		<h4>Punte berekene</h4>
		<div class="form-group"><label>Min tijd:</label><input type="text" class="form-control" data-bind="date: $root.hikeMinTijd,dateFormat:'HH:mm'" /></div>
		<div class="form-group"><label>Max tijd:</label><input type="text" class="form-control" data-bind="date: $root.hikeMaxTijd,dateFormat:'HH:mm'" /></div>
		<div class="form-group"><label>Max punten hike:</label><input type="number" class="form-control" data-bind="value: $root.hikeMaxPunten" /></div>
		<div class="form-group"><label>Max punten tijd:</label><input type="number" class="form-control" data-bind="value: $root.hikeMaxPuntenLopen" /></div>
		<div class="form-group"><label>Punten per post:</label><input type="number" class="form-control" data-bind="value: $root.hikePostPunten" /></div>
		<div class="form-group"><label>Strafpunten per post:</label><input type="number" class="form-control" data-bind="value: $root.hikePostMinPunten" /></div>

		<h4>Overzicht</h4>
		<table class="table table-striped table-responsive">
			<thead>
				<tr>
					<th>Nr</th>
					<th>Naam</th>
					<th>Route</th>
					<!-- ko foreach: $root.posten.punten -->
					<th data-bind="css: {'allow_skip': allow_skip}, visible: type() != 'Start'">Aankomst <span data-bind="text: naam"></span></th>
					<th data-bind="css: {'allow_skip': allow_skip}, visible: (type() != 'Finish' && type() != 'Tag')">Vertrokke <span data-bind="text: naam"></span></th>
					<!-- /ko -->
					<th>Totaal Lopen</th>
					<th>Posten</th>
					<th>Vragen</th>
					<th>Totaal</th>
					<th>Opslaan</th>
				</tr>
			</thead>
			<tbody data-bind="foreach: $root.teams_order_hike_totaal">
				<tr data-bind="css: {strikeout: canceled}">
					<td data-bind="text: nummer"></td>
					<td data-bind="text: team"></td>
					<td data-bind="text: route.naam"></td>

					<!-- ko foreach: route.punten -->
					<td data-bind="visible: post.type() != 'Start', date: checkin, dateFormat: 'HH:mm'"></td>
					<td data-bind="visible: (post.type() != 'Finish' && post.type() != 'Tag'), date: checkout, dateFormat: 'HH:mm'"></td>
					<!-- /ko -->

					<td>
						<!-- ko if: $root.auth.indexOf('hike.correctie') > -1 -->
						<button type="button" class="btn btn-sm btn-secondary" data-bind="click: popup_tijden, time: totaal_gelope"></button>
						<!-- /ko -->
						<!-- ko ifnot: $root.auth.indexOf('hike.correctie') > -1 -->
						<span data-bind="time: totaal_gelope"></span>
						<!-- /ko -->
					</td>
					<td data-bind="text: hike_posten_checkin"></td>
					<td>
						<button type="button" class="btn btn-sm btn-info" data-bind="visible: route.vragen().length > 0, text: vragen_score, click: popup_hike_vragen"></button>
					</td>
					<td>
						<button type="button" class="btn btn-sm btn-success" data-bind="text: hike_totaal_score_round, click: popup_hike_berekening"></button>
					</td>
					<td>
						<input type="number" class="form-control" data-bind="value: hike_punten" />
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
