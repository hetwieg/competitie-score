<?php
require_once(__DIR__.'/TCPDF/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $img_file = 'css/back.jpg';
        //$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

    public function Footer() {

    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Administratie');
$pdf->SetTitle('Inschrijving '.$team['naam']);
$pdf->SetSubject('Inschrijving '.$team['naam']);
$pdf->SetKeywords('Inschrijving, '.$team['naam']);

// set header and footer fonts
$pdf->setHeaderFont(false);
$pdf->setFooterFont(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// NOTE: 2D barcode algorithms must be implemented on 2dbarcode.php class file.

// set font
$pdf->SetFont('helvetica', '', 11);

// add a page
$pdf->AddPage();


$tbl = <<<EOD
Een brief die jij zelf wil :) {%naam}
EOD;

foreach ($team as $k => $v) {
	$tbl = str_replace('{%'.$k.'}', $v, $tbl);
}
foreach ($args as $k => $v) {
	$tbl = str_replace('{!'.$k.'}', $v, $tbl);
}

$pdf->writeHTML($tbl, true, false, false, false, '');

// set style for barcode
$style = array(
    'border' => 2,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

$url = $_SERVER['HTTP_HOST'].'/'.$team['token'];
$pdf->write2DBarcode($url, 'QRCODE,M', 150, 230, 50, 50, $style, 'N');
$pdf->Text(150, 275, $url);

//Close and output PDF document
$return_data[] = ['pdf'=>['name'=>'Inschrijving.pdf', 'file'=>base64_encode($pdf->Output('', 'S'))]];
