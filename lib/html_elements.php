<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css" />
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css" />

    <title>Hello, world!</title>
  </head>
  <body>

<!-- Popup om team tijden aan te passen? -->
<div role="dialog" id="hike-tijden" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_post_tijden }">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="hike-tijden-label">
					Tijden: <span data-bind="text: naam">[team.naam]</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Aankomst</th>
							<th>Vertrek</th>
							<th>Score</th>
							<th>Gelopen</th>
							<th>Post</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: route">
						<tr data-bind="css:'table-'+item_state()">
							<td data-bind="text: post.naam"></td>
							<td><input type="text" class="form-control" data-bind="visible: post.type() != 'Start', date: checkin, dateFormat:'HH:mm'" /></td>
							<td><input type="text" class="form-control" data-bind="visible: post.type() != 'Finish', date: checkout, dateFormat:'HH:mm'" /></td>
							<td><input type="number" class="form-control" min="0" data-bind="visible: post.max_punten() > 0, value: score, attr:{'max':post.max_punten()}" /></td>
							<td><span data-bind="visible: post.type() != 'Start', time: tijd_gelope_naar_post"></span></td>
							<td><span data-bind="visible: post.type() == 'Post', time: tijd_op_post"></span></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>Totaal:</th>
							<th colspan="2"></th>
							<th><span data-bind="text: totaal_hike_score"></span></th>
							<th><span data-bind="time: totaal_gelope"></span></th>
							<th><span data-bind="time: totaal_op_post"></span></th>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="modal-footer">
				<span class="rounded bg-default">Niet bezocht</span>
				<span class="rounded bg-warning">Op post</span>
				<span class="rounded bg-success">Vertrokke</span>
				<span class="rounded bg-danger">Tijden kloppen niet</span>
			</div>
		</div>
	</div>
</div>

<!-- Popup met alle vertrokken teams -->
<div role="dialog" id="post-vertrokken" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_post_vertrokken }">
	<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content" data-bind="with: current_post">
			<div class="modal-header">
				<h5 class="modal-title">Vertrokken teams</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table">
					<thead>
						<tr>
							<th>Nr</th>
							<th>Naam</th>
							<th data-bind="visible: $root.current_post().max_punten() > 0">Score</th>
							<th>Roete</th>
							<th>Wanneer</th>
						</tr>
					</thead>
					<tbody data-bind="foreach: vertrokken_teams">
						<tr>
							<td data-bind="text: team.nummer"></td>
							<td data-bind="text: team.naam"></td>
							<td data-bind="text: score, visible: $root.current_post().max_punten() > 0"></td>
							<td data-bind="text: team.route.naam"></td>
							<td data-bind="timer: checkout"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>

<div role="dialog" id="post-checkin" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_post_checkin }">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="post-checkin-label">Check in: <span data-bind="text: naam"></span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Door op check in te klikken word <b data-bind="text: naam"></b> aan uw post toegevoegd.
				<div class="mt-2" data-bind="visible: ingecheckt_current_post">
					<b class="text-danger">Dit team is <span data-bind="timer: ingecheckt_current_post"></span> op uw post ingecheckt</b><br />
					Door dit team opnieuw op uw post in te chekken word de vorige tijd van check in verwijdert
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Annuleren</button>
				<button type="button" class="btn btn-success" data-bind="click: send_checkin">Check in</button>
			</div>
		</div>
	</div>
</div>

<div role="dialog" id="post-checkout" aria-hidden="true" class="modal fade" data-bind="modal: { show: popup_post_checkout }">
	<div class="modal-dialog" role="document">
		<div class="modal-content" data-bind="with: current_team">
			<div class="modal-header">
				<h5 class="modal-title" id="post-vertrek-label">Check uit voor:
					<span data-bind="text: naam"></span>
					(<span data-bind="text: route.naam"></span>)
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row" data-bind="visible: $root.current_post().max_punten() > 0">
					<label class="col-sm-2 col-form-label" for="team_score_vertrokke">Punten</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" id="team_score_vertrokke" aria-describedby="team_score_vertrokkeHelp" placeholder="" min="0" data-bind="value: checkout_score, attr:{'max':$root.current_post().max_punten()}">
						<small id="team_score_vertrokkeHelp" class="form-text text-muted">
							Punten behaald voor opdrachten op deze post
						</small>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
					Annuleren
				</button>

				<button type="button" class="btn btn-success" data-bind="click: send_checkout">Check uit</button>
			</div>
		</div>
	</div>
</div>

<div class="" data-bind="with: current_post">
	<div class="heading">
		<h2 data-bind="text: naam"></h2>
	</div>

	<div class="body">
		<!-- Aanwezige teams -->
		<div data-bind="visible: aanwezige_teams().length > 0">
			<h3>Aanwezig</h3>
			<table class="table">
				<thead>
					<tr>
						<th>Nr</th>
						<th>Naam</th>
						<th>Roete</th>
						<th data-bind="visible: type() != 'Start'">Tijd</th>
						<th data-bind="visible: type() != 'Finish'">Check uit</th>
					</tr>
				</thead>
				<tbody data-bind="foreach: aanwezige_teams">
					<tr>
						<td data-bind="text: team.nummer"></td>
						<td data-bind="text: team.naam"></td>
						<td data-bind="text: team.route.naam"></td>
						<td data-bind="timer: checkin, visible: $parent.type() != 'Start'"></td>
						<td data-bind="visible: $parent.type() != 'Finish'">
							<button type="button" class="btn btn-sm btn-secondary" data-bind="click: team.popup_checkout">Check uit</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div data-bind="visible: aanwezige_teams().length == 0">
			<h3>Aanwezig</h3>
			Momenteel is er niemand aanwezig op uw post
		</div>

		<!-- Verwachte teams -->
		<div data-bind="visible: type() != 'Start' && verwachte_teams().length > 0">
			<h3>Verwacht</h3>
			<table class="table">
				<thead>
					<tr>
						<th>Nr</th>
						<th>Naam</th>
						<th>Roete</th>
						<th>Vertokke op vorige post</th>
						<th>Check in</th>
					</tr>
				</thead>
				<tbody data-bind="foreach: verwachte_teams">
					<tr>
						<td data-bind="text: team.nummer"></td>
						<td data-bind="text: team.naam"></td>
						<td data-bind="text: team.route.naam"></td>
						<td data-bind="timer: team.route.laatste_checkout().checkout"></td>
						<td><button class="btn btn-sm btn-secondary" data-bind="click: team.popup_checkin">Check in</button></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div data-bind="visible: verwachte_teams().length == 0 && type() != 'Start'">
			<h3>Verwacht</h3>
			Volgens de vertrektijden is er niemand verwacht op uw post, maar er kunnen ploegen aankomen die niet op de vorige post zijn geweest.
		</div>

		<!-- Post acties -->
		<div class="btn-toolbar" role="toolbar" aria-label="Post acties">
			<div class="btn-group mr-2" role="group" data-bind="visible: type() != 'Start'">
				<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Check in voor een ander team
				</button>
				<div class="dropdown-menu">
					<!-- ko foreach: $root.subkamp -->
					<h6 class="dropdown-header" data-bind="text: naam"></h6>
					<!-- ko foreach: teams -->
					<a class="dropdown-item" href="#" data-bind="click: popup_checkin">
						<span data-bind="text: nummer"></span>.
						<span data-bind="text: naam"></span>
					</a>
					<!-- /ko -->
					<!-- /ko -->
				</div>
			</div>

			<div class="btn-group mr-2" role="group" data-bind="visible: type() != 'Finish'">
				<button type="button" class="btn btn-info" data-bind="click: popup_vertrokken">Toon vertrokke teams</button>
			</div>

			<div class="btn-group mr-2" role="group">
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Wissel van post
				</button>
				<div class="dropdown-menu">
					<!-- ko foreach: $root.posten -->
					<a class="dropdown-item" href="#" data-bind="click: $root.current_post">
						<span data-bind="text: naam"></span>
					</a>
					<!-- /ko -->
				</div>
			</div>
		</div>
	</div>
</div>

    <script src="/js/jquery-3.4.1.js"></script>
    <script src="/js/moment-with-locales.js"></script>
    <script src="/js/bootstrap.bundle.js"></script>
    <script src="/js/bootstrap-datetimepicker.js"></script>
    <script src="/js/knockout-3.5.0.js"></script>
    <script src="/js/knockout-bindings.js"></script>
    <script src="/js/app.js"></script>
  </body>
</html>
