<?php



?>

<div data-bind="with: user_team">
	<div data-bind="visible: need_team_name">
		<div class="heading">
			<h2>Nieuw team</h2>
		</div>

		<div class="body">
			Wij hebben nog geen naam van jullie team, vul jullie naam hier onder in.

			<div class="form-group">
				<input type="text" id="new_team_name" class="form-control" data-bind="value: new_team_name" />
			</div>

			<div class="form-group">
				<button type="button" class="btn btn-primary" data-bind="click: send_team_name">Opslaan</button>
			</div>
		</div>
	</div>
	<div data-bind="visible: !need_team_name()">
		<div class="heading">
			<h2 data-bind="text: team"></h2>
		</div>

		<div class="body">
			Op de volgende posten ben je geweest,
			<ul data-bind="foreach: route" style="list-style-type: none;">
				<li data-bind="visible: (post.type() == 'Post' || post.type() == 'Tag') && checkin">
					<span class="badge" data-bind="text: '#', css: 'badge-'+item_state()"></span>
					<span data-bind="text: post.naam"></span>
					<?php  if($config['score']['hike']){ echo '<span data-bind="visible: score() > 0"> (<span data-bind="text: score"></span> punten)</span>'; } ?>
				</li>
			</ul>

			<?php  if($config['score']['hike']){ echo '<div data-bind="visible: totaal_hike_score() > 0">Jullie hebben <b data-bind="text: totaal_hike_score"></b> punten</div>'; } ?>

			Het laatste wat we gezien hebben is: <code data-bind="text: route.laaste_actie"></code>

		<!-- Vragenlijst -->
		<div class="mt-3" data-bind="visible: route.vragen().length > 0">
			<h4>Vragen</h4>

			<div data-bind="foreach: route.vragen">
				<div class="form-group" data-bind="visible: visible">
					<label for="andwoord">
						<b data-bind="text: vraag.naam"></b>
						<span data-bind="text: vraag.vraag"></span>
					</label>
					<input type="text" class="form-control" id="andwoord" data-bind="value: antwoord" />
				</div>
			</div>

			<button type="button" class="btn btn-primary" data-bind="click: send_hike_vragen">
				Insturen
			</button>
		</div>

		<div class="noodenvelop" data-bind="hidden: $root.envelope_open">
			<p><b>Noodenvelop:</b></p>
			<p>
				<button type="button" class="btn btn-sm btn-info" data-bind="click: $root.open_envelope">
					Openen
				</button>
			</p>
		</div>
		<div class="noodenvelop" data-bind="visible: $root.envelope_open">
			<p><b>Noodenvelop:</b></p>
			<p><?php echo $config['noodenvelop']; ?></p>
			<p>
				Als het nodig is kan er gevraacht worden om je GPS locatie te sturen
				<button type="button" class="btn btn-sm btn-success" data-bind="click: $root.activate_gps">
					Stuur locatie
				</button>
			</p>
		</div>
	</div>
</div>
