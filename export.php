<?php

require('config.php');
require_once('lib/database.php');

require_once('login.php');

if(!in_array('rsw.export', $login_user['auth'])) {
	header("location: /");
	die('Go back');
}

function csvstr(array $fields) : string
{
	$f = fopen('php://memory', 'r+');
	if (fputcsv($f, $fields, ";") === false) {
		return false;
	}
	rewind($f);
	$csv_line = stream_get_contents($f);
	return rtrim($csv_line);
}

$filename = 'export.csv';
$header = ['Ploeg'];
$rows = [];

$teams = [];
{
	$stmt_team = $db->prepare("SELECT `id`, `team`, `nummer`, `subcamp_id`, `speltak`, `groep`, `contact`, `canceled`, `token`, `route_id`, `rsw_joker` FROM `team`;");
	$stmt_team->execute();

	foreach($stmt_team->fetchAll() as $team) {
		$teams[$team['id']] = $team;
	}
}

if(array_key_exists('score', $_REQUEST)) {
	$stmt_group = $db->prepare("SELECT * FROM `rsw_group` WHERE `naam` like :naam;");
	$stmt_group->execute(['naam'=>$_REQUEST['score']]);
	$group = false;
	foreach($stmt_group->fetchAll() as $row_group) {
		$group = $row_group;
	}

	if(!$group) {
		die('Scores niet gevonden: '.$_REQUEST['score']);
	}
	$filename = $group['naam'].'.csv';

	$stmt_post = $db->prepare("SELECT *
		FROM `rsw_post` p
		WHERE p.`groep_id` = :group_id
		ORDER BY p.`id` ASC;");

	$header[] = 'Totaal';
	$stmt_post->execute(['group_id'=>$group['id']]);
	foreach($stmt_post->fetchAll() as $row_post) {
		$header[] = $row_post['naam'];
	}

	$stmt_score = $db->prepare("SELECT p.`id` as post_id,
		(select `score` from `rsw_score` where `post_id`=  p.`id` and `team_id` = :team_id order by `id` desc limit 1) AS `score`
		FROM `rsw_post` p
		WHERE p.`groep_id` = :group_id
		ORDER BY p.`id` ASC;");

	foreach ($teams as $team_id => $team) {
		$stmt_score->execute([
			'team_id'=>$team_id,
			'group_id'=>$group['id'],
		]);

		$totaal = 0;
		$line = [];
		foreach($stmt_score->fetchAll() as $row_score) {
			$line[] = $row_score['score'];
			$totaal += $row_score['score'];
		}
		if($totaal > 0) {
			$rows[] = array_merge([$team['nummer'], $totaal], $line);
		}
	}
}

elseif(array_key_exists('special', $_REQUEST)) {
	switch ($_REQUEST['special']) {
		case 'joker':
			$filename = 'Joker.csv';
			$stmt_joker = $db->prepare("SELECT * FROM `rsw_joker`;");
			$stmt_joker->execute();

			$jokers = [];
			foreach($stmt_joker->fetchAll() as $row_joker) {
				$jokers[$row_joker['id']] = $row_joker['naam'];
			}

			$header[] = 'Joker';

			foreach ($teams as $id => $team) {
				if(array_key_exists($team['rsw_joker'], $jokers)) {
					$rows[] = [
						$team['nummer'],
						$jokers[$team['rsw_joker']],
					];
				}
			}
			break;
	}
}

header('Content-Encoding: UTF-8');
header('Content-type: text/csv; charset=UTF-8');
header("Content-Disposition: attachment; filename=".$filename);
header("Pragma: no-cache");
header("Expires: 0");
echo "\xEF\xBB\xBF"; // UTF-8 BOM

echo csvstr($header)."\n";
foreach ($rows as $row) {
	echo csvstr($row)."\n";
}
