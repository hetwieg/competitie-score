<?php

// https://gist.github.com/cp6/ce5d8c73ce91d55bb874fba1bf30ba25

require('config.php');
require_once('login.php');

if (!in_array('system.backup', $login_user['auth'])) {
	echo "No user cookie";
	die();
}

$backup_config = [
    'db'=>$config['db'],

    'INCLUDE_DROP_TABLE' => true,
    'TIMEZONE' => 'UTC',//Timezone for date format
    'COMPRESS' => array_key_exists('gz', $_REQUEST),//Compress into gz otherwise keep as .sql
];

backupDB($backup_config);

function backupDB(array $config)
{
    $db = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass']);
    $db->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_NATURAL);

    date_default_timezone_set($config['TIMEZONE']);
    $do_compress = $config['COMPRESS'];

    $save_string = tempnam(sys_get_temp_dir(), 'score_db_export_');
    if ($do_compress) {
        rename($save_string, $save_string .= '.sql.gz');
        $zp = gzopen($save_string, "a9");
    } else {
        rename($save_string, $save_string .= '.sql');
        $handle = fopen($save_string, 'a+');
    }

    //array of all database field types which just take numbers
    $numtypes = array('tinyint', 'smallint', 'mediumint', 'int', 'bigint', 'float', 'double', 'decimal', 'real');

    $return =  "-- ------------------------------------------------ \n";
    $return .= "-- Backup generated at ".date('Y-m-d\TH:i:s\Z')."\n";
    $return .= "-- ------------------------------------------------ \n\n";
    $return .= "CREATE DATABASE IF NOT EXISTS `{$config['db']['data']}`;\n";
    $return .= "USE `{$config['db']['data']}`;\n\n";
    $return .= "SET FOREIGN_KEY_CHECKS = 0;\n";
    $return .= "\n-- ------------------------------------------------ \n";

    //get all tables
    $pstm1 = $db->query('SHOW TABLES');
    while ($row = $pstm1->fetch(PDO::FETCH_NUM)) {
        $tables[] = $row[0];
    }

    if ($config['INCLUDE_DROP_TABLE']) {
        $return .= "\n";

        foreach ($tables as $table) {
            $return .= "DROP TABLE IF EXISTS `".$table."`;\n";
        }

        $return .= "\n-- ------------------------------------------------ \n";
    }

    //cycle through the table(s)
    foreach ($tables as $table) {
        $result = $db->query("SELECT * FROM $table");
        $num_fields = $result->columnCount();
        $num_rows = $result->rowCount();

        //table structure
        $pstm2 = $db->query("SHOW CREATE TABLE $table");
        $row2 = $pstm2->fetch(PDO::FETCH_NUM);
        $ifnotexists = str_replace('CREATE TABLE', 'CREATE TABLE IF NOT EXISTS', $row2[1]);
        $return .= "\n\n" . $ifnotexists . ";\n\n";

        if ($do_compress) {
            gzwrite($zp, $return);
        } else {
            fwrite($handle, $return);
        }
        $return = "";

        //insert values
        if ($num_rows) {
            $return = 'INSERT INTO `' . $table . '` (';
            $pstm3 = $db->query("SHOW COLUMNS FROM $table");
            $count = 0;
            $type = array();

            while ($rows = $pstm3->fetch(PDO::FETCH_NUM)) {
                if (stripos($rows[1], '(')) {
                    $type[$table][] = stristr($rows[1], '(', true);
                } else {
                    $type[$table][] = $rows[1];
                }

                $return .= "`" . $rows[0] . "`";
                $count++;
                if ($count < ($pstm3->rowCount())) {
                    $return .= ", ";
                }
            }

            $return .= ")" . ' VALUES';

            if ($do_compress) {
                gzwrite($zp, $return);
            } else {
                fwrite($handle, $return);
            }
            $return = "";
        }
        $counter = 0;
        while ($row = $result->fetch(PDO::FETCH_NUM)) {
            $return = "\n\t(";

            for ($j = 0; $j < $num_fields; $j++) {

                if (isset($row[$j])) {

                    //if number, take away "". else leave as string
                    if ((in_array($type[$table][$j], $numtypes)) && (!empty($row[$j]))) {
                        $return .= $row[$j];
                    } else {
                        $return .= $db->quote($row[$j]);
                    }
                } else {
                    $return .= 'NULL';
                }
                if ($j < ($num_fields - 1)) {
                    $return .= ',';
                }
            }
            $counter++;
            if ($counter < ($result->rowCount())) {
                $return .= "),";
            } else {
                $return .= ");";
            }
            if ($do_compress) {
                gzwrite($zp, $return);
            } else {
                fwrite($handle, $return);
            }
            $return = "";
        }

        $return = "\n\n-- ------------------------------------------------ \n\n";

        if ($do_compress) {
            gzwrite($zp, $return);
        } else {
            fwrite($handle, $return);
        }
        $return = "";
    }

    $error1 = $pstm2->errorInfo();
    $error2 = $pstm3->errorInfo();
    $error3 = $result->errorInfo();
    echo $error1[2];
    echo $error2[2];
    echo $error3[2];

    if ($do_compress) {
        gzclose($zp);
    } else {
        fclose($handle);
    }

    if ($do_compress) {
        header("Content-Type: " .mime_content_type($save_string));
        header("Content-Length: " .filesize($save_string));
        header("Content-Transfer-Encoding: binary");
        header('Content-Disposition: attachment; filename="score_db_'.date('Y-m-d\TH-i-s\Z').'.sql.gz"');
        header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
        header('Pragma: no-cache');

        if ($fp=fopen($save_string, "r")){
            fpassthru($fp);
        }
    }
    else {
        header('Content-Type: application/sql');
        header('Content-Disposition: attachment; filename="score_db_'.date('Y-m-d\TH-i-s\Z').'.sql"');
        header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
        header('Pragma: no-cache');

        if ($fp=fopen($save_string, "r")){
            fpassthru($fp);
        }
    }

    unlink($save_string);
}
