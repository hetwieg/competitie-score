ko.extenders.numeric = function (target, options) {
	//create a writable computed observable to intercept writes to our observable
	var result = ko.pureComputed({
		read: target,
		//always return the original observables value
		write: function (newValue) {
			var current = target();
      var newValueAsNum = isNaN(newValue) ? 0 : parseInt(+newValue, 10);
      var valueToWrite = newValueAsNum;

      if(options.round !== undefined) {
        var roundingMultiplier = Math.pow(10, options.round);
        valueToWrite = Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;
      }

			if (options.min !== undefined && newValueAsNum < options.min) {
				valueToWrite = options.min;
			}

			if (options.max !== undefined && newValueAsNum > options.max) {
				valueToWrite = options.max;
			}

			//only write if it changed
			if (valueToWrite !== current) {
				target(valueToWrite);
			}
      else {
				//if the rounded value is the same, but a different value was written, force a notification for the current field
				if (newValue !== current) {
					target.notifySubscribers(valueToWrite);
				}
			}
		}
	}).extend({ notify: 'always' });
	//initialize with current value to make sure it is rounded appropriately
	result(target());
	//return the new computed observable
	return result;
};

ko.bindingHandlers.modal = {
	init: function(element, optionsAccessor, allBindingsAccessor, viewModel, bindingContext) {
		var $element = $(element);

		if ($element.attr("tabindex") === undefined) {
			$element.attr("tabindex", -1);
		}

		$element.modal(ko.toJS(optionsAccessor()));

		$element.on("hidden.bs.modal", function() {
			var options = ko.utils.unwrapObservable(optionsAccessor());
			options.show(false);
		});

		$(document).on("keyup", function(e) {
			if (e.which === 27) {
				$element.modal("hide");
			}
		});

		//hide elements inside modal unless we are showing it
		var _showAccessor = showAccessor.bind(optionsAccessor, optionsAccessor);
		ko.bindingHandlers["if"].init(element, _showAccessor, allBindingsAccessor, viewModel, bindingContext);
	},
	update: function(element, optionsAccessor, allBindingsAccessor, viewModel, bindingContext) {
		var _showAccessor = showAccessor.bind(optionsAccessor, optionsAccessor);

		$(element).modal(_showAccessor() ? "show" : "hide");
	}
};

function showAccessor(accessor) {
	var options = ko.utils.unwrapObservable(accessor());
	return ko.utils.unwrapObservable(options.show);
}

function getDateFormat(type, defaultDateFormat){
  var dateFormat = defaultDateFormat;
  if (type == 'date') {
    dateFormat = 'YYYY-MM-DD';
  }
  else if (type == 'datetime-local' || type == 'datetime') {
    dateFormat = 'YYYY-MM-DDTHH:mm';
  }
  else if (type == 'month') {
    dateFormat = 'YYYY-MM';
  }
  else if (type == 'time') {
    dateFormat = 'HH:mm';
  }
  else if(type == 'week'){
    dateFormat = 'GGGG-[W]WW';
  }
  return dateFormat;
}

ko.bindingHandlers.date = {
  init: function(element, valueAccessor, allBindingsAccessor) {
    element.onchange = function() {
      var value = valueAccessor();

      var dateFormat = allBindingsAccessor().dateFormat
          ? ko.utils.unwrapObservable(allBindingsAccessor().dateFormat) : 'L';

      if (element.tagName == 'INPUT') {
        var type = element.type;
        dateFormat = getDateFormat(type, dateFormat);
        d = moment(element.value, dateFormat);
        var newD = moment();

        if(value() instanceof moment)
          newD = value();

        if(type == 'time') {
          d.date(newD.date());
          d.month(newD.month());
          d.yaer(newD.year());
        }
        if(type =='date' || type == 'month' || type == 'week'){
          d.hour(newD.hour());
          d.minute(newD.minute());
          d.second(newD.second());

          if(type == 'month' || type == 'week'){
            d.date(newD.date());
          }
        }
      }
      else {
        d = moment(element.textContent, dateFormat);
      }

      if (d) {
        if(value() instanceof moment) {
          value(d);
        }
        else if(value() == null) {
          value(d);
        }
        else if(typeof value === "function"){
          value(d.toDate().getTime() / 1000);
        }
        else if(value instanceof Date){
          value.setTime(d.toDate().getTime());
        }
        else {
          value = d.toDate();
        }
      }
      else {
        if(typeof value === "function"){
          value(null);
        }
        else if(value instanceof Date){
          value.setTime(0);
        }
        else {
          value = null;
        }
      }
    };
  },
  update: function(element, valueAccessor, allBindingsAccessor, viewModel) {
    var value = valueAccessor();
    var valueUnwrapped = ko.utils.unwrapObservable(value);

    var dateFormat = allBindingsAccessor().dateFormat
        ? ko.utils.unwrapObservable(allBindingsAccessor().dateFormat) : 'L';

    if (element.tagName == 'INPUT' && element != document.activeElement) {
      dateFormat = getDateFormat(element.type, dateFormat);

      if(valueUnwrapped === null) {
        element.value = '';
      }
      else if(valueUnwrapped instanceof moment) {
        if(valueUnwrapped.isValid()) {
          element.value = valueUnwrapped.format(dateFormat);
        }
      }
      else {
        element.value = moment(valueUnwrapped).format(dateFormat);
      }
    }
    else {
      if(valueUnwrapped === null) {
        $(element).text('');
      }
      else if(valueUnwrapped instanceof moment) {
        $(element).text(valueUnwrapped.format(dateFormat));
      }
      else {
        $(element).text(moment(valueUnwrapped).format(dateFormat));
      }
    }
  }
};

ko.bindingHandlers.time = {
  update: function(element, valueAccessor) {
    var t = ko.utils.unwrapObservable(valueAccessor());

    if(isNaN(t)) {
      $(element).text('');
    }
    else if (t == Number.POSITIVE_INFINITY) {
      $(element).html('&infin;');
    }
    else if (t == Number.NEGATIVE_INFINITY) {
      $(element).html('-&infin;');
    }
    else {
      var sec_num = t;
      var hours   = Math.floor(sec_num / 3600);
      var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
      var seconds = sec_num - (hours * 3600) - (minutes * 60);

      if (minutes < 10) {minutes = "0"+minutes;}
      if (seconds < 10) {seconds = "0"+seconds;}
      $(element).text(hours+':'+minutes+':'+seconds);
    }
  }
};

ko.bindingHandlers.timer = {
  init: function(element, valueAccessor) {
    element.timer_update = function(){
      var time = ko.utils.unwrapObservable(valueAccessor());

      if(time instanceof moment) {
        var text = time.fromNow();

        $(element).text(text);
      }
    };

    element.timer_update();
    var timer = setInterval(element.timer_update, 10000);
  },
  update: function(element, valueAccessor) {
    element.timer_update = function(){
      var time = ko.utils.unwrapObservable(valueAccessor());

      if(time instanceof moment) {
        var text = time.fromNow();

        $(element).text(text);
      }
    };

    element.timer_update();
  }
};
