var data_service = "/backend.php";

if(typeof token != "undefined")
	data_service += "?token=" + token;

var serverToDo = [];
var selfView = null;

var gps_uuid = '';
var gps_interval = -1; // elke 30 seconden
var gps_timer = null;
var gps_enabled = false;

moment.locale('nl');

var mqtt = null;
var teamInterval = null;

function UpdateTeam(id) {
	if(mqtt.client !== undefined && mqtt.client.isConnected()) {
		setTimeout(function(){mqtt.update_team(id)}, 500);
	}
	else {
		ServerPush({'global.team': {id: id}});
	}
}

function shallowEqual(object1, object2) {
	const keys1 = Object.keys(object1);
	const keys2 = Object.keys(object2);
	if (keys1.length !== keys2.length) {
		return false;
	}
	for (let key of keys1) {
		if (object1[key] !== object2[key]) {
			return false;
		}
	}
	return true;
}

// --- Users ---
function User(data) {
	var self = this;

	self.id = data.id;
	self.username = ko.observable('');

	self.auth_raw = ko.observable([]);

	self.groups_raw = ko.observable([]);

	self.update = function(data) {
		if(data.username !== undefined)
			self.username(data.username);

		if(data.auth !== undefined)
			self.auth_raw(data.auth);

		if(data.groups !== undefined)
			self.groups_raw(data.groups);
	};
	self.update(data);
}

function Group(data) {
	var self = this;

	self.id = data.id;
	self.name = ko.observable('');
	self.contact = ko.observable('');

	self.teams = ko.computed(()=>{
		return ko.utils.arrayFilter(selfView.teams(), (team)=>{
			return team.group_id() === self.id;
		});
	});

	self.update = function(data) {
		if(data.name !== undefined)
			self.name(data.name);

			if(data.contact !== undefined)
				self.contact(data.contact);
	};
	self.update(data);
}

// --- RSW class ---
function TeamRouteItem(data, team) {
	var self = this;

	self.team = team;
	self.post = null;

	self.checkin = ko.observable();
	self.checkout = ko.observable();
	self.score = ko.observable();
	self.score_server = null;

	ko.computed(function() {
		self.checkin();
		if(self.post == null) return;

		if(self.post.type() == 'Tag') {
			self.checkout(self.checkin());
		}
	});

	self.item_state = ko.computed(function() {
		var aan = self.checkin();
		var ver = self.checkout();

		if(ver && self.post.type() == 'Start')
			return "success";

		if(aan && self.post.type() == 'Finish')
			return "success";

		if(aan && !ver)
			return 'info';

		if(!ver||!aan)
			return 'default';

		if(ver >= aan)
			return 'success';

		return 'danger';
	});

	self.tijd_gelope_naar_post = ko.computed(function() {
		var checkin = self.checkin();
		var last_checkout = 0;

		if(!self.team)
			return null;

		self.team.route().forEach(function(r) {
			var t = r.checkout();

			if(t < checkin)
				last_checkout = Math.max(last_checkout, t);
		});

		if(last_checkout == 0) return null;
		if(checkin - last_checkout < 0) return null;
		return (checkin - last_checkout) / 1000;
	});

	self.tijd_op_post = ko.computed(function() {
		var checkin = self.checkin();
		var checkout = self.checkout();

		if(checkin && checkout) {
			if(checkout - checkin < 0) return null;
			return (checkout - checkin) / 1000;
		}
		return null;
	});

	self.update = function(data) {
		if(data.post_id !== undefined) {
			selfView.posten().forEach(function(post) {
				if(post.id == data.post_id)
					self.post = post;
			});
		}

		if(data.checkin !== undefined)
			self.checkin(data.checkin == null ? null : moment(data.checkin, moment.ISO_8601));

		if(data.checkout !== undefined)
			self.checkout(data.checkout == null ? null : moment(data.checkout, moment.ISO_8601));

		if(data.score !== undefined) {
			self.score_server = data.score;
			self.score(data.score);
		}
	}
	self.update(data);
}

function TeamVraagItem(data, team, meta) {
	var self = this;

	self.team = team;
	self.vraag = null;
	self.nummer = meta.nummer;
	self.min_plaats = meta.min_plaats;

	self.updating = true;
	self.antwoord = ko.observable();
	ko.computed(function(){
		self.antwoord();

		if(self.updating)
			return;

		if(self.vraag == null)
			return;

		localStorage.setItem('hikevraag_'+self.vraag.id+'_'+self.team.id, self.antwoord());
	});

	self.visible = ko.computed(function(){
		return (self.team.route.min_place() >= self.min_plaats);
	});

	self.status = ko.observable('onbekend');
	self.color = ko.computed(function() {
		var t = self.status();

		if(t == 'auto_goed' || t == 'manuel_goed')
			return "success";

		if(t == 'auto_fout' || t == 'manuel_fout')
			return "danger";

		return 'default';
	});

	self.update = function(data) {
		self.updating = true;

		if(data.vraag_id !== undefined) {
			selfView.vragen().forEach(function(vraag) {
				if(vraag.id == data.vraag_id)
					self.vraag = vraag;
			});
		}

		if(data.status !== undefined) {
			self.status(data.status);
		}

		var ant = null;
		if(data.antwoord !== undefined)
			ant = data.antwoord;

		if((ant == null || ant == '') && self.vraag != null) {
			// Load last andwoord from coockie if empty
			var tmp = localStorage.getItem('hikevraag_'+self.vraag.id+'_'+self.team.id);

			if(tmp != null)
				ant = tmp;
		}

		if(ant != null)
			self.antwoord(ant);

		self.updating = false;
	}
	self.update(data);
}

function TeamRswItem(data, team, onderdeel) {
	var self = this;

	self.team = team;
	self.onderdeel = onderdeel;
	self.post = null;

	self.time = ko.observable();
	self.score = ko.observable();
	self.score_server = null;

	self.item_state = ko.computed(function() {
		if(self.score() || self.score() === 0)
			return "success";

		return 'default';
	});

	self.update = function(data) {
		if(data.id !== undefined) {
			selfView.rsw_posten().forEach(function(post) {
				if(post.id == data.id)
					self.post = post;
			});
		}

		if(data.time !== undefined)
			self.time(data.time == null ? null : moment(data.time, moment.ISO_8601));

		if(data.score !== undefined) {
			self.score_server = data.score;
			self.score(data.score);
		}
	}
	self.update(data);
}

function TeamBeoordeling(data, team) {
	var self = this;

	self.team = team;
	self.onderdeel = ko.observable();

	self.opmerking = ko.observable(); // to submit new
	self.opmerkingen = ko.computed(function(){
		if(self.onderdeel() == null) return [];
		return ko.utils.arrayFilter(self.team.opmerkingen(), function(opmerkingen){
			return opmerkingen.onderdeel().id === self.onderdeel().id;
		});
	});

	self.is_joker = ko.computed(function() {
		if(!self.onderdeel())
			return false;

		return self.onderdeel().joker_id() == self.team.joker_id() && self.team.joker_id() != null;
	});
	self.joker = ko.computed(function(){
		var r = null;

		if(!self.is_joker())
			return null;

		selfView.jokers().forEach(function(j){
			if(j.id == self.onderdeel().joker_id()) {
				r = j;
			}
		});

		return r;
	});

	self.score_raw = ko.observable([]);
	self.score_server_raw = [];
	self.score = ko.computed(function(){
		self.score_raw();

		if(!self.onderdeel())
			return [];

		var ret = [];

		self.onderdeel().posten().forEach(function(p) {
			self.score_raw().forEach(function(log){
				if(log.id == p.id) {
					ret.push(new TeamRswItem(log, self.team, self));
				}
			});
		});

		return ret;
	});
	self.max_score = ko.computed(function(){
		var score = 0;

		self.score().forEach(function(i){
			score += i.post.max_punten();
		});

		return score;
	});
	self.posten_score_voor_max_punten = ko.computed(function(){
		var sum = 0;

		self.score().forEach(function(i){
			sum += i.score();
		});

		return sum;

	});

	self.posten_score = ko.computed(function(){
		var sum = 0;

		self.score().forEach(function(i){
			sum += i.score();
		});

		if(self.onderdeel()) {
			///TODO: Add min_dyn (alles < min_dyn = 0)
			if(self.onderdeel().max_punten_verschil() != 0) {
				sum /= self.onderdeel().max_punten_verschil();
			}
		}

		var max = 100;
		if(self.onderdeel()) {
			max = self.onderdeel().max_punten();
		}

		return Math.min(Math.max(sum, 0), max);
	});
	self.posten_score_round = ko.computed(()=>{return (Math.round(self.posten_score()) || 0);});

	self.joker_max = ko.computed(function(){
		if(self.is_joker()) {
			return self.joker().max_punten();
		}

		return 0;
	});

	self.joker_score = ko.computed(function(){
		if(self.is_joker()) {
			return self.posten_score() / self.joker().onderdelen.max_punten() * self.joker().max_punten();
		}

		return 0;
	});
	self.joker_score_round = ko.computed(()=>{return (Math.round(self.joker_score()) || 0);});

	self.totaal_score = ko.computed(function(){
		var score = self.posten_score();
		score += self.joker_score();

		return score;
	});

	self.totaal_score_round = ko.computed(function(){
		var score = self.posten_score_round();
		score += self.joker_score_round();

		return score;
	});

	self.complete = ko.computed(function(){
		var done = true;

		self.score().forEach(function(i){
			if(i.score() === null) {
				done = false;
			}
		});

		return done;
	});

	self.item_state = ko.computed(function() {
		var t = 0;
		self.score().forEach(function(i){
			if(i.score() || i.score() === 0) {
				t++;
			}
		});

		if(t == self.score().length) // alles beoordeeld
			return "success";

		if(t > 0) // deels beoordeeld
			return "warning";

		return 'default';
	});

	self.update = function(data) {
		if(data.id !== undefined) {
			selfView.onderdelen().forEach(function(onderdeel) {
				if(onderdeel.id == data.id)
					self.onderdeel(onderdeel);
			});
		}

		if(data.score !== undefined) {
			self.score_server_raw = data.score;
			self.score_raw(data.score);
		}
	}
	self.update(data);

	self.popup_beoordeel = function() {
		selfView.current_team(self.team);
		selfView.current_beoordeling(self);

		selfView.popup_beoordeel(true);
	}
}

function Team(data) {
	var self = this;

	self.id = data.id;
	self.token = ko.observable();
	self.naam = ko.observable('');
	self.team = ko.observable('');
	self.new_team_name = ko.observable('');
	self.nummer = ko.observable();
	self.speltak = ko.observable();
	self.group_id = ko.observable();
	self.group = ko.computed(function(){
		var id = self.group_id();
		return (ko.utils.arrayFilter(selfView.groups(), function(group){
			return group.id === id;
		})||[])[0];
	});
	self.own_group = ko.computed(function(){
		var id = self.group_id();
		if(selfView.user_user()) {
			return selfView.user_user().groups_raw().includes(id);
		}
		return false;
	});
	self.contact = ko.observable('');
	self.canceled = ko.observable(null);
	self.joker_id = ko.observable();
	self.joker = ko.computed(function(){
		var f = null;
		var id = self.joker_id();

		selfView.jokers().forEach(function(i){
			if(i.id == id)
				f = i;
		});

		return f;
	});
	self.joker_max = ko.computed(function(){
		if(self.joker()) {
			return self.joker().max_punten();
		}

		return 0;
	});

	self.subcamp_id = ko.observable();
	self.subcamp = ko.computed(function(){
		var f = null;
		var id = self.subcamp_id();

		selfView.subcamp().forEach(function(i){
			if(i.id == id)
				f = i;
		});

		return f;
	});

	self.onderdeel_raw = ko.observable([]);
	self.onderdelen = ko.computed(function(){
		self.onderdeel_raw();
		selfView.onderdelen();

		var ret = [];

		selfView.onderdelen().forEach(function(r) {
			self.onderdeel_raw().forEach(function(log){
				if(log.id == r.id) {
					ret.push(new TeamBeoordeling(log, self));
				}
			});
		});

		return ret;
	});

	self.totaal_score = ko.computed(function(){
		var score = 0;

		// Tel alle velden bij elkaar op
		self.onderdelen().forEach(function(r){
			score += r.totaal_score();
		});

		return score;
	});
	self.totaal_score_round = ko.computed(()=>{return (Math.round(self.totaal_score()) || 0);});

	self.joker_score = ko.computed(function(){
		var score = 0;

		// Tel alle velden bij elkaar op
		self.onderdelen().forEach(function(r){
			score += r.joker_score();
		});

		return score;
	});
	self.joker_score_round = ko.computed(()=>{return (Math.round(self.joker_score()) || 0);});

	self.plaats = ko.computed(function(){
		if(selfView.auth.indexOf('klassement.overzicht') == -1) {
			return 1;
		}

		var score = self.totaal_score_round();
		var plaats = 1;

		if(self.canceled() != null) {
			score = Number.NEGATIVE_INFINITY;
		}

		selfView.teams().forEach(function(t) {
			if(t.canceled() != null) return;

			if(t.totaal_score_round() > score) {
				plaats++;
			}
		});

		return plaats;
	});

	self.opmerkingen_raw = ko.observable([]);
	self.opmerkingen = ko.computed(function(){
		self.opmerkingen_raw();
		selfView.opmerkingen();

		var ret = [];

		self.opmerkingen_raw().forEach(function(o) {
			var found = false;
			selfView.opmerkingen().forEach(function(log){
				if(log.id == o.id) {
					ret.push(log);
					found = true;
				}
			});
			if(!found) {
				///TODO: Rewrite to global option
				var t = new Opmerking(o);
				ret.push(t);
				selfView.opmerkingen.push(t);
			}
		});

		return ret;
	});

	self.route_id = ko.observable();
	self.hike_raw = ko.observable([]);
	self.vragen_raw = ko.observable([]);
	self.route = ko.computed(function(){
		self.hike_raw();
		selfView.posten();
		self.route_id();

		if(self.route == undefined // Skip wanneer wij nog niet bestaan
		   || self.route.volgorde() == undefined) // Skip waneer de route nog niet een volgorde heeft
			return [];

		var ret = [];

		self.route.volgorde().forEach(function(post) {
			self.hike_raw().forEach(function(log){
				if(log.post_id == post.id) {
					ret.push(new TeamRouteItem(log, self));
				}
			});
		});

		return ret;
	});
	self.route.punten = ko.computed(function(){
		var ret = self.route();

		ret.sort(function (left, right) {
			return left.post.id == right.post.id ? 0 :
			(left.post.id < right.post.id ? -1 : 1)
		});

		return ret;
	});
	self.route.naam = ko.computed(function(){
		var ret = null;
		var id = self.route_id();

		selfView.routes().forEach(function(r){
			if(id == r.id)
				ret = r.naam();
		});

		return ret;
	});
	self.route.volgorde = ko.computed(function(){
		var ret = null;
		var id = self.route_id();

		selfView.routes().forEach(function(r){
			if(id == r.id)
				ret = r.volgorde();
		});

		return ret;
	});
	self.route.laatste_checkout = ko.computed(function() {
		var ret = null;

		self.route().forEach(function(r) {
			if(r.checkout)
				if(ret == null || r.checkout() > ret.checkout())
					ret = r;
		});

		return ret;
	});
	self.route.laaste_actie = ko.computed(function() {
		var lastTime = 0;
		var lastAction = "";

		self.route().forEach(function(r) {
			if(r.checkin() && lastTime < r.checkin()) {
				lastTime = r.checkin();
				lastAction = r.post.naam() + " (Aangekome)";
			}

			if(r.checkout() && lastTime < r.checkout()) {
				lastTime = r.checkout();
				lastAction = r.post.naam() + " (Vertrokke)";
			}
		});


		return lastAction;
	});
	self.route.min_place = ko.computed(function(){
		var ret = -1;

		var v = self.route.volgorde();

		self.route().forEach(function(r) {
			if(r.checkout() || (r.checkin() && r.post.type() == 'Start')) {
				ret = Math.max(ret, v.indexOf(r.post));
			}
		});

		return ret;
	});
	self.route.vragen = ko.computed(function(){
		var vragen = null;
		var id = self.route_id();

		selfView.routes().forEach(function(r){
			if(id == r.id)
				vragen = r.vragen();
		});

		var ret = [];

		if(vragen) {
			vragen.forEach(function(vraag) {
				self.vragen_raw().forEach(function(log){
					if(log.vraag_id == vraag.vraag.id) {
						ret.push(new TeamVraagItem(log, self, vraag));
					}
				});
			});
		}

		return ret;
	});

	self.totaal_gelope = ko.computed(function() {
		var sum = 0;

		self.route().forEach(function(i){
			if(i.tijd_gelope_naar_post())
				sum += i.tijd_gelope_naar_post();
		});

		return sum;
	});

	self.totaal_op_post = ko.computed(function() {
		var sum = 0;

		self.route().forEach(function(i){
			if(i.tijd_op_post())
				sum += i.tijd_op_post();
		});

		return sum;
	});

	self.totaal_hike_score = ko.computed(function() {
		var sum = 0;

		self.route().forEach(function(i){
			if(i.score() !== null)
				sum += parseInt(i.score(), 10);
		});

		return sum;
	});

	self.vragen_score = ko.computed(function() {
		var sum = 0;

		self.route.vragen().forEach(function(i){
			var t = i.status();
			if(t == 'auto_goed' || t == 'manuel_goed')
				sum += 1;
		});

		return sum;
	});

	self.ingecheckt_current_post = ko.computed(function(){
		var id = selfView.current_post();
		var ret =  false;

		if(id == null)
			return false;

		id = id.id;

		self.route().forEach(function(r){
			if(r.post.id == id && (r.checkin() != null)) {
				ret = r.checkin();
			}
		});

		return ret;
	});

	self.uitgecheckt_current_post = ko.computed(function(){
		var id = selfView.current_post();
		var ret =  false;

		if(id == null)
			return false;

		id = id.id;

		self.route().forEach(function(r){
			if(r.post.id == id && (r.checkout() != null)) {
				ret = r.checkout();
			}
		});

		return ret;
	});

	self.hike_post_score = ko.computed(function() {
		var sum = 0;

		self.route().forEach(function(r){
			if(r.post.type() == 'Post' && r.score()) {
				sum += parseInt(r.score(), 10);
			}
		});

		return sum;
	});

	self.hike_tijd_score = ko.computed(function() {
		var max_punten = selfView.hikeMaxPuntenLopen(); // max punten voor de tijd

		var minimale_looptijd = selfView.hikeMinTijd(); // deze tijd is de volle punten waard
		var maximale_looptijd = selfView.hikeMaxTijd(); // langer dan deze tijd is 0 punten waard

		// Convert tijden naar seconden
		minimale_looptijd = minimale_looptijd.diff(minimale_looptijd.clone().startOf('day'));
		maximale_looptijd = maximale_looptijd.diff(maximale_looptijd.clone().startOf('day'));

		var tijd_gelopen = self.totaal_gelope() * 1000; // Moment handles milliseconds

		if(tijd_gelopen <= minimale_looptijd)
			return max_punten/1; // alleen de maximaale score terug geven

		if(maximale_looptijd - tijd_gelopen < 0)
			return 0; // telang over gedaan dus 0 punten

		return (max_punten / (maximale_looptijd - minimale_looptijd)) * (maximale_looptijd - tijd_gelopen);
	});
	self.hike_tijd_score_round = ko.computed(()=>{return (Math.round(self.hike_tijd_score()) || 0);});

	self.hike_posten_checkin = ko.computed(function(){
		var count = 0;

		self.route().forEach(function(r){
			if(r.checkin() && r.post.type() == 'Post') {
				count++;
			}
		});

		return count;
	});

	self.hike_posten_checkin_score = ko.computed(function(){
		var score_per_post = parseInt(selfView.hikePostPunten(), 10);
		return score_per_post * self.hike_posten_checkin();
	});

	self.hike_posten_checkin_skiped = ko.computed(function(){
		var skiped = 0;

		self.route().forEach(function(r){
			if(r.post.type() == 'Post' || r.post.type() == 'Tag') {
				if(!r.checkin() && !r.post.allow_skip()) {
					skiped++;
				}
			}
		});

		return skiped;
	});

	self.hike_posten_checkin_skiped_score = ko.computed(function(){
		var score_per_post = parseInt(selfView.hikePostMinPunten(), 10);
		return score_per_post * self.hike_posten_checkin_skiped();
	});

	self.hike_totaal_score = ko.computed(function() {
		var max_punten = selfView.hikeMaxPunten(); // max punten voor de tijd

		var sum = self.hike_tijd_score();
		sum += self.hike_posten_checkin_score();
		sum -= self.hike_posten_checkin_skiped_score();

		return Math.max(0, Math.min(max_punten, sum));
	});
	self.hike_totaal_score_round = ko.computed(()=>{return (Math.round(self.hike_totaal_score()) || 0);});

	self.is_compleet = ko.computed(function(){
		if(self.subcamp_id() == null) return false;
		if(self.route_id() == null) return false;
		if(self.joker_id() == null) return false;

		return true;
	});

	self.update = function(data) {
		if(data.token !== undefined)
			self.token(data.token);

		if(data.naam !== undefined)
			self.naam(data.naam);

		if(data.team !== undefined)
			self.team(data.team);

		if(data.nummer !== undefined)
			self.nummer(data.nummer);

		if(data.speltak !== undefined)
			self.speltak(data.speltak);

		if(data.group_id !== undefined)
			self.group_id(data.group_id);

		if(data.contact !== undefined)
			self.contact(data.contact);

		if(data.canceled !== undefined)
			self.canceled(data.canceled == null ? null : moment(data.canceled, moment.ISO_8601));

		if(data.subcamp_id !== undefined)
			self.subcamp_id(data.subcamp_id);

		if(data.route_id !== undefined)
			self.route_id(data.route_id);

		if(data.hike !== undefined)
			self.hike_raw(data.hike);

		if(data.vragen !== undefined)
			self.vragen_raw(data.vragen);

		if(data.opmerkingen !== undefined)
			self.opmerkingen_raw(data.opmerkingen);

		if(data.onderdeel !== undefined) {
			self.onderdeel_raw(data.onderdeel);

		if(data.rsw_joker !== undefined)
			self.joker_id(data.rsw_joker);
		}
	};
	self.update(data);

	self.popup_beoordeel = function() {
		selfView.current_team(self);
		var onderdeel_id = selfView.current_onderdeel().id;

		self.onderdelen().forEach(function(r){
			if(r.onderdeel().id == onderdeel_id) {
				selfView.current_beoordeling(r);
			}
		});

		selfView.popup_beoordeel(true);
	}

	self.send_beoordeling = function() {
		var onderdeel = selfView.current_beoordeling();

		onderdeel.score().forEach(function(r){
			if(r.score() == r.score_server)
				return;

			var t = r.score();
			t = Math.min(t, r.post.max_punten());
			t = Math.max(t, 0);

			ServerPush({'rsw.submit': {
				'team_id': self.id,
				'post': r.post.id,
				'score': t
			}});
		});

		if(onderdeel.opmerking()) {
			ServerPush({'rsw.submit': {
				'team_id': self.id,
				'onderdeel': onderdeel.onderdeel().id,
				'opmerking': onderdeel.opmerking()
			}});
			onderdeel.opmerking(null);
		}
		ServerUpdate();

		UpdateTeam(self.id);

		selfView.popup_beoordeel(false);
		selfView.current_team(null);
	}

	self.popup_tijden = function() {
		selfView.current_team(self);
		selfView.popup_post_tijden(true);
	}

	self.send_tijden = function() {
		self.route().forEach(function(r){
			var t = r.score();
			t = Math.min(t, r.post.max_punten());
			t = Math.max(t, 0);

			ServerPush({'hike.correctie': {
				'team_id': self.id,
				'post': r.post.id,
				'score': t,
				'checkin': r.checkin,
				'checkout': r.checkout
			}});
		});
		ServerUpdate();

		UpdateTeam(self.id);

		selfView.popup_post_tijden(false);
		selfView.current_team(null);
	}

	self.popup_checkin = function() {
		if(selfView.current_post() == null)
			return;

		selfView.current_team(self);
		selfView.popup_post_checkin(true);
	}

	self.send_checkin = function() {
		ServerPush({'hike.checkin': {
			'team_id': self.id,
			'post': selfView.current_post().id
		}});
		ServerUpdate();

		if(selfView.current_post().type() == 'Tag') {
			ServerPush({'hike.checkout': {
				'team_id': self.id,
				'post': selfView.current_post().id
			}});
		}

		UpdateTeam(self.id);

		selfView.popup_post_checkin(false);
		selfView.current_team(null);
	}

	self.checkout_score = ko.observable();
	self.checkout_route = ko.observable();

	self.popup_checkout = function() {
		if(selfView.current_post() == null)
			return;

		self.checkout_route(self.route_id());

		selfView.current_team(self);
		selfView.popup_post_checkout(true);
	}

	self.send_checkout = function() {
		var t = self.checkout_score();
		t = Math.min(t, selfView.current_post().max_punten());
		t = Math.max(t, 0);

		if(selfView.auth.indexOf('hike.route') > -1 && self.checkout_route() != self.route_id()) {
			ServerPush({'hike.route': {
				'team_id': self.id,
				'route': self.checkout_route()
			}});
		}

		ServerPush({'hike.checkout': {
			'team_id': self.id,
			'post': selfView.current_post().id,
			'score': t
		}});
		ServerUpdate();

		UpdateTeam(self.id);

		self.checkout_score(t);
		selfView.popup_post_checkout(false);
		selfView.current_team(null);
	}

	self.need_team_name = ko.computed(function(){
		if(self.team() == null) return true;
		if(self.team() == '') return true;
		return false;
	});
	self.send_team_name = function() {
		ServerPush({'team.team_name': {
			'team_id': self.id,
			'team': self.new_team_name()
		}});
		ServerUpdate();

		UpdateTeam(self.id);
	}

	self.popup_inschrijving = function() {
		selfView.current_team(self);
		selfView.popup_inschrijving(true);
	}

	self.update_team = function() {
		ServerPush({'team.update': {
			id: self.id,
			naam: self.naam,
			nummer: self.nummer,
			group_id: self.group_id,
			contact: self.contact,
			canceled: self.canceled,
			speltak: self.speltak,
			subcamp_id: self.subcamp_id,
			team: self.team,
			joker_id: self.joker_id,
			route_id: self.route_id
		}});
		ServerUpdate();

		UpdateTeam(self.id);

		selfView.popup_inschrijving(false);
		selfView.current_team(null);
	}

	self.popup_opmerkingen = function() {
		selfView.current_team(self);
		selfView.popup_opmerkingen(true);
	}

	self.close_opmerkingen = function() {
		selfView.popup_opmerkingen(false);
	}

	self.popup_score = function() {
		selfView.current_team(self);
		selfView.popup_score(true);
	}

	self.close_score = function() {
		selfView.popup_score(false);
	}

	self.popup_hike_vragen = function() {
		selfView.current_team(self);
		selfView.popup_hike_vragen(true);
	}

	self.popup_hike_berekening = function() {
		selfView.current_team(self);
		selfView.popup_hike_berekening(true);
	}

	self.send_hike_vragen = function() {
		self.route.vragen().forEach(function(r){
			ServerPush({'hike.antwoord': {
				team_id: self.id,
				vraag: r.vraag.id,
				antwoord: r.antwoord(),
			}});
		});

		selfView.popup_hike_vragen(false);

		UpdateTeam(self.id);
	}

	self.get_inschrijving = function() {
		self.update_team();
		ServerPush({'team.inschrijving': {team_id: self.id}});
	}

	self.load_team_gps = function() {
		if(typeof load_team_gps_gpx !== 'undefined') {
			load_team_gps_gpx(self.id);
		}
	}

	// Woord gebruikt om in de eerste Finish post opteslaan
	self.hike_punten = ko.observable();
}

function Subcamp(data) {
	var self = this;

	self.id = data.id;
	self.name = ko.observable('');
	self.contact = ko.observable(null);

	self.teams = ko.computed(()=>{
		return ko.utils.arrayFilter(selfView.teams(), (team)=>{
			return team.subcamp_id() === self.id;
		});
	});

	self.update = function(data) {
		if(data.name !== undefined)
			self.name(data.name);

		if(data.contact !== undefined)
			self.contact(data.contact);
	};
	self.update(data);
}

function Opmerking(data) {
	var self = this;

	self.id = data.id;
	self.team_id = ko.observable();
	self.user_id = ko.observable();
	self.user = ko.computed(function(){
		var f = null;
		var id = self.user_id();

		selfView.users().forEach(function(i){
			if(i.id == id)
				f = i;
		});

		return f;
	});

	self.onderdeel_id = ko.observable();
	self.onderdeel = ko.computed(function(){
		var f = null;
		var id = self.onderdeel_id();

		selfView.onderdelen().forEach(function(i){
			if(i.id == id)
				f = i;
		});

		return f;
	});

	self.opmerking = ko.observable();

	self.update = function(data) {
		if(data.team_id !== undefined)
			self.team_id(data.team_id);

		if(data.user_id !== undefined)
			self.user_id(data.user_id);

		if(data.groep_id !== undefined)
			self.onderdeel_id(data.groep_id);

		if(data.opmerking !== undefined)
			self.opmerking(data.opmerking);
	};
	self.update(data);

	self.remove = function() {
		ServerPush({'rsw.remove_comment': {id: self.id}});
		ServerUpdate();

		UpdateTeam(self.team_id());

		selfView.popup_opmerkingen(false);
	}
}

function Joker(data) {
	var self = this;

	self.id = data.id;
	self.naam = ko.observable('');
	self.max_punten = ko.observable(100);

	self.onderdelen = ko.computed(function(){
		return ko.utils.arrayFilter(selfView.onderdelen(), function(onderdeel){
			return onderdeel.joker_id() === self.id;
		});
	});

	self.onderdelen.max_punten = ko.computed(function(){
		var sum = 0;

		self.onderdelen().forEach(function(onderdeel){
			sum += onderdeel.max_punten();
		});

		return sum;
	});

	self.update = function(data) {
		if(data.naam !== undefined)
			self.naam(data.naam);

		if(data.max_punten !== undefined)
			self.max_punten(data.max_punten);
	};
	self.update(data);
}

function Onderdeel(data) {
	var self = this;

	self.id = data.id;
	self.naam = ko.observable('');
	self.auth = ko.observable(false);
	self.joker_id = ko.observable();
	self.max_punten = ko.observable(0);
	self.dynamic = ko.observable(false);
	self.dynamic_min = ko.observable();
	self.dynamic_max = ko.observable();

	// Voor het insturen van dit onderdeel
	self.opmerking = ko.observable();

	self.posten = ko.computed(function(){
		return ko.utils.arrayFilter(selfView.rsw_posten(), function(post){
			return post.groep_id() === self.id;
		});
	});

	self.posten.max_punten = ko.computed(function(){
		var sum = 0;

		self.posten().forEach(function(i){
			var t = Number(i.max_punten());
			if(!isNaN(t)) {
				sum += t;
			}
		});

		return sum;
	});

	self.posten.min_score = ko.computed(function(){
		var min = Number.POSITIVE_INFINITY;

		selfView.teams().forEach(function(team){
			team.onderdelen().forEach(function(r){
				if(r.onderdeel().id == self.id) {
					min = Math.min(min, r.posten_score_voor_max_punten());
				}
			});
		});

		return min;
	});

	self.posten.max_score = ko.computed(function(){
		var max = Number.NEGATIVE_INFINITY;

		selfView.teams().forEach(function(team){
			team.onderdelen().forEach(function(r){
				if(r.onderdeel().id == self.id) {
					max = Math.max(max, r.posten_score_voor_max_punten());
				}
			});
		});

		return max;
	});

	self.posten.dynamic_min = ko.computed(function(){
		var min_dyn = self.posten.min_score();

		if(self.dynamic()) {
			if(self.dynamic_min()) {
				var t = self.dynamic_min();
				if(t >= 0) min_dyn = t;
				if(t < 0) min_dyn -= t;
			}
		}
		else {
			min_dyn = 0;
		}

		return min_dyn;
	});

	self.posten.dynamic_max = ko.computed(function(){
		var max_dyn = self.posten.max_score();

		if(self.dynamic()) {
			if(self.dynamic_max()) {
				var t = Number(self.dynamic_max());
				if(t > 0) max_dyn = t;
				if(t < 0 && !isNaN(t)) max_dyn += t;
			}
		}
		else {
			max_dyn = self.posten.max_punten();
		}

		return max_dyn;
	});

	// used by: posten_score >>> sum /= self.onderdeel().max_punten_verschil();
	self.max_punten_verschil = ko.computed(function(){
		var max = self.max_punten();
		//var min_dyn = self.posten.dynamic_min();
		var max_dyn = self.posten.dynamic_max();
		var max_posten = self.posten.max_punten();

		if(self.dynamic()) {
			///TODO: Add min_dyn (alles < min_dyn = 0)
			return max_dyn/max;
		}
		else {
			return max_posten/max;
		}
	});

	self.tebeordelen_teams = ko.computed(function(){
		var ret = [];

		selfView.teams().forEach(function(team){
			team.onderdelen().forEach(function(r){
				if(r.onderdeel().id == self.id && !r.complete()) {
					ret.push(r);
				}
			});
		});

		return ret;
	});

	self.team_verloop = ko.computed(function(){
		var ret = [];

		var totaal = selfView.teams().length;
		var voledig = totaal - self.tebeordelen_teams().length;
		var deels = 0;
		var canceled = 0;
		self.tebeordelen_teams().forEach(function(i){
			if(i.team.canceled()) {
				canceled += 1;
			}
			else if(i.item_state() == 'warning') {
				deels += 1;
			}
		});

		// Verander aantalen naar procenten
		totaal = 100 / totaal;

		var p_deels = deels * totaal;
		var p_voledig = voledig * totaal;
		var p_canceled = canceled * totaal;

		ret.push({
			value: p_voledig,
			animated: false,
			striped: false,
			type: 'success',
			text: '('+voledig+') voledig'
		});

		ret.push({
			value: p_deels,
			animated: false,
			striped: true,
			type: 'warning',
			text: '('+deels+') deels'
		});

		ret.push({
			value: p_canceled,
			animated: false,
			striped: true,
			type: 'success',
			text: '('+canceled+') vervallen'
		});

		return ret;
	});

	self.update = function(data) {
		if(data.naam !== undefined)
			self.naam(data.naam);

		if(data.auth !== undefined)
			self.auth(data.auth);

		if(data.joker_id !== undefined)
			self.joker_id(data.joker_id);

		if(data.max_punten !== undefined)
			self.max_punten(data.max_punten);

		if(data.dynamic !== undefined)
			self.dynamic(data.dynamic);

		if(data.dynamic_min !== undefined)
			self.dynamic_min(data.dynamic_min);

		if(data.dynamic_max !== undefined)
			self.dynamic_max(data.dynamic_max);
	};
	self.update(data);

	self.popup_posten_toevoegen = function() {
		selfView.popup_posten_toevoegen(true);
	}

	self.posten_printen = function() {
		if(selfView.pdf_loading()) return;

		ServerPush({'rsw.formulier': {
			'id': self.id,
			'score': true
		}});

		selfView.pdf_loading(true);
	}

	self.qrcodes_printen = function() {
		if(selfView.pdf_loading()) return;

		ServerPush({'qrcodes.rsw': {
			'id': self.id
		}});

		selfView.pdf_loading(true);
	}

	self.new_onderdeel = function() {
		selfView.rsw_posten.push(new RswPost({
			id: -1,
			groep_id: self.id
		}));
	}

	self.save_onderdeel = function() {
		ServerPush({'rsw.onderdeel_update':{
			'id': self.id,
			'naam': self.naam(),
			'max_punten': self.max_punten(),
			'dynamic': self.dynamic(),
			'dynamic_min': self.dynamic_min(),
			'dynamic_max': self.dynamic_max(),
			'joker_id': self.joker_id()
		}});

		self.posten().forEach(function(r){
			ServerPush({'rsw.toevoegen': {
				'id': r.id,
				'groep_id': r.groep_id(),
				'naam': r.naam(),
				'uitleg': r.uitleg(),
				'max_punten': r.max_punten(),
				'custom': r.custom_json(),
			}});

			if(r.id == -1) {
				selfView.rsw_posten.remove(r);
			}
		});

		ServerPush({'global.rsw': {}});

		selfView.popup_posten_toevoegen(false);
	}

	self.popup_manage_users = function() {
		selfView.popup_manage_users(true);
	}
}

function RswPostPreset(data) {
	var self = this;

	self.name = ko.observable();
	self.value = ko.observable();

	self.update = function(data) {
		if(data.name !== undefined)
			self.name(data.name);

		if(data.value !== undefined)
			self.value(data.value);
	};
	self.update(data);
}

function RswPost(data) {
	var self = this;

	self.id = data.id;
	self.groep_id = ko.observable(0);
	self.naam = ko.observable('');
	self.uitleg = ko.observable('');
	self.max_punten = ko.observable(0);
	self.score = ko.observable();
	self.score_server = null;
	self.custom = ko.observableArray([]);
	self.custom_set = ko.computed(function(){
		if(Array.isArray(self.custom())) {
			return self.custom().length > 0;
		}
		return false;
	});
	self.custom_json = ko.computed({
		read: ()=>{
			let ret = {};
			self.custom().forEach(e=>{
				ret[e.name()] = e.value();
				empty = false;
			});
			return ko.toJSON(ret);
		},
		write: (value)=>{
			var a = null;
			try {a=JSON.parse(value);} catch(e){}

			let set = [];
			Object.keys(a).forEach(key => {
				empty = false;
				set.push(new RswPostPreset({name:key, value:a[key]}));
			});

			self.custom(set);
		}
	});

	self.update = function(data) {
		if(data.naam !== undefined)
			self.naam(data.naam);

		if(data.groep_id !== undefined)
			self.groep_id(data.groep_id);

		if(data.uitleg !== undefined)
			self.uitleg(data.uitleg);

		if(data.max_punten !== undefined)
			self.max_punten(data.max_punten);

		if(data.custom !== undefined) {
			if(Array.isArray(data.custom)) {
				self.custom(data.custom.map(e=>{return new RswPostPreset(e)}));
			}
		}
	};
	self.update(data);
}

// --- Hike class ---
function HikePost(data) {
	var self = this;

	self.id = data.id;
	self.naam = ko.observable('');
	self.auth = ko.observable(false);
	self.type = ko.observable('Post');
	self.max_punten = ko.observable(0);
	self.allow_skip = ko.observable(false);
	self.lat = ko.observable(null);
	self.lng = ko.observable(null);

	self.teams = ko.computed(()=>{
		return ko.utils.arrayFilter(selfView.teams(), (team)=>{
			return ko.utils.arrayFilter(team.route(), (route)=>{
				return route.post.id === self.id;
			}).length > 0;
		});
	});

	self.vertrokken_teams = ko.computed(function(){
		var ret = [];

		selfView.teams().forEach(function(team) {
			team.route().forEach(function(r) {
				if(r.post.id == self.id) {
					if(r.checkout() && (r.checkin() || r.post.type() == 'Start')) {
						ret.push(r); // Add route met team object zodat de tijden beschikbaar zijn
					}
				}
			});
		});

		ret.sort(function (left, right) {
			return left.checkout() == right.checkout() ? 0 :
			(left.checkout() > right.checkout() ? -1 : 1)
		});

		return ret;
	});

	self.last_checkout = ko.computed(function(){
		var r = moment(0);
		var f = false;

		self.vertrokken_teams().forEach(function(team){
			if(r < team.checkout()) {
				r = team.checkout;
				f = true;
			}
		});

		if(!f)
			return null;

		return r;
	});

	self.aanwezige_teams = ko.computed(function(){
		var ret = [];

		selfView.teams().forEach(function(team) {
			team.route().forEach(function(r) {
				if(r.post.id == self.id) {
					if((!r.checkout() || r.checkout() < r.checkin()) && (r.checkin() || r.post.type() == 'Start'))
						ret.push(r);
				}
			});
		});

		ret.sort(function (left, right) {
			return left.checkout() == right.checkout() ? 0 :
			(left.checkout() > right.checkout() ? -1 : 1)
		});

		return ret;
	});

	self.verwachte_teams = ko.computed(function(){
		var ret = [];

		selfView.teams().forEach(function(team) {
			var last = null;
			var set = null;

			// Loop door alle route items gesorteerd op loop volgorde
			team.route().forEach(function(r) {
				if(r.post.id == self.id) {
					if(last && last.checkout() && !r.checkin()) {
						set = last;
						ret.push(set);
					}
				}
				if(set && r.post.type() == 'Finish' && r.checkin()) {
					var t = ret.indexOf(set);
					if(t > -1)
						ret.splice(t, 1); // Haal team weer uit verwacht lijst als deze al gefinisht is
					set = null;
				}
				last = r;
			});
		});

		ret.sort(function (left, right) {
			return left.checkout() == right.checkout() ? 0 :
			(left.checkout() < right.checkout() ? -1 : 1)
		});

		return ret;
	});

	self.team_verloop = ko.computed(function(){
		var ret = [];

		var totaal = self.teams().length;

		if(totaal == 0) {
			return [];
		}

		var checkin_count = self.aanwezige_teams().length;
		var checkout_count = self.vertrokken_teams().length;;
		var aanlopend_count = self.verwachte_teams().length;
		var gepasseerd_count = 0;

		selfView.teams().forEach(function(team) {
			var op_post_geweest = false;

			// Als een team al bij de finish is van het totaal afhalen
			team.route().forEach(function(r) {
				if(r.post.id == self.id && (r.checkin() || r.post.type() == 'Start')) {
					op_post_geweest = true;
				}

				if(r.post.type() == 'Finish' && r.checkin() && !op_post_geweest) {
					gepasseerd_count++;
				}
			});
		});

		// Verander aantalen naar procenten
		totaal = 100 / totaal;

		var p_gepasseerd_count = gepasseerd_count * totaal;
		var p_checkout_count = checkout_count * totaal;
		var p_checkin_count = checkin_count * totaal;
		var p_aanlopend_count = aanlopend_count * totaal;

		ret.push({
			value: p_gepasseerd_count,
			animated: false,
			striped: true,
			type: 'success',
			text: '('+gepasseerd_count+') gepasseerd'
		});

		ret.push({
			value: p_checkout_count,
			animated: false,
			striped: false,
			type: 'success',
			text: '('+checkout_count+') vertrokken'
		});

		ret.push({
			value: p_checkin_count,
			animated: false,
			striped: false,
			type: 'info',
			text: '('+checkin_count+') aanwezig'
		});

		ret.push({
			value: p_aanlopend_count,
			animated: true,
			striped: true,
			type: 'secondary',
			text: '('+aanlopend_count+') verwacht'
		});

		return ret;
	});

	self.update = function(data) {
		if(data.naam !== undefined)
			self.naam(data.naam);

		if(data.auth !== undefined)
			self.auth(data.auth);

		if(data.type !== undefined)
			self.type(data.type);

		if(data.allow_skip !== undefined)
			self.allow_skip(data.allow_skip);

		if(data.max_punten !== undefined)
			self.max_punten(data.max_punten);

		if(data.lat !== undefined)
			self.lat(data.lat);

		if(data.lng !== undefined)
			self.lng(data.lng);
	};
	self.update(data);

	self.popup_vertrokken = function() {
		selfView.current_post(self);
		selfView.popup_post_vertrokken(true);
	}

	self.formulier_printen = function() {
		ServerPush({'hike.formulier': {
			'id': self.id,
			'score': true
		}});
	}
}

function HikeRoute(data) {
	var self = this;

	self.id = data.id;
	self.naam = ko.observable('');

	self.teams = ko.computed(()=>{
		return ko.utils.arrayFilter(selfView.teams(), (team)=>{
			return team.route_id() === self.id;
		});
	});

	self.snelste_gelope = ko.computed(function(){
		var snelste = Number.POSITIVE_INFINITY;

		self.teams().forEach(function(t){
			if(t.canceled() != null) return;

			if(t.totaal_gelope() < snelste && t.totaal_gelope() > 0) {
				snelste = t.totaal_gelope();
			}
		});

		return snelste;
	});

	self.avg_gelope = ko.computed(function(){
		var sum = 0;
		var count = 0;

		self.teams().forEach(function(t){
			if(t.canceled() != null) return;

			if(t.totaal_gelope() > 0) {
				sum += t.totaal_gelope();
				count++;
			}
		});

		return ((sum/count) || 0);
	});

	self.langste_gelope = ko.computed(function(){
		var snelste = 0;

		self.teams().forEach(function(t){
			if(t.canceled() != null) return;

			if(t.totaal_gelope() > snelste) {
				snelste = t.totaal_gelope();
			}
		});

		return snelste;
	});
	self.avg_gelope_round = ko.computed(()=>{return (Math.round(self.avg_gelope()) || 0);});

	self.volgorde_raw = ko.observable([]);
	self.volgorde = ko.computed(function(){
		var r = [];

		self.volgorde_raw().forEach(function(e){
			selfView.posten().forEach(function(i){
				if(i.id == e)
					r.push(i);
			});
		});

		return r;
	});

	self.vragen_raw = ko.observable([]);
	self.vragen = ko.computed(function(){
		var r = [];
		var ids = [];

		self.vragen_raw().forEach(function(e){
			selfView.vragen().forEach(function(i){
				if(i.id == e.vraag_id && ids.indexOf(e.vraag_id) == -1) {
					ids.push(e.vraag_id);
					r.push(new VraagItem(i, e));
				}
			});
		});

		return r;
	});

	self.update = function(data) {
		if(data.naam !== undefined)
			self.naam(data.naam);

		if(data.volgorde !== undefined) {
			self.volgorde_raw(Object.values(data.volgorde));
		}

		if(data.vragen !== undefined) {
			self.vragen_raw(data.vragen);
		}
	};
	self.update(data);
}

function HikeVraag(data) {
	var self = this;

	self.id = data.id;
	self.naam = ko.observable('');
	self.vraag = ko.observable('');
	self.min_place = 0;

	self.update = function(data) {
		if(data.naam !== undefined)
			self.naam(data.naam);

		if(data.vraag !== undefined)
			self.vraag(data.vraag);
	};
	self.update(data);
}

function VraagItem(vraag, info) {
	var self = this;

	self.vraag = vraag;
	self.min_plaats = info.min_plaats;
	self.nummer = info.nummer;
}

function AppViewModel() {
	var self = this;
	selfView = self;

	self.window_hidden = ko.observable(false);
	self.pdf_loading = ko.observable(false);

	// Popup's
	self.popup_beoordeel = ko.observable(false);
	self.popup_post_tijden = ko.observable(false);
	self.popup_post_vertrokken = ko.observable(false);
	self.popup_post_checkin = ko.observable(false);
	self.popup_post_checkout = ko.observable(false);
	self.popup_hike_vragen = ko.observable(false);
	self.popup_hike_berekening = ko.observable(false); // passive (not in active)
	self.popup_inschrijving = ko.observable(false);
	self.popup_opmerkingen = ko.observable(false);
	self.popup_score = ko.observable(false);
	self.popup_posten_toevoegen = ko.observable(false);
	self.popup_manage_users = ko.observable(false);

	self.popup_active = ko.computed(function(){
		if(self.popup_beoordeel()) return true;
		if(self.popup_post_tijden()) return true;
		if(self.popup_post_vertrokken()) return true;
		if(self.popup_post_checkin()) return true;
		if(self.popup_post_checkout()) return true;
		if(self.popup_hike_vragen()) return true;
		if(self.popup_inschrijving()) return true;
		if(self.popup_opmerkingen()) return true;
		if(self.popup_score()) return true;
		if(self.popup_posten_toevoegen()) return true;
		if(self.popup_manage_users()) return true;

		return false;
	});

	// Noodenvelope
	self.envelope_open = ko.observable(false);
	self.open_envelope = function() {
		self.envelope_open(true);
	}

	// Users
	self.users = ko.observableArray([]);
	self.user_user = ko.observable(null);

	// auth lijst voor specifieke pagina's
	self.auth = ko.observableArray();

	// Team Elements
	self.groups = ko.observableArray([]);
	self.teams = ko.observableArray([]);
	self.current_team = ko.observable(null);
	self.user_team = ko.observable(null);
	self.ingeschreven_teams = ko.computed(function(){
		var ret = [];

		var totaal = selfView.teams().length;
		var heeft_joker = 0;
		var canceled = 0;
		selfView.teams().forEach(function(i){
			if(i.joker_id()) {
				heeft_joker += 1;
			}
			else if(i.canceled()) {
				canceled += 1;
			}
		});

		// Verander aantalen naar procenten
		totaal = 100 / totaal;

		var p_heeft_joker = heeft_joker * totaal;
		var p_canceled = canceled * totaal;

		ret.push({
			value: p_heeft_joker,
			animated: false,
			striped: false,
			type: 'success',
			text: '('+heeft_joker+') ingeschreven'
		});

		ret.push({
			value: p_canceled,
			animated: false,
			striped: true,
			type: 'success',
			text: '('+canceled+') vervallen'
		});

		return ret;
	});

	self.popup_new_team = function() {
		var new_team = new Team({id: -1});
		new_team.popup_inschrijving()
	}

	self.klassement = ko.computed(function(){
		if(self.auth.indexOf('klassement.overzicht') == -1) {
			return [];
		}

		var ret = [];

		self.teams().forEach(function(team) {
				ret.push(team);
		});

		ret.sort(function (left, right) {
			return left.plaats() == right.plaats() ? 0 :
			(left.plaats() < right.plaats() ? -1 : 1)
		});

		return ret;
	});

	self.subcamp = ko.observableArray([]);

	// RSW Elements
	self.opmerkingen = ko.observableArray([]);
	self.jokers = ko.observableArray([]);
	self.onderdelen = ko.observableArray([]);
	self.rsw_posten = ko.observableArray([]);
	self.current_onderdeel = ko.observable();
	self.current_beoordeling = ko.observable();

	self.pdf_tentbriefjes = function() {
		if(selfView.pdf_loading()) return;

		ServerPush({'rsw.tentbriefjes': {}});

		selfView.pdf_loading(true);
	}

	// Hike Elements
	self.posten = ko.observableArray([]);
	self.posten.punten = ko.computed(function(){
		var ret = self.posten();

		ret.sort(function (left, right) {
			return left.id == right.id ? 0 :
			(left.id < right.id ? -1 : 1)
		});

		return ret;
	});
	self.current_post = ko.observable(null);
	ko.computed(function(){
		if(self.current_post()) {
			localStorage.setItem('current_hike_post', self.current_post().id);
		}
	});
	self.post_auth_count = ko.computed(function(){
		var c = 0;

		self.posten().forEach(function(p){
			if(p.auth())
				c++;
		});

		return c;
	});

	self.routes = ko.observableArray([]);
	self.vragen = ko.observableArray([]);

	// Voor hike tijd punten
	self.hikeMinTijd = ko.observable(moment(0));
	self.hikeMaxTijd = ko.observable(moment(3600*10*1000));
	self.hikeMaxPunten = ko.observable(150);
	self.hikeMaxPuntenLopen = ko.observable(100);
	self.hikePostPunten = ko.observable(10);
	self.hikePostMinPunten = ko.observable(50);

	self.teams_order_totaal_gelope = ko.computed(function(){
		var ret = [];

		self.teams().forEach(function(team) {
			if(team.totaal_gelope() > 0) {
				ret.push(team);
			}
		});

		ret.sort(function (left, right) {
			return left.totaal_gelope() == right.totaal_gelope() ? 0 :
			(left.totaal_gelope() < right.totaal_gelope() ? -1 : 1)
		});

		// Plaats niet lopers aan het einde
		self.teams().forEach(function(team) {
			if(team.totaal_gelope() == 0) {
				ret.push(team);
			}
		});

		return ret;
	});

	self.teams_order_hike_totaal = ko.computed(function(){
		var ret = self.teams_order_totaal_gelope();

		ret.sort(function (left, right) {
			return left.hike_totaal_score() == right.hike_totaal_score() ? 0 :
			(left.hike_totaal_score() > right.hike_totaal_score() ? -1 : 1)
		});

		return ret;
	});

	self.save_hike = function() {
		self.teams().forEach(function(t){
			var team_finish = null;

			t.route().forEach(function(r){
				if(r.post.type() == 'Finish')
					team_finish = r.post.id;
			});

			if(team_finish && t.hike_punten()) {
				ServerPush({'hike.checkout': {
					'team_id': t.id,
					'post': team_finish,
					'score': t.hike_punten(),
					'noTime': true
				}});
			}

			UpdateTeam(t.id);
		});

		selfView.popup_hike_vragen(false);
	}

	// Popup voor code scannen
	self.token_popup = false;
	ko.computed(function(){
		if(self.token_popup)
			return;

		if(!self.current_post())
			return;

		if(self.user_team()) {
			if(self.user_team().ingecheckt_current_post() == false && self.current_post().type() != "Start") {
				self.user_team().popup_checkin();
				self.token_popup = true;
			}
			else if (self.user_team().uitgecheckt_current_post() == false && self.current_post().type() != "Finish") {
				self.user_team().popup_checkout();

				self.token_popup = true;
			}
		}
	});

	// QRCodes
	self.qrcodes_printen_teams = function() {
		if(selfView.pdf_loading()) return;

		ServerPush({'qrcodes.teams': {}});

		selfView.pdf_loading(true);
	}

	self.qrcodes_printen_users = function() {
		if(selfView.pdf_loading()) return;

		ServerPush({'qrcodes.users': {}});

		selfView.pdf_loading(true);
	}

	// Navigatie op pagina
	self.currentTab = null;
	self.tabs = ko.computed(function(){
		var ret = [];

		self.onderdelen().forEach(function(o) {
			if(o.auth()) {
				t = {
					title:o.naam(),
					template:(o.naam() == 'Hike' ? 'hike_onderdeel' : 'punten_onderdeel'),
					onderdeel: o,
					color: 'default',
					active: ko.observable(false)
				};

				ret.push(t);
			}
		});

		// laat pagina's afhankelijk van auth
		if(selfView.auth.indexOf('hike.punten') > -1) {
			t = {
				title:'Hike Punten',
				template:'hike_punten',
				onderdeel: null,
				color: 'warning',
				active: ko.observable(false)
			};

			ret.push(t);
		}

		if(selfView.auth.indexOf('hike.gps') > -1) {
			t = {
				title:'Hike GPS',
				template:'hike_gps',
				onderdeel: null,
				color: 'default',
				active: ko.observable(false),
				click: ()=>{
					if(typeof update_map_render !== "undefined") {
						update_map_render();
					}
				}
			};

			ret.push(t);
		}

		if(selfView.auth.indexOf('team.update') > -1) {
			t = {
				title:'Team inschrijving',
				template:'team_inschrijving',
				onderdeel: null,
				color: 'default',
				active: ko.observable(false)
			};

			ret.push(t);
		}

		if(selfView.auth.indexOf('rsw.overzicht') > -1) {
			t = {
				title:'Score overzicht',
				template:'rsw_overzicht',
				onderdeel: null,
				color: 'default',
				active: ko.observable(false)
			};

			ret.push(t);
		}

		if(selfView.auth.indexOf('system.admin') > -1) {
			t = {
				title:'Tools',
				template:'tools_overzicht',
				onderdeel: null,
				color: 'warning',
				active: ko.observable(false)
			};

			ret.push(t);
		}

		if(selfView.auth.indexOf('klassement.overzicht') > -1) {
			t = {
				title:'Klassement',
				template:'klassement_overzicht',
				onderdeel: null,
				color: 'danger',
				question_before: ()=>{return confirm("Weet je zeker dat je het klassement  wilt openen?")},
				active: ko.observable(false)
			};

			ret.push(t);
		}

		if(!self.currentTab && ret.length > 0) {
			var loadTap = localStorage.getItem('current_tap');
			if(loadTap) {
				ret.forEach(function(tap) {
					if(tap.title == loadTap) {
						self.currentTab = tap;
					}
				});
			}

			if(self.currentTab == null) {
				self.currentTab = ret[0];
			}

			self.activate_tab(self.currentTab);
		}

		return ret;
	});

	self.activate_tab = function(data) {
		if(data.question_before !== undefined) {
			if(data.question_before() == false) {
				return;
			}
		}

		if (self.currentTab) {
			self.currentTab.active(false);
		}
		self.currentTab = data;

		if(self.currentTab.onderdeel)
			self.current_onderdeel(self.currentTab.onderdeel);

		self.currentTab.active(true);

		if(data.click !== undefined) {
			data.click();
		}

		localStorage.setItem('current_tap', data.title);
	};

	// Activate GPS
	self.activate_gps = function() {
		ServerPush({'global.gps.activate': {}});
	}
}

// --- Server communication ---
function downloadPDF(pdf, name) {
    const linkSource = `data:application/pdf;base64,${pdf}`;
    const downloadLink = document.createElement("a");
    const fileName = name;

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
}

function MQTTHandler(config) {
	var self = this;
	mqtt = this;

	self.config = {
		host:   "",
        port:   9001,
        useSSL: false,
		path:   "",
        user:   "",
        pass:   "",
        login:  "/admin/login",
        update: "/admin/update",
        gps:    "/admin/gps"
	};

	// called when the client connects
	self.onConnect = function() {
		// Once a connection has been made, make a subscription and send a message.
		console.log("MQTT Connected");

		self.client.subscribe(self.config.update);

		// Stop team fast request
		if(teamInterval) {
			clearInterval(teamInterval);
			teamInterval = null;
		}

		return self.send("Online", self.config.login);
	}

	self.doFail = function(e){
		console.log(e);
	}

	self.update_team = function(id) {
		self.send([{update_team:id}], self.config.update);
	}

	self.update_gps = function(id) {
		self.send([{update_gps:id}], self.config.update);
	}

	self.send = function(message, toppic) {
		message = new Paho.MQTT.Message(JSON.stringify(message));
		message.destinationName = toppic;
		self.client.send(message);
	}

	// called when the client loses its connection
	self.onConnectionLost = function(responseObject) {
		console.log("MQTT Disconnect:", responseObject);

		// Start team fast list
		if(!teamInterval) {
			teamInterval = setInterval(teamIntervalRequest, 30 * 1000); // 30 Seconden
		}
	}

	// called when a message arrives
	self.onMessageArrived = function(message) {
		console.log("MQTT:", message.payloadString);

		try {
			var json = JSON.parse(message.payloadString);

			json.forEach(function(e){
				if(e.server !== undefined) {
					serverSuccess(e.server);
				}

				if(e.update_team !== undefined) {
					ServerPush({'global.team':{id: e.update_team}});
				}

				if(e.update_gps !== undefined && update_gps != undefined) {
					update_gps(e.update_gps);
				}

				if(e.full_update !== undefined) {
					longIntervalRequest();
				}
			});
		}
		catch (e) {
			//console.log("MQTT Parse error:", e);
		}
	}

	self.update = function(config) {
		var is_changed = false;

		Object.keys(self.config).forEach(function(k){
			if(k in config) {
				if(config[k] != self.config[k]) {
					//console.log(k, 'self:', self.config[k], 'new:', config[k]);
					self.config[k] = config[k];

					is_changed = true;
				}

			}
		});

		if(is_changed) {
			if(self.client != undefined && self.client.isConnected()){
				self.client.disconnect();
			}

			// Create a client instance
			self.client = new Paho.MQTT.Client(self.config.host, self.config.port, self.config.path, "web_"+parseInt(Math.random() * 1000, 10));

			// set callback handlers
			self.client.onConnectionLost = self.onConnectionLost;
			self.client.onMessageArrived = self.onMessageArrived;
			var options = {
				useSSL: self.config.useSSL,
				userName: self.config.user,
				password: self.config.pass,
				onSuccess:self.onConnect,
				onFailure:self.doFail
			}

			// connect the client
			self.client.connect(options);
		}
	}
}

function serverSuccess(data) {
	var new_items = [];

	var classes = [];
	// --- RSW onderdelen ---
	classes.subcamp = {arr: selfView.subcamp, cls: Subcamp};
	classes.rsw_joker = {arr: selfView.jokers, cls: Joker};
	classes.rsw_onderdeel = {arr: selfView.onderdelen, cls: Onderdeel};
	classes.rsw_post = {arr: selfView.rsw_posten, cls: RswPost};

	// --- Hike onderdelen ---
	classes.hike_post = {arr: selfView.posten, cls: HikePost};
	classes.hike_route = {arr: selfView.routes, cls: HikeRoute};
	classes.hike_vraag = {arr: selfView.vragen, cls: HikeVraag};

	// --- Algemene items ---
	classes.user = {arr: selfView.users, cls: User};
	classes.group = {arr: selfView.groups, cls: Group};
	classes.team = {arr: selfView.teams, cls: Team};

	// --- Load arrays ---
	data.forEach(function(e){
		// --- Classess ---
		for (const group in classes) {
			if (classes.hasOwnProperty(group)) {
				if(e[group] !== undefined) {
					// Preload array data once
					if(classes[group].filter === undefined) {
						classes[group].filter = classes[group].arr();
					}

					// Check if item alredy in list
					var found = ko.utils.arrayFilter(classes[group].filter, function(item){
						return item.id == e[group].id;
					})[0];

					// If new then store for later
					if(found == null) {
						if(new_items[group] === undefined) new_items[group] = [];
						new_items[group].push(e[group]);
					}
					else {
						found.update(e[group]);
					}
				}
			}
		}
	});

	for (const group in new_items) {
		if (classes.hasOwnProperty(group)) {
			var items = ko.utils.arrayMap(new_items[group], function(item) {
				return new classes[group].cls(item);
			});
			classes[group].arr.push.apply(classes[group].arr, items);
		}
	}

	// --- Load leftovers ---
	var hike_post_timer = false;
	data.forEach(function(e){
		if(e.hike_post !== undefined) {
			if(e.hike_post.auth && selfView.current_post() == null) {
				var found = ko.utils.arrayFilter(selfView.posten(), function(item){
					return item.id == e.hike_post.id;
				})[0];

				// select eerste post waar gebruiker rechten op heeft
				if(found !== undefined) {
					if(localStorage.getItem('current_hike_post') == found.id) {
						selfView.current_post(found);

						if(hike_post_timer != false && hike_post_timer != true) {
							clearTimeout(hike_post_timer);
							hike_post_timer = true;
						}
					}

					// backup when there is no default loaded
					if(hike_post_timer == false) {
						hike_post_timer = setTimeout(()=>{
							if(selfView.current_post() == null) {
								selfView.current_post(found);
							}
						}, 50);
					}
				}
			}
		}

		// --- User ---
		if(e.current_user !== undefined && e.current_user.auth !== undefined) {
			e.current_user.auth.forEach(function(i){
				if(selfView.auth.indexOf(i) < 0){
					selfView.auth.push(i);
				}
			});

			selfView.auth().forEach(function(i){
				if(e.current_user.auth.indexOf(i) < 0){
					selfView.auth.remove(i)
				}
			});
		}

		if(e.current_user !== undefined && e.current_user.type == 'team') {
			var f = null;
			selfView.teams().forEach(function(i){
				if(i.id == e.current_user.id)
					f = i;
			});

			selfView.user_team(f);
		}

		if(e.current_user !== undefined && e.current_user.type == 'user') {
			var f = null;
			selfView.users().forEach(function(i){
				if(i.id == e.current_user.id)
					f = i;
			});

			selfView.user_user(f);
		}

		// --- GPS ---
		if(e.gps !== undefined) {
			if('uuid' in e.gps)
				gps_uuid = e.gps.uuid;

			gpsUpdateInterval(e.gps);
		}

		// --- PDF ---
		if(e.pdf !== undefined) {
			downloadPDF(e.pdf.file, e.pdf.name);
			selfView.pdf_loading(false);
		}

		// --- MQTT ---
		if(e.mqtt !== undefined) {
			if(mqtt == null)
				new MQTTHandler();

			mqtt.update(e.mqtt);
		}

		// --- Time ---
		///TODO: Time
	});
}

function gpsUpdateInterval(data) {
	var nead_update = false;

	if("interval" in data) {
		if(gps_interval != data.interval)
			nead_update = true;

		gps_interval = data.interval;
	}

	if("enabled" in data) {
		if(gps_enabled != data.enabled)
			nead_update = true;

		gps_enabled = data.enabled;
	}

	if(nead_update) {
		if(gps_timer != null) {
			clearInterval(gps_timer);
			gps_timer = null;
		}

		if(gps_enabled && gps_interval > 0 && "geolocation" in navigator) {
			gps_timer = setInterval(gpsInterval, gps_interval);
			gpsInterval();
		}
	}
}

function gpsSuccess(pos) {
	ServerPush({'global.gps.push': {'uuid':gps_uuid, 'coords': pos.coords, 'time':moment(pos.timestamp)}});

	if(mqtt.client !== undefined && mqtt.client.isConnected()) {
		setTimeout(()=>{mqtt.update_gps(selfView.user_team()?.id||-1)}, 500);
	}
}

function gpsError(err) {
	if(err.code == err.PERMISSION_DENIED) {
		clearInterval(gps_timer);
		gps_timer = null;
	}

	console.warn(`GPS ERROR(${err.code}): ${err.message}`);
}

function gpsInterval() {
	var options = {
		enableHighAccuracy: false,
		timeout: gps_interval,
		maximumAge: 0
	};

	// We maken gebruik van getCurrentPosition zodat de  interval bepaald woord door ons en niet door een actie om spammen te voorkomen
	navigator.geolocation.getCurrentPosition(gpsSuccess, gpsError, options);
}

function ServerPush(data) {
	if(data.forEach === undefined) {
		data = [data];
	}

	if(serverToDo === undefined)
		serverToDo = [];

	data.forEach(function(i) {
		serverToDo.push(i);
	});
}

function ServerUpdate() {
	if(selfView.popup_active() || selfView.window_hidden())
		return; // als auto update uit staat && popup open

	if(serverToDo.length == 0) return;

	// Remove duplicates after a sleep
	const todo = serverToDo.reverse();
	const uniqueArray = todo.filter((value, index) => {
		const _value = ko.toJSON(value);
		return index === todo.findIndex(obj => {
			return ko.toJSON(obj) === _value;
		});
	});

	// Run request
	var data = ko.toJSON({data: uniqueArray.reverse()});
	serverToDo = [];

	return $.ajax({
		type: 'POST',
		url: data_service,
		data: data,
		dataType: 'json',
		success: serverSuccess,
		contentType: "application/json"
	});
}

var teamIntervalRequest = function(){
	ServerPush({'global.team': {}});

	if(mqtt != undefined && mqtt.client != undefined && !mqtt.client.isConnected()) {
		let options = {
			useSSL: mqtt.config.useSSL,
			userName: mqtt.config.user,
			password: mqtt.config.pass,
			onSuccess: mqtt.onConnect,
			onFailure: mqtt.doFail
		}
		mqtt.client.connect(options);
	}
};

var longIntervalRequest = function(){
	ServerPush({'global.users': {}});
	ServerPush({'global.group': {}});
	ServerPush({'global.subcamp': {}});
	ServerPush({'global.rsw': {}});
	ServerPush({'global.hike': {}});

	// Laat MQTT config
	ServerPush({'global.mqtt': {}});

	if(!teamInterval)
		ServerPush({'global.team': {}});
};

setInterval(ServerUpdate, 500);
teamInterval = setInterval(teamIntervalRequest, 30 * 1000); // 30 Seconden
setInterval(longIntervalRequest, 10 * 60 * 1000); // 10 Minuten

// --- Loading document JS ---
$( document ).ready(function() {
	ko.options.deferUpdates = true;
	ko.applyBindings(new AppViewModel());

	teamIntervalRequest();
	longIntervalRequest();
	ServerUpdate();

	if(document.addEventListener) {
		document.addEventListener("visibilitychange", () => {
			if(selfView) {
				if (document.visibilityState === "visible") {
					selfView.window_hidden(false);
				} else {
					selfView.window_hidden(true);
				}
			}
		});
	}
});
