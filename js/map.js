var hike_map = null;

var svg_icons = {
    Post:`<?xml version="1.0" encoding="UTF-8"?>
    <svg width="42" height="49" version="1.1" viewBox="0 0 42 49" xmlns="http://www.w3.org/2000/svg">
    <path d="m6.5 5h29c1.0694-0.076812 2 0.99631 2 2v29c0.07681 1.0694-0.9963 2-2 2h-6l-8.5 8-8.5-8h-6c-1.0694 0.07681-2-0.9963-2-2v-29c-0.076812-1.0694 0.99631-2 2-2z" fill="{%color}" stroke="{%color_dark}" stroke-width="1px"/>
    <text x="21" y="26.838879" fill="#000000" font-family="sans-serif" font-size="14.667px" style="line-height:1.25" xml:space="preserve"><tspan x="21" y="26.838879" font-family="sans-serif" font-size="14.667px" font-weight="bold" text-align="center" text-anchor="middle">{%text}</tspan></text>
    </svg>`,

    Team:`<?xml version="1.0" encoding="UTF-8"?>
    <svg width="42" height="49" version="1.1" viewBox="0 0 42 49" xmlns="http://www.w3.org/2000/svg">
    <path d="m6.5 5h29c1.0694-0.076812 2 0.99631 2 2v29c0.07681 1.0694-0.9963 2-2 2h-6l-8.5 8-8.5-8h-6c-1.0694 0.07681-2-0.9963-2-2v-29c-0.076812-1.0694 0.99631-2 2-2z" fill="{%color}" stroke="{%color_dark}" stroke-width="1px"/>
    <text x="21" y="26.838879" fill="#000000" font-family="sans-serif" font-size="14.667px" style="line-height:1.25" xml:space="preserve"><tspan x="21" y="26.838879" font-family="sans-serif" font-size="14.667px" font-weight="bold" text-align="center" text-anchor="middle">{%text}</tspan></text>
    </svg>`,

    Empty:`<?xml version="1.0" encoding="UTF-8"?>
    <svg width="40.8" height="29.6" version="1.1" viewBox="0 0 40.8 29.6" xmlns="http://www.w3.org/2000/svg">
    </svg>`,

    Start:`<?xml version="1.0" encoding="UTF-8"?>
    <svg width="40.8" height="29.6" version="1.1" viewBox="0 0 40.8 29.6" xmlns="http://www.w3.org/2000/svg">
    </svg>`,

    End:`<?xml version="1.0" encoding="UTF-8"?>
    <svg width="40.8" height="29.6" version="1.1" viewBox="0 0 40.8 29.6" xmlns="http://www.w3.org/2000/svg">
    </svg>`,

    Shadow:`<?xml version="1.0" encoding="UTF-8"?>
    <svg width="40.8" height="29.6" version="1.1" viewBox="0 0 40.8 29.6" xmlns="http://www.w3.org/2000/svg">
    <path d="m21.893 13.767 17.479 0.19185c0.66045-0.02151 0.99891 0.38382 0.79084 0.75715l-6.0119 10.787c-0.17539 0.39826-1.0151 0.73732-1.6201 0.73068l-3.6164-0.03969-6.7816 2.9194-3.4647-3.0319-3.6164-0.03969c-0.66045 0.02151-0.99892-0.38381-0.79084-0.75715l6.0119-10.787c0.17539-0.39827 1.0151-0.73733 1.6201-0.73068z" fill-opacity=".30192"/>
    </svg>`,
};

function newShade(hexColor, magnitude) {
    hexColor = hexColor.replace(`#`, ``);
    if (hexColor.length === 6) {
        const decimalColor = parseInt(hexColor, 16);
        let r = (decimalColor >> 16) + magnitude;
        r > 255 && (r = 255);
        r < 0 && (r = 0);
        let g = (decimalColor & 0x0000ff) + magnitude;
        g > 255 && (g = 255);
        g < 0 && (g = 0);
        let b = ((decimalColor >> 8) & 0x00ff) + magnitude;
        b > 255 && (b = 255);
        b < 0 && (b = 0);
        return `#${(g | (b << 8) | (r << 16)).toString(16)}`;
    } else {
        return hexColor;
    }
};

function get_map_icon(icon, text='', color='#000000') {
    var svg = svg_icons[icon];

    svg = svg.replaceAll('{%text}', text);
    svg = svg.replaceAll('{%color}', newShade(color, 0));
    svg = svg.replaceAll('{%color_dark}', newShade(color, -50));

    return 'data:image/svg+xml,'+encodeURIComponent(svg);
}

function load_gpx(url, name, team=null, posten=false) {
    var found = null;
    hike_map.gpx_layers.forEach(l => {
        if(l.url == url) {
            found = l;

            l.reload();

            /*
            TODO: Manier vinden om naam aan te passen
            if(l.name != name) {
                hike_map.layers.removeLayer(l);
                hike_map.layers.addOverlay(l, name);
            }
            // */
        }
    });

    if(found == null) {
        var wptIconTypeUrls = {
            '': get_map_icon('Empty'),
        }

        if(posten) {
            wptIconTypeUrls.Start = get_map_icon('Post', 'S', '#E16262');
            wptIconTypeUrls.Tag = get_map_icon('Post', 'T', '#FABC60');
            wptIconTypeUrls.Post = get_map_icon('Post', 'P', '#60BCFA');
            wptIconTypeUrls.Finish = get_map_icon('Post', 'F', '#3A9679');
        }

        var start = (team != null ? get_map_icon('Team', team.nummer(), '#E16262') : get_map_icon('Start'));
        var end = (team != null ? get_map_icon('Team', team.nummer(), '#3A9679') : get_map_icon('End'));

        found = new L.GPX(url, {
            gpx_options: {
              joinTrackSegments: false
            },
            async: true,
            marker_options: {
                wptIconType: wptIconTypeUrls,
                wptIconTypeUrls: wptIconTypeUrls,
                startIconUrl: start,
                endIconUrl: end,
                shadowUrl: get_map_icon('Shadow'),
                iconSize: [42, 49],
                iconAnchor: [21, 46],
                shadowSize: [51, 37],
                shadowAnchor: [27, 37],
            }
        }).on('loaded', function(e) {
            if(posten && !hike_map.bounded) {
                hike_map.fitBounds(e.target.getBounds());
                hike_map.bounded = true;
            }
        });
        found.url  = url;
        found.name = name;
        found.posten = posten;
        found.team = team;
        hike_map.gpx_layers.push(found);

        hike_map.layers.addOverlay(found, name);
    }

    return found;
}

function render_map() {
    if(hike_map == null) {
        hike_map = L.map('hike_map').setView([0, 0], 13);
        hike_map.bounded = false;
        hike_map.gpx_layers = [];

        var baseLayers = {
            "Open Streetmap": L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(hike_map),

            "Stadia AlidadeSmoothDark": L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png', {
                maxZoom: 20,
                attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
            }),

            "Stadia Outdoors": L.tileLayer('https://tiles.stadiamaps.com/tiles/outdoors/{z}/{x}/{y}{r}.png', {
                maxZoom: 20,
                attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
            }),

            "CyclOSM": L.tileLayer('https://{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png', {
                maxZoom: 20,
                attribution: '<a href="https://github.com/cyclosm/cyclosm-cartocss-style/releases" title="CyclOSM - Open Bicycle render">CyclOSM</a> | Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }),

            "Esri WorldImagery": L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
            }),

            "Kadaster standaard": L.tileLayer('https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0/standaard/EPSG:3857/{z}/{x}/{y}.png', {
                minZoom: 6,
                maxZoom: 19,
                bounds: [[50.5, 3.25], [54, 7.6]],
                attribution: 'Kaartgegevens &copy; <a href="https://www.kadaster.nl">Kadaster</a>'
            }),

            "Kadaster luchtfoto": L.tileLayer('https://service.pdok.nl/hwh/luchtfotorgb/wmts/v1_0/Actueel_ortho25/EPSG:3857/{z}/{x}/{y}.jpeg', {
                minZoom: 6,
                maxZoom: 19,
                bounds: [[50.5, 3.25], [54, 7.6]],
                attribution: 'Kaartgegevens &copy; <a href="https://www.kadaster.nl">Kadaster</a>'
            }),
        }

        var overlays = {
           "WaymarkedTrails Hiking": L.tileLayer('https://tile.waymarkedtrails.org/hiking/{z}/{x}/{y}.png', {
                maxZoom: 18,
                attribution: '&copy; <a href="https://waymarkedtrails.org">waymarkedtrails.org</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
            }),

            "WaymarkedTrails Cycling": L.tileLayer('https://tile.waymarkedtrails.org/cycling/{z}/{x}/{y}.png', {
                maxZoom: 18,
                attribution: '&copy; <a href="https://waymarkedtrails.org">waymarkedtrails.org</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
            }),
        }

        hike_map.layers = L.control.layers(baseLayers, overlays).addTo(hike_map);
        load_gpx("/backend_route.php?posten", "Posten", null, true).addTo(hike_map);

        ko.computed(()=>{
            selfView.teams().forEach(function(t) {
                load_gpx("/backend_route.php?team="+t.id, "Team: "+t.nummer()+". "+t.team(), t);
            });
        });
    }
}

function update_map_render() {
    setTimeout(render_map, 10);


    if(selfView.currentTab.title == 'Hike GPS' && hike_map != null) {
        hike_map.gpx_layers.forEach(l => {
            l.reload();
        });
    }
}

function update_gps(id) {
    if(selfView.currentTab.title == 'Hike GPS') {
        hike_map.gpx_layers.filter(l=>{return l.team?.id == id;}).forEach(l => {
            l.reload();
        });
    }
}
