# Uitleg tocht systeem
Hierbij hebben we een korte uitleg van alle functionalitijd binnen de tocht administratie.
> **Let op:** Post en Persoonlijke QR Codes zijn alleen bedoelt voor de post bemaning. Bij het zoekraken graag zo spoedig mogelijk doorgeven aan tocht begeleiding om codes in te trekken. **Waarom:** Met deze codes kan iedereen de administratie van een post door elkaar gooien.

# Info voor Teams
Elk team krijcht zijn eigen unieke team QR code mee, deze code bevat een link naar het tocht systeem. Als een team deze QR code met een mobiele telefoon scand krijgen ze een pagina met de volgende info te zien:

- De naam van het team
- Op welke posten ze zijn geweest
- Wat hun huidige aantal punten is (alleen als er punten te zijn verdienen op de post)
- En wat als laatst van het team is geregistreerd.

# Info voor post bemaning
Iedere post krijcht een unieke QR code, deze codes zijn niet uitwisselbaar voor de posten onderling. Doormiddel van deze QR code beschik je over de volgende informatie:

- Welk team er momenteel aanwezig is op jouw post
- Welke teams er volgens de route bij de post voor jouw zijn vertrokken
- Welke teams er bij jouw vertrokken zijn en hoelang geleden dat is geweest
- Een overzicht pagina die van alle teams het verloop laat zien
- Status verloop van huidige bezoek aantal

## Check in
Als je als post een team QR code scand krijg je een popup die vraagt of je dit team wil inchecken. **Vergeet niet op de groene knop te drukken**, hierna kan je het team zien bij de aanwezige op je post. Op dit moment is de loop tijd gepauzeerd voor het team.

Waneer een team geen QR code meer heeft kan je een team ook inchecken via de lijst verwachte teams of via de knop "Check in voor een ander team".

> Waneer een team al op jouw post een keer is ingecheckt en je checkt een team opnieuw in word de vorige checkin tijm overschreven. (als je alsnog dit team wilt inchecken moet dit via "Check in voor een ander team")

## Check uit
Waneer je een QR code scand van een team wat al op je post aanwezig is krijg je een popup met de vraag hoeveel punten ze behaald hebben (waneer de punten voor jouw post te behalen zijn) en een groene knop om te bevestigen dat ze zijn uitgecheckt.

In dit scherm kan je ook komen via de grijze check uit knop in de lijst met aanwezige teams.

> Als de score die je heb ingevuld niet goed is kan dit worden gewijzicht bij de tocht leiding, graag via een berichtje doorgeven.


# Voor speleiders
Voor de speleiders zijn er nog een aantal extra modules beschikbaar:

- Bij de Finich post kunnen de tijden en punten per team worden aangepast (Alleen als de gebruiker hier rechten voor heeft).
- Zodra een team gefinishd is woordt deze automaties uit alle post verwacht lijsten gehaald.
- Een reken hulp voor de score van de teams

# Logging
Alle acties die gebeuren binnen het systeem worden op naam opgeslagen, hierdoor is het altijd mogelijk om terug te zien wie welke score heeft aangepast.
