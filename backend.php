<?php

require('config.php');
require_once('lib/database.php');
require_once('lib/global.php');

require_once('login.php');

define('JSON_TIME_FORMAT', 'Y-m-d\TH:i:s.u\Z');
define('MYSQL_TIME_FORMAT', 'Y-m-d H:i:s');

$start_time = DateTime::createFromFormat('U.u', microtime(TRUE), new \DateTimeZone('UTC'));

header("Content-type: application/json; charset=UTF-8");

if (array_key_exists('data', $_REQUEST)) {
	$request_data = json_decode($_REQUEST['data'], true);
}
else {
	$request_data = json_decode(file_get_contents("php://input"), true);
}

// Check if we have all the info
if($request_data == null) {
	echo(json_encode(['error' => 'No data inside request']));
	die();
}

///
$stmt_hike_posten = $db->prepare("SELECT * FROM `hike_post`;");
$stmt_hike_posten->execute();
$rows = $stmt_hike_posten->fetchAll();
$posten = [];

foreach($rows as $row) {
	$posten[$row['id']] = $row;
}

// Connect to database
// done in login.php

$return_data = [];

// --------------------------------------------------------------------------------
// Verwerk alle acties

foreach($request_data['data'] as $command => $args) {
	if(!is_nan($command)) {
		$command = key($args);
		$args = $args[$command];
	}

	if(!in_array($command, $login_user['auth'])
		&& (substr($command, 0, 7) != 'global.' || ($login_user['id'] < 0 && $login_team['id'] < 0))
		&& ($command == 'team.team_name' && $login_team['id'] != $args['team_id'])) {
		$return_data[] = ['rights_error' => [$command => $args]];
		continue;
	}

	switch($command) {

		// === User modules ====

		case 'global.users': {
			$stmt_user = $db->prepare("SELECT `id`, `username` FROM `user`;");

			$stmt_group = $db->prepare("SELECT `group_id` FROM `group_user` WHERE `user_id` = :user_id;");

			$stmt_auth = $db->prepare("SELECT `module` FROM `auth` WHERE `user_id` = :user_id;");

			$stmt_rsw_group = $db->prepare("SELECT `group_id`, `subcamp_id` FROM `rsw_auth` WHERE `user_id` = :user_id;");

			$stmt_rsw_post = $db->prepare("SELECT `rsw_post_id` FROM `rsw_auth_post` WHERE `user_id` = :user_id;");

			$stmt_hike_post = $db->prepare("SELECT `post_id` FROM `hike_auth` WHERE `user_id` = :user_id;");

			$stmt_user->execute();
			foreach($stmt_user->fetchAll() as $user) {
				if(
					!(in_array('rsw.comments', $login_user['auth']) || in_array('rsw.remove_comment', $login_user['auth'])) && // rechten voor alle comments
					!(in_array('user.auth', $login_user['auth'])) && // auth manager
						$login_user['id'] != $user['id'] // eigen comments
				) {
					continue;
				}

				// Laad alle groepen van user
				$stmt_group->execute([
					'user_id'=>$user['id'],
				]);

				$user['groups'] = [];
				foreach($stmt_group->fetchAll() as $row) {
					$user['groups'][] = $row['group_id'];
				}

				if(in_array('user.auth', $login_user['auth'])) {
					$user['auth']=[
						'module' => [],
						'rsw_group' => [],
						'rsw_post' => [],
						'hike_post' => [],
					];

					// Laad alle rechten van user
					$stmt_auth->execute([
						'user_id'=>$user['id'],
					]);

					foreach($stmt_auth->fetchAll() as $auth) {
						$user['auth']['module'][] = $auth['module'];
					}

					$stmt_rsw_group->execute([
						'user_id'=>$user['id'],
					]);
					foreach($stmt_rsw_group->fetchAll() as $auth) {
						$user['auth']['rsw_group'][] = [
							'group'=>$auth['group_id'],
							'subcamp'=>$auth['subcamp_id']
						];
					}

					$stmt_rsw_post->execute([
						'user_id'=>$user['id'],
					]);
					foreach($stmt_rsw_post->fetchAll() as $auth) {
						$user['auth']['rsw_post'][] = $auth['rsw_post_id'];
					}

					$stmt_hike_post->execute([
						'user_id'=>$user['id'],
					]);
					foreach($stmt_hike_post->fetchAll() as $auth) {
						$user['auth']['hike_post'][] = $auth['post_id'];
					}
				}

				$return_data[] = ['user'=>$user];
			}
			break;
		}

		case 'user.auth': {

			break;
		}

		// === Kamp modules ====
		case 'global.group': {
			$stmt_group = $db->prepare("SELECT `id`, `name`, `contact` FROM `group` ORDER BY `name`;");
			$stmt_group->execute();

			foreach($stmt_group->fetchAll() as $group) {
				$return_data[] = ['group'=>$group];
			}
			break;
		}

		case 'global.team': {
			$stmt_team = null;
			if($login_user['id'] > 0 && !array_key_exists('id', $args)) {
				$stmt_team = $db->prepare("SELECT `id`, `naam`, `team`, `nummer`, `subcamp_id`, `speltak`, `group_id`, `contact`, `canceled`, `token`, `route_id`, `rsw_joker` FROM `team`;");
				$stmt_team->execute();
			}
			else {
				$stmt_team = $db->prepare("SELECT `id`, `naam`, `team`, `nummer`, `subcamp_id`, `speltak`, `group_id`, `contact`, `canceled`, `token`, `route_id`, `rsw_joker` FROM `team` WHERE `id` = :team_id;");

				if(array_key_exists('id', $args)) {
					$stmt_team->execute([
						'team_id'=>$args['id'],
					]);
				}
				else {
					$stmt_team->execute([
						'team_id'=>$login_team['id'],
					]);
				}
			}

			$stmt_log = $db->prepare("SELECT :team_id AS team, p.`id` AS post_id,
				(select `time` from `hike_log` where `post_id`=  p.`id` and `team_id` = team and `actie` = 'checkin' order by `id` desc limit 1) AS `checkin`,
				(select `time` from `hike_log` where `post_id`=  p.`id` and `team_id` = team and `actie` = 'checkout' order by `id` desc limit 1) AS `checkout`,
				(select `score` from `rsw_score` where `post_id`=  p.`rsw_post_id` and `team_id` = team order by `id` desc limit 1) AS `score`
				FROM `hike_post` p"); ///TODO: Make that it only load post's that are in the team route

			$stmt_vragen = $db->prepare("SELECT :team_id AS team, r.`vraag_id`,
				(select a.`antwoord` from `hike_antwoord` a where a.`team_id`=team and a.`vraag_id`=r.`vraag_id` order by a.`id` desc limit 1) AS 'antwoord',
				(select a.`status` from `hike_antwoord` a where a.`team_id`=team and a.`vraag_id`=r.`vraag_id` order by a.`id` desc limit 1) AS 'status'
				FROM `hike_route_vraag` r
				WHERE r.`route_id` = (select t.`route_id` from `team` t where t.`id`= :team_id_route limit 1)");

			$stmt_rsw = $db->prepare("SELECT :team_id AS team, p.`id`, p.`groep_id`,
				(select `time` from `rsw_score` where `post_id`=  p.`id` and `team_id` = team order by `id` desc limit 1) AS `time`,
				(select `score` from `rsw_score` where `post_id`=  p.`id` and `team_id` = team order by `id` desc limit 1) AS `score`
				FROM `rsw_post` p
				WHERE p.`groep_id` IN (select `group_id` from `rsw_auth` where `user_id` = :user_id);");
				// :team_id, :user_id

			$stmt_comment = $db->prepare("SELECT *
				FROM `rsw_opmerking`
				WHERE `team_id` = :team_id AND `visible` = 1
				ORDER BY `id` DESC;");
				// :team_id

			foreach($stmt_team->fetchAll() as $team) {
				$team_id = $team['id'];

				if(!array_key_exists($team['id'], $login_user['teams']) && $login_team['id'] != $team['id']) {
					continue;
				}

				if(!in_array('team.update', $login_user['auth'])) {
					unset($team['token']);
				}

				if(!in_array('team.inschrijving', $login_user['auth']) && !in_array('team.update', $login_user['auth'])) {
					if($team['canceled'] != null) {
						$team['naam'] = 'VERVALLEN';
						$team['speltak'] = '';
						$team['groep'] = '';
					}
				}

				$team['score'] = 0;

				// hike history
				if(in_array('hike.view', $login_user['auth'])) {
					$team['hike'] = [];

					$stmt_log->execute([
						'team_id'=>$team['id'],
					]);

					foreach($stmt_log->fetchAll() as $hike) {
						if($hike['checkin'] != null)
							$hike['checkin'] = DateTime::createFromFormat(MYSQL_TIME_FORMAT, $hike['checkin'])->format(JSON_TIME_FORMAT);

						if($hike['checkout'] != null)
							$hike['checkout'] = DateTime::createFromFormat(MYSQL_TIME_FORMAT, $hike['checkout'])->format(JSON_TIME_FORMAT);

						$team['score'] += $hike['score'];

						if(!in_array($hike['post_id'], $login_user['posten']) && ($team['id'] != $login_team['id'] || true/*remove also for team*/))
							unset($hike['score']);

						$team['hike'][] = $hike;
					}

					// hike vragen
					$team['vragen'] = [];

					$stmt_vragen->execute([
						'team_id'=>$team['id'],
						'team_id_route'=>$team['id'],
					]);

					foreach($stmt_vragen->fetchAll() as $vraag) {
						if(!in_array('hike.correctie', $login_user['auth'])) {
							unset($vraag['status']);
						}

						$team['vragen'][] = $vraag;
					}
				}
				else {
					unset($team['route_id']);
				}

				// opmerkingen
				///TODO: convert to global
				$team['opmerkingen'] = [];

				$stmt_comment->execute([
					'team_id'=>$team['id'],
				]);

				foreach($stmt_comment->fetchAll() as $comment) {
					if(
						!(in_array('rsw.comments', $login_user['auth']) || in_array('rsw.remove_comment', $login_user['auth'])) && // rechten voor alle comments
						$login_user['id'] != $comment['user_id'] // eigen comments
					) {
						continue;
					}

					if($comment['time'] != null)
						$comment['time'] = DateTime::createFromFormat(MYSQL_TIME_FORMAT, $comment['time'])->format(JSON_TIME_FORMAT);

					$team['opmerkingen'][] = $comment;
				}

				// rsw history
				if(in_array('rsw.view', $login_user['auth'])) {
					$tmp_onderdelen = [];

					$stmt_rsw->execute([
						'user_id'=>$login_user['id'],
						'team_id'=>$team['id'],
					]);

					foreach($stmt_rsw->fetchAll() as $rsw) {
						if($rsw['time'] != null)
							$rsw['time'] = DateTime::createFromFormat(MYSQL_TIME_FORMAT, $rsw['time'])->format(JSON_TIME_FORMAT);

						$team['score'] += $rsw['score'];

						if(!check_auth_with_subcamp($rsw['groep_id'], $login_user['rsw'])) {
							unset($rsw['score']);
						}

						if(!array_key_exists($rsw['groep_id'], $tmp_onderdelen)) {
							$tmp_onderdelen[$rsw['groep_id']]['id'] = $rsw['groep_id'];
							$tmp_onderdelen[$rsw['groep_id']]['score'] = [];
						}

						$tmp_onderdelen[$rsw['groep_id']]['score'][] = $rsw;
					}

					foreach ($tmp_onderdelen as $v) {
						$team['onderdeel'][] = $v;
					}
				}
				else {
					unset($team['score']);
					unset($team['joker_id']);
				}

				$return_data[] = ['team'=>$team];
			}
			break;
		}

		case 'team.team_name': {
			$stmt = $db->prepare("UPDATE `team` SET `team`=:team WHERE `id`=:team_id;");
			$stmt->execute([
				'team_id'=>$args['team_id'],
				'team'=>$args['team'],
			]);
			break;
		}

		case 'team.update': {
			if(array_key_exists('canceled', $args) && $args['canceled'] != null)
				$args['canceled'] = DateTime::createFromFormat(JSON_TIME_FORMAT, $args['canceled'])->format(MYSQL_TIME_FORMAT);

			if($args['id'] == -1) {
				$stmt = $db->prepare("INSERT INTO `team`
					(`naam`, `subcamp_id`, `nummer`, `speltak`, `group_id`, `contact`, `canceled`, `team`, `rsw_joker`, `route_id`, `token`)
					VALUES (:naam, :subcamp_id, :nummer, :speltak, :group_id, :contact, :canceled, :team, :joker_id, :route_id, :token);");

				$stmt->execute([
					'naam'=>$args['naam'],
					'nummer'=>$args['nummer'],
					'speltak'=>$args['speltak'],
					'group_id'=>(array_key_exists('group_id', $args) ? $args['group_id'] : null),
					'contact'=>$args['contact'],
					'canceled'=>(array_key_exists('canceled', $args) ? $args['canceled'] : null),
					'subcamp_id'=>(array_key_exists('subcamp_id', $args) ? $args['subcamp_id'] : null),
					'team'=>$args['team'],
					'joker_id'=>(array_key_exists('joker_id', $args) ? $args['joker_id'] : null),
					'route_id'=>(array_key_exists('route_id', $args) ? $args['route_id'] : null),
					'token'=>generateRandomString($config['token']['team'], $config['token']['chars']),
				]);
			}
			else {
				$stmt = $db->prepare("UPDATE `team`
					SET `naam`=:naam,
						`subcamp_id`=:subcamp_id,
						`nummer`=:nummer,
						`speltak`=:speltak,
						`group_id`=:group_id,
						`contact`=:contact,
						`canceled`=:canceled,
						`team`=:team,
						`rsw_joker`=:joker_id,
						`route_id`=:route_id
					WHERE `id`=:team_id;");

				$stmt->execute([
					'team_id'=>$args['id'],
					'naam'=>$args['naam'],
					'nummer'=>$args['nummer'],
					'speltak'=>$args['speltak'],
					'group_id'=>(array_key_exists('group_id', $args) ? $args['group_id'] : null),
					'contact'=>$args['contact'],
					'canceled'=>(array_key_exists('canceled', $args) ? $args['canceled'] : null),
					'subcamp_id'=>(array_key_exists('subcamp_id', $args) ? $args['subcamp_id'] : null),
					'team'=>$args['team'],
					'joker_id'=>(array_key_exists('joker_id', $args) ? $args['joker_id'] : null),
					'route_id'=>(array_key_exists('route_id', $args) ? $args['route_id'] : null),
				]);
			}
			break;
		}

		case 'team.inschrijving': {
			$stmt_team = $db->prepare("SELECT * FROM `team` WHERE `id` = :team_id");
			$stmt_team->execute([
				'team_id'=>$args['team_id'],
			]);

			foreach($stmt_team->fetchAll() as $team) { ///TODO: Replace with team (team in login need to bereplaced)
				@require('lib/inschrijf_brief.php');
				break;
			}

			break;
		}

		case 'global.subcamp': {
			$stmt_subcamp = $db->prepare("SELECT `id`, `name`, `contact` FROM `subcamp`;");
			$stmt_subcamp->execute();

			foreach($stmt_subcamp->fetchAll() as $subcamp) {
				$return_data[] = ['subcamp'=>$subcamp];
			}
			break;
		}

		// === RSW Modules ===
		case 'global.rsw': {
			if(!in_array('rsw.view', $login_user['auth'])) {
				$return_data[] = ['rights_error' => [$command => $args]];
				break;
			}

			// Joker mogelijkheden
			$stmt_joker = $db->prepare("SELECT `id`, `naam`, `max_punten` FROM `rsw_joker`;");
			$stmt_joker->execute();
			foreach($stmt_joker->fetchAll() as $rsw_joker) {
				$return_data[] = ['rsw_joker'=>$rsw_joker];
			}

			// Onderdelen
			$stmt_rsw_group = $db->prepare("SELECT `id`, `naam`, `max_punten`, `dynamic`, `dynamic_min`, `dynamic_max`, `joker_id` FROM `rsw_group`;");
			$stmt_rsw_group->execute();
			foreach($stmt_rsw_group->fetchAll() as $rsw_group) {
				if(!array_key_exists($rsw_group['id'], $login_user['rsw'])) {
					continue;
				}

				$rsw_group['auth']=true;

				$rsw_group['dynamic'] = in_array($rsw_group['dynamic'], [true, 'true', 1, '1', 'on', 'yes']);

				if(!array_key_exists($rsw_group['id'], $login_user['rsw'])) {
					$rsw_group['auth'] = false;
				}

				if($config['statics']['hike_score_group'] == $rsw_group['id']) {
					$rsw_group['auth'] = (count($login_user['posten']) > 0);
				}

				$return_data[] = ['rsw_onderdeel'=>$rsw_group];
			}

			$stmt_rsw_post = $db->prepare("SELECT `id`, `naam`, `groep_id`, `uitleg`, `max_punten`, `custom` FROM `rsw_post`;");
			$stmt_rsw_post->execute();

			foreach($stmt_rsw_post->fetchAll() as $rsw_post) {
				if(!array_key_exists($rsw_post['id'], $login_user['rsw_post'])) {
					continue;
				}

				$rsw_post['custom'] = @json_decode($rsw_post['custom']);
				if($rsw_post['custom']) {
					$t_c = [];
					foreach($rsw_post['custom'] as $k=>$v) { ///TODO: implement multiple types (select, checkbox, slider, ...)
						$t_c[] = ['name'=>$k, 'value'=>$v];
					}

					$rsw_post['custom'] = $t_c;
				}

				$return_data[] = ['rsw_post'=>$rsw_post];
			}
			break;
		}

		case 'rsw.submit': {
			$time = $start_time->format(MYSQL_TIME_FORMAT);

			if(array_key_exists("opmerking", $args) && array_key_exists("onderdeel", $args) && strlen($args['opmerking']) > 0) {
				if(!check_auth_with_subcamp($args['onderdeel'], $login_user['rsw'], $args['team_id'])) {
					$return_data[] = ['rights_error' => [$command => $args]];
					break;
				}

				$stmt = $db->prepare("INSERT INTO `rsw_opmerking`
					(`user_id`, `groep_id`, `team_id`, `time`, `opmerking`, `visible`)
					VALUES (:user_id, :group_id, :team_id, :time, :opmerking, 1);");

				$stmt->execute([
					'user_id'=>$login_user['id'],
					'group_id'=>$args['onderdeel'],
					'team_id'=>$args['team_id'],
					'time'=>$time,
					'opmerking'=>$args['opmerking'],
				]);

				$return_data[] = ['update' => [$command => $args]];
			}

			if(array_key_exists("score", $args) && array_key_exists("post", $args)) {
				if(!check_auth_with_subcamp($args['post'], $login_user['rsw_post'], $args['team_id'])) {
					$return_data[] = ['rights_error' => [$command => $args]];
					break;
				}

				// max_punten check bij scores (0 =< geen score mogelijk)
				$stmt = $db->prepare("SELECT `score` FROM `rsw_score` WHERE `post_id`=  :post_id AND `team_id` = :team_id ORDER BY `time` DESC LIMIT 1;");

				$stmt->execute([
					'post_id'=>$args['post'],
					'team_id'=>$args['team_id'],
				]);

				$current_score = null;
				if($stmt->rowCount() > 0) {
					$current_score = $stmt->fetch(PDO::FETCH_COLUMN);
				}

				if($args['score'] < 0) {
					$args['score'] = null;
				}

				if($args['score'] !== $current_score) {
					// max_punten check bij scores (0 =< geen score mogelijk)
					if($args['score'] > 0) {
						$stmt = $db->prepare("SELECT `max_punten` FROM `rsw_post` WHERE `id` = :post_id;");
						$stmt->execute([
							'post_id'=>$args['post'],
						]);
						$max_punten = $stmt->fetch(PDO::FETCH_COLUMN);
						$args['score'] = min($args['score'], $max_punten);
					}

					$stmt = $db->prepare("INSERT INTO `rsw_score`
						(`user_id`, `post_id`, `team_id`, `time`, `score`)
						VALUES (:user_id, :post_id, :team_id, :time, :score);");
					$stmt->execute([
						'user_id'=>$login_user['id'],
						'post_id'=>$args['post'],
						'team_id'=>$args['team_id'],
						'time'=>$time,
						'score'=>$args['score'],
					]);

					$return_data[] = ['update' => [$command => $args, 'before'=>$current_score]];
				}
				else {
					$return_data[] = ['skip' => [$command => $args, 'before'=>$current_score]];
				}
			}

			break;
		}

		case 'rsw.onderdeel_update': {
			$args['dynamic'] = in_array($args['dynamic'], [true, 'true', 1, '1', 'on', 'yes']) ? 1 : 0;

			if($args['id'] == -1) {
				$stmt = $db->prepare("INSERT INTO `rsw_group`
					(`naam`, `max_punten`, `dynamic`, `dynamic_min`, `dynamic_max`, `joker_id`)
					VALUES (:naam, :max_punten, :dynamic, :dynamic_min, :dynamic_max, :joker_id);");

				$stmt->execute([
					'naam'=>$args['naam'],
					'max_punten'=>$args['max_punten'],
					'dynamic'=>$args['dynamic'],
					'dynamic_min'=>(is_numeric($args['dynamic_min']) ? $args['dynamic_min']:null),
					'dynamic_max'=>(is_numeric($args['dynamic_max']) ? $args['dynamic_max']:null),
					'joker_id'=>$args['joker_id'],
				]);
			}
			else {
				$stmt = $db->prepare("UPDATE `rsw_group`
					SET `naam` = :naam,
						`max_punten` = :max_punten,
						`dynamic`=:dynamic,
						`dynamic_min`=:dynamic_min,
						`dynamic_max`=:dynamic_max,
						`joker_id` = :joker_id
					WHERE `id` = :group_id;");

				$stmt->execute([
					'group_id'=>$args['id'],
					'naam'=>$args['naam'],
					'max_punten'=>$args['max_punten'],
					'dynamic'=>$args['dynamic'],
					'dynamic_min'=>(is_numeric($args['dynamic_min']) ? $args['dynamic_min']:null),
					'dynamic_max'=>(is_numeric($args['dynamic_max']) ? $args['dynamic_max']:null),
					'joker_id'=>$args['joker_id'],
				]);
			}

			break;
		}

		case 'rsw.toevoegen': {
			if($args['id'] == -1) {
				$stmt = $db->prepare("INSERT INTO `rsw_post`
					(`groep_id`, `naam`, `uitleg`, `max_punten`, `custom`)
					VALUES (:group_id, :naam, :uitleg, :max_punten, :custom);");

				$stmt->execute([
					'group_id'=>$args['groep_id'],
					'naam'=>$args['naam'],
					'uitleg'=>$args['uitleg'],
					'max_punten'=>$args['max_punten'],
					'custom'=>$args['custom'],
				]);
			}
			else {
				$stmt = $db->prepare("UPDATE `rsw_post`
				SET `groep_id` = :group_id,
					`naam` = :naam,
					`uitleg` = :uitleg,
					`max_punten` = :max_punten,
					`custom` = :custom
				WHERE `id` = :post_id;");

				$stmt->execute([
					'post_id'=>$args['id'],
					'group_id'=>$args['groep_id'],
					'naam'=>$args['naam'],
					'uitleg'=>$args['uitleg'],
					'max_punten'=>$args['max_punten'],
					'custom'=>$args['custom'],
				]);
			}

			break;
		}

		case 'rsw.remove_comment': {
			$stmt = $db->prepare("UPDATE `rsw_opmerking` SET `visible` = 0 WHERE `id` = :opmerking_id;");
			$stmt->execute([
				'opmerking_id'=>$args['id'],
			]);
			break;
		}

		case 'rsw.formulier': {
			$stmt_rsw = null;

			if(array_key_exists('id', $args)) {
				$stmt_rsw = $db->prepare("SELECT * FROM `rsw_group` WHERE `id` = :group_id");
				$stmt_rsw->execute([
					'group_id'=>$args['id'],
				]);
			}
			else {
				$stmt_rsw = $db->prepare("SELECT * FROM `rsw_group`");
				$stmt_rsw->execute();
			}

			$stmt_post = $db->prepare("SELECT * FROM `rsw_post` WHERE `groep_id` = :group_id");

			foreach($stmt_rsw->fetchAll() as $rsw_groep) {
				$rsw_groep['onderdeel']=[];

				$stmt_post->execute([
					'group_id'=>$rsw_groep['id'],
				]);

				foreach($stmt_post->fetchAll() as $rsw_post) {
					$rsw_groep['onderdeel'][$rsw_post['id']] = $rsw_post;
				}

				require_once(__DIR__.'/lib/rsw_formulier/formulier.php');
				@make_form_for_groep($rsw_groep);
			}
			break;
		}

		case 'rsw.tentbriefjes': {
			require_once(__DIR__.'/lib/rsw_formulier/tentbriefjes.php');
			@tentbriefjes();
			break;
		}

		// === Hike modules ====

		case 'global.hike': {
			$stmt_hike_route = $db->prepare("SELECT `id`, `naam`
				FROM `hike_route`;");

			$stmt_volgorde = $db->prepare("SELECT `plaats`, `post_id`
				FROM `hike_volgorde`
				WHERE `route_id` = :route_id
				ORDER BY `plaats`;");

			$stmt_vragen = $db->prepare("SELECT `nummer`, `min_plaats`, `vraag_id`
				FROM `hike_route_vraag`
				WHERE `route_id` = :route_id
				ORDER BY `nummer`;");

			$used_vragen = [];
			$stmt_hike_route->execute();
			foreach($stmt_hike_route->fetchAll() as $route) {
				$route_in_user = false;
				$route['volgorde']=[];
				$route['vragen']=[];

				if($login_team['id'] > -1 && $login_team['route_id'] == $route['id']) {
					$route_in_user = true;
				}

				// Laat route volgorde
				$stmt_volgorde->execute([
					'route_id'=>$route['id'],
				]);
				foreach($stmt_volgorde->fetchAll() as $volgorde) {
					$route['volgorde'][$volgorde['plaats']] = $volgorde['post_id'];

					if(in_array($volgorde['post_id'], $login_user['posten'])) {
						$route_in_user = true;
					}
				}
				ksort($route['volgorde']);


				// Laat route vragen
				$stmt_vragen->execute([
					'route_id'=>$route['id'],
				]);
				foreach($stmt_vragen->fetchAll() as $vraag) {
					$route['vragen'][] = $vraag;
				}

				if($route_in_user) {
					foreach($route['vragen'] as $a){
						$used_vragen[$a['vraag_id']] = true;
					}

					$return_data[] = ['hike_route'=>$route];
				}
			}

			// Posten
			$stmt_post = $db->prepare("SELECT `id`, `naam`, `type`, `allow_skip`, `lat`, `lng`,
				(select `max_punten` from `rsw_post` where `rsw_post`.`id` = `hike_post`.`rsw_post_id`) AS `max_punten`
				FROM `hike_post`;");
			$stmt_post->execute();

			foreach($stmt_post->fetchAll() as $post) {
				// if(!in_array($post['id'], $login_user['posten']) && $login_team['id'] == -1) { ///TODO: Proper team check
				// 	continue;
				// }

				$post['auth']=true;

				if(!in_array('hike.gps', $login_user['auth']) || !in_array($post['id'], $login_user['posten'])) {
					unset($post['lat']);
					unset($post['lng']);
				}
				if(!in_array($post['id'], $login_user['posten'])) {
					$post['auth'] = false;
				}

				$post['allow_skip'] = ($post['allow_skip'] == 1);

				$return_data[] = ['hike_post'=>$post];
			}

			// Vragen
			$stmt_vraag = $db->prepare("SELECT `id`, `naam`, `vraag` FROM `hike_vraag`;");
			$stmt_vraag->execute();

			foreach($stmt_vraag->fetchAll() as $vraag) {
				if(!array_key_exists($vraag['id'], $used_vragen)) {
					continue;
				}

				$return_data[] = ['hike_vraag'=>$vraag];
			}

			break;
		}

		case 'hike.checkin': {
			if(!in_array($args['post'], $login_user['posten'])) {
				$return_data[] = ['rights_error' => [$command => $args]];
				break;
			}

			$time = $start_time->format(MYSQL_TIME_FORMAT);

			$stmt = $db->prepare("INSERT INTO `hike_log`
				(`user_id`, `post_id`, `team_id`, `time`, `actie`)
				VALUES (:user_id, :post_id, :team_id, :time, 'checkin');");

			$stmt->execute([
				'user_id'=>$login_user['id'],
				'post_id'=>$args['post'],
				'team_id'=>$args['team_id'],
				'time'=>$time,
			]);

			$return_data[] = ['update' => [$command => $args]];
			break;
		}

		case 'hike.checkout': {
			if(!in_array($args['post'], $login_user['posten'])) {
				$return_data[] = ['rights_error' => [$command => $args]];
				break;
			}

			$time = $start_time->format(MYSQL_TIME_FORMAT);

			if(!array_key_exists('noTime', $args)) {
				$stmt = $db->prepare("INSERT INTO `hike_log`
					(`user_id`, `post_id`, `team_id`, `time`, `actie`)
					VALUES (:user_id, :post_id, :team_id, :time, 'checkout');");

				$stmt->execute([
					'user_id'=>$login_user['id'],
					'post_id'=>$args['post'],
					'team_id'=>$args['team_id'],
					'time'=>$time,
				]);
			}

			if(array_key_exists('score', $args)) {
				///TODO: auth subcamp_id (hike score log)

				$stmt = $db->prepare("SELECT `score`
					FROM `rsw_score`
					WHERE `post_id` = (select `rsw_post_id` from `hike_post` where `id` = :post_id)
					AND `team_id` = :team_id
					ORDER BY `id` DESC
					LIMIT 1;");

				$stmt->execute([
					'post_id'=>$args['post'],
					'team_id'=>$args['team_id'],
				]);
				$current_score = null;
				while($row = $stmt->fetch(PDO::FETCH_COLUMN)) {$current_score = $row;}

				if($args['score'] != $current_score) {
					// max_punten check bij scores (0 =< geen score mogelijk)
					$stmt = $db->prepare("SELECT `max_punten` FROM `rsw_post` WHERE `id` = (select `rsw_post_id` from `hike_post` where `id` = :post_id);");
					$stmt->execute([
						'post_id'=>$args['post'],
					]);
					$max_punten = $stmt->fetch(PDO::FETCH_COLUMN);

					if($max_punten > 0) {
						$args['score'] = min($max_punten, $args['score']);

						if($args['score'] < 0)
							$args['score'] = null;

						$stmt = $db->prepare("INSERT INTO `rsw_score`
							(`user_id`, `post_id`, `team_id`, `time`, `score`)
							VALUES (:user_id, (select `rsw_post_id` from `hike_post` where `id` = :post_id), :team_id, :time, :score);");

						$stmt->execute([
							'user_id'=>$login_user['id'],
							'post_id'=>$args['post'],
							'team_id'=>$args['team_id'],
							'time'=>$time,
							'score'=>$args['score'],
						]);
					}
				}
			}

			$return_data[] = ['update' => [$command => $args]];
			break;
		}

		case 'hike.antwoord': {
			if(!in_array('hike.correctie', $login_user['auth'])) {
				if($args['team_id'] != $login_team['id']) {
					$return_data[] = ['rights_error' => [$command => $args]];
					break;
				}

				unset($args['status']);
			}

			$status = 'auto_fout';
			$gevonden = false;

			if(array_key_exists('antwoord', $args)) {
				$antwoord = $args['antwoord'];
				$antwoord = trim($antwoord); // Verwijder spaties enz
				$antwoord = strtolower($antwoord); // Vermijd hoofdletter fouten
				if($antwoord == '') {
					$args['antwoord'] = null;
					$antwoord = null;
				}
			}
			else {
				$args['antwoord'] = null;
				$antwoord = null;
			}

			// Andwoorden van deze vraag laden om te bepalen of het goed is
			$stmt_vraag = $db->prepare("SELECT `antwoorden` FROM `hike_vraag` WHERE `id` = :vraag_id;");

			$stmt_vraag->execute([
				'vraag_id'=>$args['vraag'],
			]);
			foreach($stmt_vraag->fetchAll() as $vraag) {
				$gevonden = true;

				foreach (explode("\n", $vraag['antwoorden']) as $value) {
					$value = trim($value);
					$value = strtolower($value);

					if($value == $antwoord && $value != '') {
						$status = 'auto_goed';
						break;
					}
				}
			}

			if(array_key_exists('status', $args))
				$status = $args['status'];

			if($gevonden) {
				// Kijk of huidige antwoord en status gelijk zijn aan gegeven antwoord
				$stmt = $db->prepare("SELECT `antwoord`, `status`
					FROM `hike_antwoord`
					WHERE `vraag_id` = :vraag_id
					AND `team_id` = :team_id
					ORDER BY `id`
					DESC LIMIT 1");

				$stmt->execute([
					'vraag_id'=>$args['vraag'],
					'team_id'=>$args['team_id'],
				]);
				$huidig = null;
				while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {$huidig = $row;}

				if($huidig == null ||
					$huidig['antwoord'] != $args['antwoord'] ||
					$huidig['status'] != $status) {
					$time = $start_time->format(MYSQL_TIME_FORMAT);

					$stmt = $db->prepare("INSERT INTO `hike_antwoord`
						(`vraag_id`, `team_id`, `antwoord`, `time`, `status`)
						VALUES (:vraag_id, :team_id, :antwoord, :time, :status);");

					$stmt->execute([
						'vraag_id'=>$args['vraag'],
						'team_id'=>$args['team_id'],
						'antwoord'=>$args['antwoord'],
						'time'=>$time,
						'status'=>$status,
					]);

					$return_data[] = ['update' => [$command => $args]];
				}
				else {
					$return_data[] = ['skip' => [$command => $args]];
				}
			}
			else {
				$return_data[] = ['rights_error' => [$command => $args]];
			}
			break;
		}

		case 'hike.correctie': {
			if(!in_array($args['post'], $login_user['posten'])) {
				$return_data[] = ['rights_error' => [$command => $args]];
				break;
			}

			foreach (['checkin', 'checkout'] as $actie) {
				if($args[$actie] == null || $args[$actie] == '') continue;
				if($posten[$args['post']]['type'] == 'Start' && $actie == 'checkin') continue;
				if($posten[$args['post']]['type'] == 'Finish' && $actie == 'checkout') continue;

				if(array_key_exists($actie, $args)) {
					if($args[$actie] != null)
						$args[$actie] = DateTime::createFromFormat(JSON_TIME_FORMAT, $args[$actie])->format(MYSQL_TIME_FORMAT);

					// Only update when time is diffrent
					$stmt = $db->prepare("SELECT `time`
						FROM `hike_log`
						WHERE `post_id` = :post_id
						AND `team_id` = :team_id
						AND `actie` = :actie
						ORDER BY `id` DESC
						LIMIT 1;");

					$stmt->execute([
						'post_id'=>$args['post'],
						'team_id'=>$args['team_id'],
						'actie'=>$actie,
					]);
					$current_time = null;
					while($row = $stmt->fetch(PDO::FETCH_COLUMN)) {$current_time = $row;}

					if($current_time != $args[$actie]) {
						$stmt = $db->prepare("INSERT INTO `hike_log`
							(`user_id`, `post_id`, `team_id`, `time`, `actie`)
							VALUES (:user_id, :post_id, :team_id, :time, :actie);");

						$stmt->execute([
							'user_id'=>$login_user['id'],
							'post_id'=>$args['post'],
							'team_id'=>$args['team_id'],
							'time'=>$args[$actie],
							'actie'=>$actie,
						]);
					}
				}
			}

			if(array_key_exists('score', $args)) {
				///TODO: auth subcamp_id (hike score log)

				$time = $start_time->format(MYSQL_TIME_FORMAT);

				$stmt = $db->prepare("SELECT `score`
					FROM `rsw_score`
					WHERE `post_id` = (select `rsw_post_id` from `hike_post` where `id` = :post_id)
					AND `team_id` = :team_id
					ORDER BY `id` DESC
					LIMIT 1;");

				$stmt->execute([
					'post_id'=>$args['post'],
					'team_id'=>$args['team_id'],
				]);
				$current_score = null;
				while($row = $stmt->fetch(PDO::FETCH_COLUMN)) {$current_score = $row;}

				if($args['score'] != $current_score) {
					// max_punten check bij scores (0 =< geen score mogelijk)
					$stmt = $db->prepare("SELECT `max_punten` FROM `rsw_post` WHERE `id` = (select `rsw_post_id` from `hike_post` where `id` = :post_id);");
					$stmt->execute([
						'post_id'=>$args['post'],
					]);
					$max_punten = $stmt->fetch(PDO::FETCH_COLUMN);

					if($max_punten > 0) {
						$args['score'] = min($max_punten, $args['score']);

						if($args['score'] < 0)
							$args['score'] = null;

						$stmt = $db->prepare("INSERT INTO `rsw_score`
							(`user_id`, `post_id`, `team_id`, `time`, `score`)
							VALUES (:user_id, (select `rsw_post_id` from `hike_post` where `id` = :post_id), :team_id, :time, :score);");

						$stmt->execute([
							'user_id'=>$login_user['id'],
							'post_id'=>$args['post'],
							'team_id'=>$args['team_id'],
							'time'=>$time,
							'score'=>$args['score'],
						]);
					}
				}
			}

			$return_data[] = ['update' => [$command => $args]];
			break;
		}

		case 'hike.route': {
			$stmt = $db->prepare("UPDATE `team` SET `route_id` = :route_id WHERE `id` = :team_id;");
			$stmt->execute([
				'route_id'=>$args['route'],
				'team_id'=>$args['team_id'],
			]);

			$return_data[] = ['update' => [$command => $args]];
			break;
		}

		// === QRCodes ===
		case 'qrcodes.rsw': {
			if(array_key_exists('id', $args)) {
				$stmt = $db->prepare("SELECT `username`, `token` FROM `user` WHERE `rsw_group_id` = :group_id");
				$stmt->execute([
					'group_id'=>$args['id'],
				]);

				$tokens = [];
				foreach($stmt->fetchAll() as $row) {
					$tokens[$row['token']] = $row['username'];
				}

				require_once(__DIR__.'/lib/qrcodes_formulier.php');
				@print_qrcodes($tokens);
			}

			break;
		}

		case 'qrcodes.users': {
			$stmt = $db->prepare("SELECT `username`, `token` FROM `user` WHERE `password` IS NOT NULL");
			$stmt->execute();

			$tokens = [];
			foreach($stmt->fetchAll() as $row) {
				$tokens[$row['token']] = $row['username'];
			}

			require_once(__DIR__.'/lib/qrcodes_formulier.php');
			@print_qrcodes($tokens);

			break;
		}

		case 'qrcodes.teams': {
			$stmt = $db->prepare("SELECT `team`, `token` FROM `team`");
			$stmt->execute();

			$tokens = [];
			foreach($stmt->fetchAll() as $row) {
				$tokens[$row['token']] = $row['team'];
			}

			require_once(__DIR__.'/lib/qrcodes_formulier.php');
			@print_qrcodes($tokens);

			break;
		}

		// === GPS Logging ===

		case 'global.gps.push': {
			$args['time'] = DateTime::createFromFormat(JSON_TIME_FORMAT, $args['time'])->format(MYSQL_TIME_FORMAT);

			$stmt = $db->prepare("INSERT INTO `gps_log`
				(`gps_id`, `time`, `latitude`, `longitude`, `accuracy`, `altitude`, `altitudeAccuracy`, `heading`, `speed`)
				VALUES ((SELECT `id` FROM `gps_user` WHERE `uuid` = :uuid), :time, :latitude, :longitude, :accuracy, :altitude, :altitudeAccuracy, :heading, :speed);");

			$stmt->execute([
				'uuid'=>$args['uuid'],
				'time'=>$args['time'],
				'latitude'=>$args['coords']['latitude'],
				'longitude'=>$args['coords']['longitude'],
				'accuracy'=>$args['coords']['accuracy'],
				'altitude'=>$args['coords']['altitude'],
				'altitudeAccuracy'=>$args['coords']['altitudeAccuracy'],
				'heading'=>$args['coords']['heading'],
				'speed'=>$args['coords']['speed'],
			]);
		}

		case 'global.gps.activate': {
			$gps_uuid = gen_uuid();

			if(!array_key_exists($config['gps']['cookie'], $_COOKIE)) {
				$stmt = $db->prepare("INSERT INTO `gps_user`
					(`uuid`, `user_id`, `team_id`, `interval`)
					VALUES (:uuid, :user_id, :team_id, :interval);");

				$stmt->execute([
					'uuid'=>$gps_uuid,
					'user_id'=>($login_user['id'] < 1 ? null : $login_user['id']),
					'team_id'=>($login_team['id'] < 1 ? null : $login_team['id']),
					'interval'=> $config['gps']['interval'],
				]);

				setcookie($config['gps']['cookie'], $gps_uuid, time() + (86400 * 30), "/");

				$gps_user = [
					'id'=>$db->lastInsertId(),
					'uuid'=>$gps_uuid,
					'interval'=>$config['gps']['interval']
				];

				$return_data[] = ['update' => [$command => $args]];
			}
		}

		// === MQTT ===

		case 'global.mqtt': {
			if(array_key_exists('mqtt', $config)) {
				$return_data[] = ['mqtt' => $config['mqtt']];
			}
		}
	}
}

// --------------------------------------------------------------------------------
// Return responce
if($login_user['id'] > 0) {
	$return_data[] = ['current_user'=>[
		'id'=>$login_user['id'],
		'username'=>$login_user['username'],
		'type'=>'user',
		'auth'=>$login_user['auth'],
		'raw'=>$login_user
	]];
}
if($login_team['id'] > 0) {
	$return_data[] = ['current_user'=>[
		'id'=>$login_team['id'],
		'naam'=>$login_team['naam'],
		'type'=>'team'
	]];
}

// Add gps info
if($gps_user['id'] > 0) {
	$return_data[] = ['gps'=>[
		'uuid'=>$gps_user['uuid'],
		'interval'=>$gps_user['interval'],
		'enabled'=>true
	]];
}
else {
	$return_data[] = ['gps'=>[
		'uuid'=>'',
		'interval'=>0,
		'enabled'=>false
	]];
}

$return_data[] = ['time'=>[
	'start'=>$start_time->format(JSON_TIME_FORMAT),
	'finish'=>(DateTime::createFromFormat('U.u', microtime(TRUE), new \DateTimeZone('UTC'))->format(JSON_TIME_FORMAT))
]];

echo json_encode($return_data);
