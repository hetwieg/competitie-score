<?php

if(!file_exists('config.php')) {
	header('location: setup.php');
	die();
}

require('config.php');
require_once('lib/database.php');
require_once('login.php');

/// === Berijd template voor

$template_replace = [
	'CSS'=>'',
	'JS'=>'',
	'APP'=>'',
	'TITLE'=>'',
	'PAGE'=>'',
	'POPUP'=>''
];
ob_start();

/// === Start scripting ===

if(array_key_exists('file', $_GET)) {
	$files = [
		'overzicht'=>'lib/overzicht.php',
		'punten'=>'lib/punten.php'
	];

	if(array_key_exists($_GET['file'], $files)) {
		include($files[$_GET['file']]);
	}
}
else {
	if($login_user['id'] > 0) {
		include('lib/user.php');
	}
	else if($login_team['id'] > 0) {
		include('lib/team.php');
	}
	else {
		echo "Leuk dat je deze pagina bekijkt, maar scan een code voor meer info.";
	}
}

/// === Toon template ===

$template_replace['PAGE'] = ob_get_contents();
ob_end_clean();

$template_replace['CSS'] .= "<link rel=\"stylesheet\" href=\"/css/bootstrap.css\" />";
$template_replace['CSS'] .= "<link rel=\"stylesheet\" href=\"/css/handsontable.full.min.css\" />";
$template_replace['CSS'] .= "<link rel=\"stylesheet\" href=\"/css/app.css?".time()."\" />";
$template_replace['JS'] .= "<script src=\"/js/jquery-3.6.1.js\"></script>";
$template_replace['JS'] .= "<script src=\"/js/handsontable.full.min.js\"></script>";
$template_replace['JS'] .= "<script src=\"/js/moment-with-locales.js\"></script>";
$template_replace['JS'] .= "<script src=\"/js/bootstrap.bundle.js\"></script>";
$template_replace['JS'] .= "<script src=\"/js/knockout-3.5.0.js\"></script>";
$template_replace['JS'] .= "<script src=\"/js/knockstrap.js\"></script>";
$template_replace['JS'] .= "<script src=\"/js/knockout-bindings.js\"></script>";
$template_replace['JS'] .= "<script src=\"/js/mqttws31.js\"></script>";
//$template_replace['JS'] .= "<script src=\"/js/bootstrap-datetimepicker.js\"></script>";

if(in_array('hike.gps', $login_user['auth'])) {
	$template_replace['CSS'] .= '<link rel="stylesheet" href="/css/leaflet.css" type="text/css">';
	$template_replace['JS'] .= '<script src="/js/leaflet-src.js"></script>';
	$template_replace['JS'] .= '<script src="/js/gpx.js"></script>';

	$template_replace['JS'] .= "<script src=\"/js/map.js?".time()."\"></script>";
}

$t = false;
if(array_key_exists($config['token']['cookie'], $_GET)) {
	$t = $_GET[$config['token']['cookie']];
	$template_replace['APP'] .= "<script type=\"text/javascript\">var token=\"".$_GET[$config['token']['cookie']]."\"</script>";
}

if(array_key_exists($config['token']['cookie'], $_COOKIE)) {
	$template_replace['APP'] .= "<script type=\"text/javascript\">var ctoken=\"".$_COOKIE[$config['token']['cookie']]."\"</script>";
}
elseif($t) {
	$template_replace['APP'] .= "<script type=\"text/javascript\">var ctoken=\"".$t."\"</script>";
}

$template_replace['APP'] .= "<script src=\"/js/app.js\"></script>";

$template = implode('', file('template.html'));
foreach ($template_replace as $k => $v) {
	$k = sprintf("{%%%s}", $k);
	$template = str_replace($k, $v, $template);
}

echo $template;
